#!/usr/bin/env bash

#don't forget to add :Z parameter to the volumes to make sure selinux adds a rule that enables you to browser/edit the mounted directory
#this avoids you from having to do: "su -c 'setenforce 0'"
#optional mount -v $HOME/.m2:/root/.m2:Z \

docker run  -v $PWD/../inventory:/tmp/project:Z \
            -v $PWD/../inventory:/tmp/builds/0/project-0:Z \
            -t ocp-golden-images.artifactory.tuiwestern.eu/gitlab-runner-fuse-local:latest gitlab-runner exec shell \
            --env "STRATEGY=none" \
            --env "CICD_SA_BUILD_TOKEN=$1" \
            $2