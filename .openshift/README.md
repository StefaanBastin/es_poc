# How to provision the OpenShift environment for this project
0 Install and configure a Docker server on the local machine

    getting started on Windows
    https://confluence.tui.nl/pages/viewpage.action?pageId=301770616#OpenShiftonWindows7/10-RedHatCDK-gettingstarted-(Optional)DockerToolbox
  
1 OpenShift login on the target cluster

    oc login ...

2 (VirtualBox Only - Docker Toolbox) When using source folders (for openshift-applier and/or project inventory) outside of the $HOME Folder
    
    for every required source folder add a shared folder on the docker toolbox vm in virtualbox 
      
    (settings - shared folder) e.g. add shared folder d/work/dev on d:\work\dev
    
3  Enable the TUI artifactory golden images docker repo   

    docker login -u <artifactory_username> -p <artifactory_password> ocp-golden-images.artifactory.tuiwestern.eu   
   
4 Execute ./dev.sh in .openshift directory using Git Bash or Linux Terminal

    precondition in windows using Git Bash: set environment variable MSYS_NO_PATHCONV=1