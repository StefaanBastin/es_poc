#!/usr/bin/env bash

#don't forget to add :Z parameter to the volumes to make sure selinux adds a rule that enables you to browser/edit the mounted directory
#this avoids you from having to do: "su -c 'setenforce 0'"

docker run -v $HOME/.kube:/root/.kube:Z \
           -v $PWD/../.openshift:/tmp/openshift:Z \
           -t ocp-golden-images.artifactory.tuiwestern.eu/ansible-openshift-applier ansible-playbook -i tmp/openshift/inventory_non_prd opt/openshift-applier/playbooks/openshift-cluster-seed.yml