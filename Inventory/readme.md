# Reservation

## Getting Started

### Docker images

```
docker pull eventstore/eventstore
docker pull rmohr/activemq

docker run --name eventstore -it -p 2113:2113 -p 1113:1113 eventstore/eventstore
docker run --name activemq-node -p 5672:5672 -p 8161:8161 rmohr/activemq
```

### Weburl

1. eventstore : http://192.168.99.100:1113/
1. active mq : http://192.168.99.100:8161/

## Running Openshift image locally

GitLab creates a tar.gz file that is uploaded to OpenShift. This creates a Docker image that you can download and run locally. To run this image locally, perform the following actions:

- add `"docker-registry-default.apps.dev-ocp.tuiwestern.eu"` to the `InsecureRegistry` array in the `config.json` file of your default VM (e.g. C:\Users\username\.docker\machine\machines\default\config.json)
- `docker-machine provision`
- `docker login docker-registry-default.apps.dev-ocp.tuiwestern.eu` (using your OpenShit username and token, which you can find by clicking on the user menu and copying the login command)
- `docker pull docker-registry-default.apps.dev-ocp.tuiwestern.eu/inventory-build/inventory`
- `docker create -t -i docker-registry-default.apps.dev-ocp.tuiwestern.eu/inventory-build/inventory bash`
- `docker start -a -i <hash>`

You are now in a bash session in the application folder on the Docker image. Running `dotnet Inventory.CommandService.Host.WebApiApp.dll` will start the Inventory service.