﻿using System;
using System.Threading.Tasks;
using Amqp;
using Inventory.CommandService.Application;
using Newtonsoft.Json;

namespace Inventory.CommandService.Infra.ActiveMq
{
    public class ActiveMqSender : IExternalMessenger
    {
        private readonly IActiveMqConnection _activeMqConnection;

        public ActiveMqSender(IActiveMqConnection activeMqConnection)
        {
            _activeMqConnection = activeMqConnection;
        }

        public Task Send(Type source, object message)
        {
            var senderLink = new SenderLink(_activeMqConnection.Session, "senderLink", source.Name);
            var json = JsonConvert.SerializeObject(message);
            var amqpMessage = new Message(json);

            return senderLink.SendAsync(amqpMessage);
        }
    }
}