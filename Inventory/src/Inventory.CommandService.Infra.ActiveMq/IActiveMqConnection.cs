﻿using Amqp;

namespace Inventory.CommandService.Infra.ActiveMq
{
    public interface IActiveMqConnection
    {
        Session Session { get; }
    }
}