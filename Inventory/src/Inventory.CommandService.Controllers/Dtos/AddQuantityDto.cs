﻿using System;

namespace Inventory.CommandService.Controllers.Dtos
{
    public class AddQuantityDto
    {
        public DateTime Date { get; set; }
        public int Quantity { get; set; }
    }
}