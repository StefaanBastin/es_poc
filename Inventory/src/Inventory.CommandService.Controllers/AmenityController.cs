﻿using System;
using Inventory.CommandService.Controllers.Dtos;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Domain.Commands;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.CommandService.Controllers
{
    [Route("api/amenity")]
    public class AmenityController
    {
        private readonly ICommandHandler<AddQuantity> _addQuantityHandeler;

        public AmenityController(ICommandHandler<AddQuantity> addQuantityHandeler)
        {
            _addQuantityHandeler = addQuantityHandeler;
        }

        [HttpPut("{id}")]
        public async void AddQuantity(Guid id, [FromBody]AddQuantityDto addQuantity)
            => await _addQuantityHandeler.HandleAsync(new AddQuantity(id, addQuantity.Date, addQuantity.Quantity));
    }
}
