﻿namespace Inventory.CommandService.Host.WebApiApp
{
    public interface IActiveMqConfiguration
    {
        string ActiveMqUri { get; }
    }
}