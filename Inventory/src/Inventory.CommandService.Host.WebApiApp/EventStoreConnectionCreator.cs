﻿using System;
using Autofac;
using EventStore.ClientAPI;

namespace Inventory.CommandService.Host.WebApiApp
{
    public class EventStoreConnectionCreator : IStartable, IDisposable
    {
        public IEventStoreConnection Connection { get; set; }

        private readonly IEventStoreConfiguration _eventStoreConfiguration;

        public EventStoreConnectionCreator(IEventStoreConfiguration eventStoreConfiguration)
        {
            _eventStoreConfiguration = eventStoreConfiguration;
        }

        public void Start()
        {
            Connection = EventStoreConnection.Create(_eventStoreConfiguration.EventStoreEndPoint);
            Connection.ConnectAsync().Wait();
        }

        public void Dispose()
        {
            Connection?.Dispose();
        }
    }
}