﻿using System.IO;
using System.Net;
using Autofac;
using Microsoft.Extensions.Configuration;

namespace Inventory.CommandService.Host.WebApiApp
{
    public class Configuration : IStartable, IEventStoreConfiguration, IActiveMqConfiguration
    {
        private IConfigurationRoot _configuration;

        public void Start()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            _configuration = builder.Build();

            EventStoreEndPoint = new IPEndPoint(IPAddress.Parse(_configuration["EventStore:Address"]), int.Parse(_configuration["EventStore:Port"]));
        }

        public IPEndPoint EventStoreEndPoint { get; private set; }

        public string ActiveMqUri => _configuration["ActiveMq:Uri"];
    }
}