﻿using System.Net;

namespace Inventory.CommandService.Host.WebApiApp
{
    public interface IEventStoreConfiguration
    {
        IPEndPoint EventStoreEndPoint { get; }
    }
}