﻿using Autofac;

namespace Inventory.CommandService.Host.WebApiApp.Container
{
    public class ContainerBuilderFactory
    {
        public ContainerBuilder Create()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new ConfigurationModule());
            containerBuilder.RegisterModule(new EventStoreModule());
            containerBuilder.RegisterModule(new ActiveMqModule());
            containerBuilder.RegisterModule(new DomainModule());

            return containerBuilder;
        }
    }
}