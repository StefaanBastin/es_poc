﻿using Autofac;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Entities;
using Module = Autofac.Module;

namespace Inventory.CommandService.Host.WebApiApp.Container
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<AddQuantityHandler>()
                .As<ICommandHandler<AddQuantity>>();

            builder
                .RegisterType<EventDispatcher>();

            builder
                .RegisterType<AmenityRepository>()
                .As<IRepository<Amenity>>();
        }
    }
}