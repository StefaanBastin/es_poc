﻿using Autofac;
using EventStore.ClientAPI;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Domain.Events;
using Inventory.CommandService.Infra.EventStore;
using Inventory.CommandService.Infra.EventStore.Converters;
using Module = Autofac.Module;

namespace Inventory.CommandService.Host.WebApiApp.Container
{
    public class EventStoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<EventStoreConnectionCreator>()
                .AsSelf()
                .As<IStartable>()
                .SingleInstance();

            builder
                .RegisterType<AggregateEventVersionCache>()
                .As<IAggregateEventVersionCache>()
                .InstancePerLifetimeScope();

            builder
                .Register(context => context.Resolve<EventStoreConnectionCreator>().Connection)
                .As<IEventStoreConnection>()
                .SingleInstance();

            builder
                .RegisterType<StreamNameGenerator>()
                .As<IStreamNameGenerator>();

            builder
                .RegisterType<UTF8ByteArrayConverter>()
                .As<IByteArrayConverter>();

            builder
                .RegisterType<EventRepository>()
                .As<IEventConsumer>()
                .As<IEventRepository>();

            var registerEventTypes = new[]
            {
                typeof(AmenityCreated),
                typeof(AmenityReturned),
                typeof(AmenityTaken),
                typeof(QuantityAdded),
            };

            builder
                .RegisterType<EventMessenger>()
                .AsSelf()
                .As<IEventConsumer>()
                .OnActivated(em => em.Instance.RegisterEventTypes(registerEventTypes))
                .SingleInstance();
        }
    }
}