﻿using System.Threading.Tasks;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Domain.Commands;

namespace Inventory.CommandService.Host.WebApiApp.Container
{
    public class AddQuantityHandlerMock : ICommandHandler<AddQuantity>
    {
        public Task HandleAsync(AddQuantity command)
        {
            return Task.CompletedTask;
        }
    }
}