﻿using Autofac;

namespace Inventory.CommandService.Host.WebApiApp.Container
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<Configuration>()
                .As<IEventStoreConfiguration>()
                .As<IActiveMqConfiguration>()
                .As<IStartable>()
                .SingleInstance();
        }
    }
}