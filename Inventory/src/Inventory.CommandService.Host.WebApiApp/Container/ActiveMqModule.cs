﻿using Autofac;
using Inventory.CommandService.Application;
using Inventory.CommandService.Infra.ActiveMq;

namespace Inventory.CommandService.Host.WebApiApp.Container
{
    public class ActiveMqModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ActiveMqConnection>()
                .As<IActiveMqConnection>()
                .As<IStartable>()
                .SingleInstance();

            builder
                .RegisterType<ActiveMqSender>()
                .As<IExternalMessenger>();
        }
    }
}