﻿using Autofac;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Domain.Commands;

namespace Inventory.CommandService.Host.WebApiApp.Container
{
    public class ContainerBuilderFactoryMock
    {
        public ContainerBuilder Create()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .RegisterType<AddQuantityHandlerMock>()
                .As<ICommandHandler<AddQuantity>>();

            return containerBuilder;
        }
    }
}