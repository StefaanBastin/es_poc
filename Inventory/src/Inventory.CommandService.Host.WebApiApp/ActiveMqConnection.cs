﻿using System;
using Amqp;
using Inventory.CommandService.Infra.ActiveMq;
using IStartable = Autofac.IStartable;

namespace Inventory.CommandService.Host.WebApiApp
{
    public class ActiveMqConnection : IStartable, IActiveMqConnection, IDisposable
    {
        private readonly IActiveMqConfiguration _configuration;
        private Connection _connection;
        public Session Session { get; private set; }

        public ActiveMqConnection(IActiveMqConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Start()
        {
            var address = new Address(_configuration.ActiveMqUri);
            _connection = new Connection(address);
            Session = new Session(_connection);
        }

        public void Dispose()
        {
            _connection.Close();
        }
    }
}