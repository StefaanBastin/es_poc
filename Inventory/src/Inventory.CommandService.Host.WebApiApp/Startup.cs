﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Inventory.CommandService.Controllers;
using Inventory.CommandService.Host.WebApiApp.Container;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSwag;
using NSwag.AspNetCore;

namespace Inventory.CommandService.Host.WebApiApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()

                // https://stackoverflow.com/questions/43669633/why-does-testserver-not-able-to-find-controllers-when-controller-is-in-separate
                .AddApplicationPart(Assembly.GetAssembly(typeof(AmenityController)))
                .AddControllersAsServices();

            services.AddRouting();

#if !DEBUG
            var containerBuilder = new ContainerBuilderFactoryMock()
                .Create();
#else
            var containerBuilder = new ContainerBuilderFactory()
                .Create();
#endif

            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // do this for all the controller projects
            app.UseMvc()
                .UseSwaggerUi3(typeof(AmenityController).GetTypeInfo().Assembly, settings =>
            {
                settings.PostProcess = document =>
                {
                    document.Info.Title = "Inventory.CommandService";
                    document.Schemes = new List<SwaggerSchema> { SwaggerSchema.Https };
                };
            });

            var routeBuilder = new RouteBuilder(app);
            routeBuilder.MapGet("probe/", context =>
            {
                context.Response.StatusCode = 200;
                return Task.CompletedTask;
            });

            var routes = routeBuilder.Build();
            app.UseRouter(routes);
        }
    }
}
