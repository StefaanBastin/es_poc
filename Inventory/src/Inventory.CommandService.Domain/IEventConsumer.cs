﻿using System;
using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public interface IEventConsumer
    {
        Task ConsumeAsync(Event evt, Type source);
    }
}