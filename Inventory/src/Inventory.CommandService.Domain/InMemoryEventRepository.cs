﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public class InMemoryEventRepository : IEventRepository, IEventConsumer
    {
        private readonly IDictionary<Guid, IList<Event>> _events = new Dictionary<Guid, IList<Event>>();

        public Task<IList<Event>> GetAsync(Guid aggregateId, Type aggregateType)
        {
            if (_events.TryGetValue(aggregateId, out var events))
            {
                return Task.FromResult(events);
            }

            return Task.FromResult((IList<Event>)new List<Event>());
        }

        public Task ConsumeAsync(Event evt, Type source)
        {
            if (_events.TryGetValue(evt.AggregateId, out var events))
            {
                events.Add(evt);
            }
            else
            {
                _events.Add(evt.AggregateId, new List<Event> { evt });
            }

            return Task.FromResult(0);
        }
    }
}