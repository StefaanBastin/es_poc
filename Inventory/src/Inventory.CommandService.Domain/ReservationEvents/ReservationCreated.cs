﻿using System;
using System.Collections.Generic;
using Inventory.CommandService.Domain.Dtos;

namespace Inventory.CommandService.Domain.ReservationEvents
{
    public class ReservationCreated
        : Event
    {
        public Guid ReservationId { get; }
        public Guid ProductId { get; }
        public IEnumerable<AmenityDto> Amenities { get;  }

        public ReservationCreated(Guid reservationId, Guid productId, IEnumerable<AmenityDto> amenities)
            : base(reservationId)
        {
            ReservationId = reservationId;
            ProductId = productId;
            Amenities = amenities;
        }
    }
}