﻿using System;

namespace Inventory.CommandService.Domain.ReservationEvents
{
    public class BookedReservationCreated
        : Event
    {
        public BookedReservationCreated(Guid aggregateId)
            : base(aggregateId)
        {
        }
    }
}