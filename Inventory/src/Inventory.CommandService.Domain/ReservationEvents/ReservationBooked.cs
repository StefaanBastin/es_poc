﻿using System;

namespace Inventory.CommandService.Domain.ReservationEvents
{
    public class ReservationBooked
        : Event
    {
        public ReservationBooked(Guid id)
            : base(id)
        {
        }
    }
}