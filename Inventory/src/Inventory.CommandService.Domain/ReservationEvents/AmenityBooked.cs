﻿using System;

namespace Inventory.CommandService.Domain.ReservationEvents
{
    public class AmenityBooked : Event
    {
        public Guid AmenityId { get; }
        public DateTime Date { get; }

        public AmenityBooked(Guid amenityId, Guid reservationId, DateTime date)
            : base(reservationId)
        {
            AmenityId = amenityId;
            Date = date;
        }
    }
}