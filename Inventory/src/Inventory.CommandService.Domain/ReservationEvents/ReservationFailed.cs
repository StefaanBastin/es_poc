﻿using System;

namespace Inventory.CommandService.Domain.ReservationEvents
{
    public class ReservationFailed
        : Event
    {
        public ReservationFailed(Guid reservationId)
            : base(reservationId)
        {
        }
    }
}