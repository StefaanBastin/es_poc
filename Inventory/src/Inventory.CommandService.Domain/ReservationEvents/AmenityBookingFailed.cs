﻿using System;

namespace Inventory.CommandService.Domain.ReservationEvents
{
    public class AmenityBookingFailed
        : Event
    {
        public Guid AmenityId { get; }
        public Guid ReservationId { get; }
        public DateTime Date { get; }

        public AmenityBookingFailed(Guid amenityId, Guid reservationId, DateTime date)
            : base(reservationId)
        {
            AmenityId = amenityId;
            ReservationId = reservationId;
            Date = date;
        }
    }
}