﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public interface IEventRepository
    {
        Task<IList<Event>> GetAsync(Guid aggregateId, Type aggregateType);
    }
}