﻿using System;

namespace Inventory.CommandService.Domain.Commands
{
    public class ReturnAmenity
    {
        public Guid AmenityId { get; }
        public DateTime Date { get; }
        public int Quantity { get; }

        public ReturnAmenity(Guid amenityId, DateTime date, int quantity)
        {
            AmenityId = amenityId;
            Date = date;
            Quantity = quantity;
        }
    }
}