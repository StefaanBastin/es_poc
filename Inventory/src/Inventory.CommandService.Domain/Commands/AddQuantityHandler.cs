﻿using System.Threading.Tasks;
using Inventory.CommandService.Domain.Entities;

namespace Inventory.CommandService.Domain.Commands
{
    public class AddQuantityHandler : ICommandHandler<AddQuantity>
    {
        private readonly IRepository<Amenity> _amenityRepository;
        private readonly EventDispatcher _eventDispatcher;

        public AddQuantityHandler(IRepository<Amenity> amenityRepository, EventDispatcher eventDispatcher)
        {
            _amenityRepository = amenityRepository;
            _eventDispatcher = eventDispatcher;
        }

        public async Task HandleAsync(AddQuantity command)
        {
            var amenity = await _amenityRepository.GetAsync(command.AmenityId);
            amenity.AddQuantity(command.Date, command.Quantity);
            await amenity.DispatchAsync(_eventDispatcher);
        }
    }
}