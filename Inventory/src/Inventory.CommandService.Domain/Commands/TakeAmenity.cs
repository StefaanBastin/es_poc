﻿using System;

namespace Inventory.CommandService.Domain.Commands
{
    public class
TakeAmenity
    {
        public Guid Id { get; }
        public DateTime Date { get; }
        public int Quantity { get; }
        public Guid ReservationId { get; }

        public TakeAmenity(Guid id, DateTime date, int quantity, Guid reservationId)
        {
            Id = id;
            Date = date;
            Quantity = quantity;
            ReservationId = reservationId;
        }
    }
}