﻿using System;

namespace Inventory.CommandService.Domain.Commands
{
    public class AddQuantity
    {
        public Guid AmenityId { get; }
        public DateTime Date { get; }
        public int Quantity { get; }

        public AddQuantity(Guid amenityId, DateTime date, int quantity)
        {
            AmenityId = amenityId;
            Date = date;
            Quantity = quantity;
        }
    }
}