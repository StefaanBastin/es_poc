﻿using System.Threading.Tasks;
using Inventory.CommandService.Domain.Entities;

namespace Inventory.CommandService.Domain.Commands
{
    public class ReturnAmenityHandler : ICommandHandler<ReturnAmenity>
    {
        private readonly IRepository<Amenity> _amenityRepository;
        private readonly EventDispatcher _eventDispatcher;

        public ReturnAmenityHandler(IRepository<Amenity> amenityRepository, EventDispatcher eventDispatcher)
        {
            _amenityRepository = amenityRepository;
            _eventDispatcher = eventDispatcher;
        }

        public async Task HandleAsync(ReturnAmenity command)
        {
            var amenity = await _amenityRepository.GetAsync(command.AmenityId);
            amenity.Return(command.Date, command.Quantity);
            await amenity.DispatchAsync(_eventDispatcher);
        }
    }
}