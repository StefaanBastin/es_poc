﻿using System.Threading.Tasks;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Exceptions;

namespace Inventory.CommandService.Domain.Commands
{
    public class TakeAmenityHandler : ICommandHandler<TakeAmenity>
    {
        private readonly IRepository<Amenity> _amenityRepository;
        private readonly EventDispatcher _eventDispatcher;
        private readonly IRejectedCommandHandler<TakeAmenity> _rejectedCommandHandler;

        public TakeAmenityHandler(
            IRepository<Amenity> amenityRepository,
            EventDispatcher eventDispatcher,
            IRejectedCommandHandler<TakeAmenity> rejectedCommandHandler)
        {
            _amenityRepository = amenityRepository;
            _eventDispatcher = eventDispatcher;
            _rejectedCommandHandler = rejectedCommandHandler;
        }

        public async Task HandleAsync(TakeAmenity command)
        {
            var amenity = await _amenityRepository.GetAsync(command.Id);

            try
            {
                amenity.Take(command.Date, command.Quantity, command.ReservationId);
                await amenity.DispatchAsync(_eventDispatcher);
            }
            catch (NoStockAvailableException)
            {
                await _rejectedCommandHandler.Reject(command);
            }
        }
    }
}