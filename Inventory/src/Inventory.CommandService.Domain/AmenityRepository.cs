﻿using System;
using System.Threading.Tasks;
using Inventory.CommandService.Domain.Entities;

namespace Inventory.CommandService.Domain
{
    public class AmenityRepository : IRepository<Amenity>
    {
        private readonly IEventRepository _eventRepository;

        public AmenityRepository(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<Amenity> GetAsync(Guid aggregateId)
        {
            var events = await _eventRepository.GetAsync(aggregateId, typeof(Amenity));

            return new Amenity(events);
        }
    }
}