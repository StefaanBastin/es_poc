﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public class InMemoryEventBus : IEventConsumer
    {
        private readonly IEnumerable<IEventHandler> _eventHandler;

        public InMemoryEventBus(IEnumerable<IEventHandler> eventHandlers)
        {
            _eventHandler = eventHandlers;
        }

        public Task ConsumeAsync(Event evt, Type source)
        {
            foreach (var eventHandler in _eventHandler)
            {
                var eventHandlerType = eventHandler.GetType();
                if (typeof(IEventHandler<>).IsAssignableFrom(eventHandlerType))
                {
                    return Task.FromResult(0);
                }

                var handleMethods = eventHandlerType
                    .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => m.Name == "HandleAsync" && m.GetParameters().Length == 1);

                foreach (var handleMethod in handleMethods)
                {
                    var eventType = handleMethod.GetParameters().Single().ParameterType;
                    if (eventType == evt.GetType())
                    {
                        handleMethod.Invoke(eventHandler, new object[] { evt });
                    }
                }
            }

            return Task.FromResult(0);
        }
    }
}