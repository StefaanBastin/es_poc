﻿using System;

namespace Inventory.CommandService.Domain.Exceptions
{
    public class NoStockAvailableException : Exception
    {
        public Guid AmenityId { get; }
        public DateTime Date { get; }
        public Guid ReservationId { get; }

        public NoStockAvailableException(string message, Guid amenityId, DateTime date, Guid reservationId)
            : base(message)
        {
            AmenityId = amenityId;
            Date = date;
            ReservationId = reservationId;
        }
    }
}