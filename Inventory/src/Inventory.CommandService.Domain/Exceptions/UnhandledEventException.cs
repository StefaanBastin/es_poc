﻿using System;

namespace Inventory.CommandService.Domain.Exceptions
{
    public class UnhandledEventException : Exception
    {
        public UnhandledEventException(Type type)
            : base($"{type.Name} event is not handled")
        {
        }
    }
}