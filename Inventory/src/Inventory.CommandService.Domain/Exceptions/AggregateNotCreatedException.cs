﻿using System;

namespace Inventory.CommandService.Domain.Exceptions
{
    public class AggregateNotCreatedException : Exception
    {
        public AggregateNotCreatedException()
        {
        }

        public AggregateNotCreatedException(string message)
            : base(message)
        {
        }
    }
}