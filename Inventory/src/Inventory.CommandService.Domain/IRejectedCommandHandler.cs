﻿using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public interface IRejectedCommandHandler<in TCommand>
    {
        Task Reject(TCommand command);
    }
}