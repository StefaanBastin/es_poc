﻿using System.Linq;
using System.Threading.Tasks;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.ReservationEvents;

namespace Inventory.CommandService.Domain.Policies
{
    public class TakeAmenityPolicy : IListenTo<ReservationCreated>
    {
        private readonly ICommandHandler<TakeAmenity> _bookAmenityHandler;

        public TakeAmenityPolicy(ICommandHandler<TakeAmenity> bookAmenityHandler)
        {
            _bookAmenityHandler = bookAmenityHandler;
        }

        public async Task HandleAsync(ReservationCreated evt)
        {
            await Task.WhenAll(evt.Amenities
                .Select(amenity => _bookAmenityHandler.HandleAsync(
                    new TakeAmenity(amenity.Id, amenity.Date, amenity.Quantity, evt.ReservationId)))
                .ToArray());
        }
    }
}