﻿using System.Threading.Tasks;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.ReservationEvents;

namespace Inventory.CommandService.Domain.Policies
{
    public class ReturnAmenityPolicy : IListenTo<AmenityBookingReleased>
    {
        private readonly ICommandHandler<ReturnAmenity> _returnAmenityHandler;

        public ReturnAmenityPolicy(ICommandHandler<ReturnAmenity> returnAmenityHandler)
        {
            _returnAmenityHandler = returnAmenityHandler;
        }

        public Task HandleAsync(AmenityBookingReleased evt)
        {
            return _returnAmenityHandler.HandleAsync(new ReturnAmenity(evt.AmenityId, evt.Date, evt.Quantity));
        }
    }
}