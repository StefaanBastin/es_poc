﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventory.CommandService.Domain.Events;
using Inventory.CommandService.Domain.Exceptions;

namespace Inventory.CommandService.Domain.Entities
{
    public class Amenity : AggregateRoot
    {
        private readonly IDictionary<DateTime, int> _inventory = new Dictionary<DateTime, int>();

        public Amenity()
        {
            Handle(new AmenityCreated(Guid.NewGuid()));
        }

        public Amenity(IList<Event> events)
            : base(events)
        {
            if (!(events.FirstOrDefault() is AmenityCreated))
            {
                throw new ArgumentException($"{nameof(Amenity)} should be created with a {nameof(AmenityCreated)}");
            }
        }

        public void AddQuantity(DateTime date, int quantity)
        {
            Handle(new QuantityAdded(Id, date, quantity));
        }

        public void Take(DateTime date, int quantity, Guid reservationId)
        {
            if (_inventory.ContainsKey(date) && _inventory[date] >= quantity)
            {
                Handle(new AmenityTaken(Id, date, quantity, reservationId));
            }
            else
            {
                throw new NoStockAvailableException(
                    $"Could not take {quantity} stock on {date} for reservation {reservationId} for amenity {Id}",
                    Id,
                    date,
                    reservationId);
            }
        }

        public void Return(DateTime date, int quantity)
        {
            Handle(new AmenityReturned(Id, date, quantity));
        }

        public int GetQuantity(DateTime date)
        {
            return _inventory.ContainsKey(date) ? _inventory[date] : 0;
        }

        protected override void RegisterEventHandlers()
        {
            Handles<AmenityCreated>(Apply);
            Handles<QuantityAdded>(Apply);
            Handles<AmenityTaken>(Apply);
            Handles<AmenityReturned>(Apply);
        }

        private void Apply(AmenityCreated evt)
        {
            Id = evt.AggregateId;
        }

        private void Apply(AmenityTaken evt)
        {
            _inventory[evt.Date] = _inventory[evt.Date] - evt.Quantity;
        }

        private void Apply(AmenityReturned evt)
        {
            _inventory[evt.Date] = _inventory[evt.Date] + evt.Quantity;
        }

        private void Apply(QuantityAdded evt)
        {
            if (!_inventory.ContainsKey(evt.Date))
            {
                _inventory.Add(evt.Date, 0);
            }

            _inventory[evt.Date] += evt.Quantity;
        }
    }
}