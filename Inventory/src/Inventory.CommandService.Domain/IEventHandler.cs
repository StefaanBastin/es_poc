﻿using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public interface IEventHandler
    {
    }

    /// <summary>
    /// An interface for classes that handle internal events, i.e. events from inside the domain.
    /// For events coming from external systems, use <see cref="IListenTo{TEvent}"/>.
    /// </summary>
    /// <typeparam name="TEvent">The event type to listen to.</typeparam>
    public interface IEventHandler<in TEvent> : IEventHandler
        where TEvent : Event
    {
        Task HandleAsync(TEvent evt);
    }
}