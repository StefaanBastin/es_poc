﻿using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public interface ICommandHandler<in TCommand>
    {
        Task HandleAsync(TCommand command);
    }
}