﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public class EventDispatcher
    {
        private readonly IEnumerable<IEventConsumer> _eventConsumer;

        public EventDispatcher(IEnumerable<IEventConsumer> eventConsumer)
        {
            _eventConsumer = eventConsumer;
        }

        public Task DispatchAsync(Event evt, Type source)
        {
            return Task.WhenAll(_eventConsumer
                .Select(eventConsumer => eventConsumer.ConsumeAsync(evt, source))
                .ToArray());
        }
    }
}