﻿using System;
using System.Threading.Tasks;

namespace Inventory.CommandService.Domain
{
    public interface IRepository<TAggregate>
    {
        Task<TAggregate> GetAsync(Guid aggregateId);
    }
}