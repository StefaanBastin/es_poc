﻿using System;

namespace Inventory.CommandService.Domain.Events
{
    public class AmenityCreated : Event
    {
        public AmenityCreated(Guid id)
            : base(id)
        {
        }
    }
}