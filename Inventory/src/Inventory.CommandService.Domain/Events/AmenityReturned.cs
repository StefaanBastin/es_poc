﻿using System;

namespace Inventory.CommandService.Domain.Events
{
    public class AmenityReturned : Event
    {
        public DateTime Date { get; }
        public int Quantity { get; }

        public AmenityReturned(Guid id, DateTime date, int quantity)
            : base(id)
        {
            Date = date;
            Quantity = quantity;
        }
    }
}