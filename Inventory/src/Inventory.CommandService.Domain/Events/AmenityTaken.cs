﻿using System;

namespace Inventory.CommandService.Domain.Events
{
    public class AmenityTaken : Event
    {
        public DateTime Date { get; }
        public int Quantity { get; }
        public Guid ReservationId { get; }

        public AmenityTaken(Guid amenityId, DateTime date, int quantity, Guid reservationId)
            : base(amenityId)
        {
            ReservationId = reservationId;
            Date = date;
            Quantity = quantity;
        }
    }
}