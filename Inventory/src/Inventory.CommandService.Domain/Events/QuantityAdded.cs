﻿using System;

namespace Inventory.CommandService.Domain.Events
{
    public class QuantityAdded : Event
    {
        public DateTime Date { get; }
        public int Quantity { get; }

        public QuantityAdded(Guid amenityId, DateTime date, int quantity)
            : base(amenityId)
        {
            Date = date;
            Quantity = quantity;
        }
    }
}