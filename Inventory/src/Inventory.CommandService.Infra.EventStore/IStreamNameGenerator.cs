﻿using System;

namespace Inventory.CommandService.Infra.EventStore
{
    public interface IStreamNameGenerator
    {
        string Generate(Guid aggregateId, Type source);
        string Generate(Type source);
    }
}