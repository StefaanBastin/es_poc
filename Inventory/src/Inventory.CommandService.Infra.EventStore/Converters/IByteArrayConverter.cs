﻿using System;

namespace Inventory.CommandService.Infra.EventStore.Converters
{
    public interface IByteArrayConverter
    {
        byte[] ToByteArray(object value);
        object FromByteArray(byte[] bytes, Type type);
    }
}