﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Infra.EventStore.Converters;

namespace Inventory.CommandService.Infra.EventStore
{
    public class EventMessenger : IEventConsumer
    {
        private readonly IStreamNameGenerator _nameGenerator;
        private readonly IEventStoreConnection _connection;
        private readonly IByteArrayConverter _converter;
        private readonly HashSet<Type> _registerdTypes;

        public EventMessenger(
            IStreamNameGenerator nameGenerator,
            IEventStoreConnection connection,
            IByteArrayConverter converter)
        {
            _nameGenerator = nameGenerator;
            _connection = connection;
            _converter = converter;
            _registerdTypes = new HashSet<Type>();
        }

        public void RegisterEventTypes(IEnumerable<Type> registerdTypes)
        {
            var unused = registerdTypes
                .Select(_registerdTypes.Add)
                .ToList();
        }

        public Task ConsumeAsync(Event evt, Type source)
        {
            if (!_registerdTypes.Any(t => t == evt.GetType()))
            {
                return Task.CompletedTask;
            }

            var eventData = _converter.ToByteArray(evt);

            var storeEvent = new EventData(evt.AggregateId, evt.GetType().Name, true, eventData, null);
            var streamName = _nameGenerator.Generate(source);

            return _connection.AppendToStreamAsync(streamName, ExpectedVersion.Any, storeEvent);
        }
    }
}