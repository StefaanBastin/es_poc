﻿using System;

namespace Inventory.CommandService.Infra.EventStore
{
    public interface IAggregateEventVersionCache
    {
        long? GetEventVersion(Guid aggregateId);
        void SetEventVersion(Guid aggregateId, long eventVersion);
    }
}