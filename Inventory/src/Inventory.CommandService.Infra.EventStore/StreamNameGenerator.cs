﻿using System;

namespace Inventory.CommandService.Infra.EventStore
{
    public class StreamNameGenerator : IStreamNameGenerator
    {
        public string Generate(Guid aggregateId, Type source)
            => source.Name + "-" + aggregateId.ToString().Replace("-", string.Empty);

        public string Generate(Type source)
            => source.Name;
    }
}