﻿using System;

namespace Inventory.CommandService.Infra.EventStore
{
    public class EventMetaData
    {
        public Type EventType { get; set; }
    }
}