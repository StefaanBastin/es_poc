﻿using System;
using System.Threading.Tasks;

namespace Inventory.CommandService.Application
{
    public interface IExternalMessenger
    {
        Task Send(Type source, object message);
    }
}
