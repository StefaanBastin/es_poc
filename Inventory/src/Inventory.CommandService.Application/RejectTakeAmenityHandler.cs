﻿using System.Threading.Tasks;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Entities;
using Reservation.CommandService.Messages;

namespace Inventory.CommandService.Application
{
    public class RejectTakeAmenityHandler : IRejectedCommandHandler<TakeAmenity>
    {
        private readonly IExternalMessenger _externalMessenger;

        public RejectTakeAmenityHandler(IExternalMessenger externalMessenger)
        {
            _externalMessenger = externalMessenger;
        }

        public Task Reject(TakeAmenity command)
        {
            return _externalMessenger.Send(typeof(Amenity), new FailAmenityBooking(command.Id, command.Date, command.ReservationId));
        }
    }
}