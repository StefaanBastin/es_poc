﻿using System;
using FluentAssertions;
using Inventory.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Infra.EventStore.UnitTest.StreamNameGeneratorTests
{
    public class When_Generating : Given_When_Then_Test
    {
        private StreamNameGenerator _sut;
        private string _result;

        protected override void Given()
        {
            _sut = new StreamNameGenerator();
        }

        protected override void When()
        {
            _result = _sut.Generate(new Guid("2e1565d5-a74c-4903-bf1d-c8c614c3cedc"), typeof(TestAggregate));
        }

        [Fact]
        public void Then_The_Name_Should_be()
        {
            _result.Should().Be("TestAggregate-2e1565d5a74c4903bf1dc8c614c3cedc");
        }
    }
}