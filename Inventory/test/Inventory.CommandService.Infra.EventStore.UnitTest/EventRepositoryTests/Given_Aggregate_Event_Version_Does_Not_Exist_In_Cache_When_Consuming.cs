﻿using System;
using EventStore.ClientAPI;
using Inventory.CommandService.Infra.EventStore.Converters;
using Inventory.CommandService.TestInfra;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Infra.EventStore.UnitTest.EventRepositoryTests
{
    public class Given_Aggregate_Event_Version_Does_Not_Exist_In_Cache_When_Consuming : Given_When_Then_Test
    {
        private EventRepository _sut;
        private Guid _aggregateId;
        private Mock<IEventStoreConnection> _eventStoreConnectionMock;
        private Mock<IAggregateEventVersionCache> _aggregateVersionCacheMock;

        protected override void Given()
        {
            _aggregateId = Guid.NewGuid();

            _aggregateVersionCacheMock = new Mock<IAggregateEventVersionCache>();
            _aggregateVersionCacheMock
                .Setup(cache => cache.GetEventVersion(_aggregateId))
                .Returns<long?>(null);

            _eventStoreConnectionMock = new Mock<IEventStoreConnection>();
            _sut = new EventRepository(new Mock<IStreamNameGenerator>().Object, _eventStoreConnectionMock.Object, _aggregateVersionCacheMock.Object, new UTF8ByteArrayConverter());
        }

        protected override void When()
        {
            _sut.ConsumeAsync(new TestEvent(_aggregateId), GetType());
        }

        [Fact]
        public void Then_Append_As_New_Stream()
        {
            _eventStoreConnectionMock
                .Verify(connection => connection.AppendToStreamAsync(It.IsAny<string>(), ExpectedVersion.NoStream, It.IsAny<EventData>()), Times.Once);
        }

        [Fact]
        public void Then_Set_Initial_Cached_Version()
        {
            _aggregateVersionCacheMock
                .Verify(cache => cache.SetEventVersion(_aggregateId, 0), Times.Once);
        }
    }
}