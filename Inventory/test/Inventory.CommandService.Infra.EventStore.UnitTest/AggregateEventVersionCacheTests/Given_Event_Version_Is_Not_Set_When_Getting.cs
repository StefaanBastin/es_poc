﻿using System;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Infra.EventStore.UnitTest.AggregateEventVersionCacheTests
{
    public class Given_Event_Version_Is_Not_Set_When_Getting : Given_When_Then_Test
    {
        private AggregateEventVersionCache _sut;
        private Guid _aggregateId;
        private long? _result;

        protected override void Given()
        {
            _aggregateId = Guid.NewGuid();

            _sut = new AggregateEventVersionCache();
        }

        protected override void When()
        {
            _result = _sut.GetEventVersion(_aggregateId);
        }

        [Fact]
        public void Then_Should_Return_Null()
        {
            _result.Should().BeNull();
        }
    }
}