﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Entities;
using Moq;
using Reservation.CommandService.Messages;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Application.UnitTests.RejectTakeAmenityHandlerTests
{
    public class When_Rejecting : Given_When_Then_Test
    {
        private Mock<IExternalMessenger> _externalMessengerMock;
        private RejectTakeAmenityHandler _sut;
        private Guid _id;
        private Guid _reservationId;
        private TakeAmenity _command;
        private IList<FailAmenityBooking> _messages;
        private DateTime _date;

        protected override void Given()
        {
            _messages = new List<FailAmenityBooking>();

            _externalMessengerMock = new Mock<IExternalMessenger>();
            _externalMessengerMock
                .Setup(x => x.Send(typeof(Amenity), It.IsAny<FailAmenityBooking>()))
                .Returns(Task.CompletedTask)
                .Callback((Type t, object msg) => _messages.Add((FailAmenityBooking)msg));

            _id = Guid.NewGuid();
            _date = new DateTime(2018, 6, 4);
            _reservationId = Guid.NewGuid();
            _command = new TakeAmenity(_id, _date, 5, _reservationId);

            _sut = new RejectTakeAmenityHandler(_externalMessengerMock.Object);
        }

        protected override void When()
        {
            _sut.Reject(_command).Wait();
        }

        [Fact]
        public void Then_Should_Send_Message_To_External_Systems()
        {
            _messages.Should().BeEquivalentTo(new List<FailAmenityBooking>
            {
                new FailAmenityBooking(_id, new DateTime(2018, 6, 4), _reservationId)
            });
        }
    }
}