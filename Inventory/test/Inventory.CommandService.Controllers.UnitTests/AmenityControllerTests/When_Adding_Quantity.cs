﻿using System;
using FluentAssertions;
using Inventory.CommandService.Controllers.Dtos;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Domain.Commands;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Controllers.UnitTests.AmenityControllerTests
{
    public class When_Adding_Quantity : Given_When_Then_Test
    {
        private AmenityController _sut;
        private Guid _ammenityId;
        private AddQuantity _command;

        protected override void Given()
        {
            var addQuantityHandelerMock = new Mock<ICommandHandler<AddQuantity>>();
            addQuantityHandelerMock
                .Setup(command => command.HandleAsync(It.IsAny<AddQuantity>()))
                .Callback<AddQuantity>(command => _command = command);

            _ammenityId = Guid.NewGuid();

            _sut = new AmenityController(addQuantityHandelerMock.Object);
        }

        protected override void When()
        {
            var addQuantityDto = new AddQuantityDto
            {
                Date = new DateTime(2003, 12, 20),
                Quantity = 55
            };
            _sut.AddQuantity(_ammenityId, addQuantityDto);
        }

        [Fact]
        public void Then_Should_Call_Command_Hanlder()
        {
            _command.Should().BeEquivalentTo(new AddQuantity(_ammenityId, new DateTime(2003, 12, 20), 55));
        }
    }
}