﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;

namespace Inventory.CommandService.Infra.ActiveMq.ItgTests
{
    public class ActiveMqDockerFixture : IDisposable
    {
        private readonly bool _isRunningBeforeStart;
        private readonly Semaphore _semaphore;

        public ActiveMqDockerFixture()
        {
            _semaphore = new Semaphore(0, 1);
            _isRunningBeforeStart = DockerRun("inspect -f {{.State.Running}} activemq-node") == "true\n";

            if (_isRunningBeforeStart)
            {
                return;
            }

            DockerRun("run --name activemq-node -p 5672:5672 -p 8161:8161 rmohr/activemq", false);
            _semaphore.WaitOne(TimeSpan.FromSeconds(10));
        }

        public void Dispose()
        {
            if (!_isRunningBeforeStart)
            {
                DockerRun("rm activemq-node -f");
            }
        }

        private string DockerRun(string command, bool waitForResult = true)
        {
            var startInfo =
                new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "docker",
                    Arguments = command,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false
                };

            var process = new Process
            {
                StartInfo = startInfo
            };

            if (waitForResult)
            {
                process.Start();
                return process.StandardOutput.ReadToEnd();
            }

            process.OutputDataReceived += OnOutputDataReceived;
            process.ErrorDataReceived += OnOutputDataReceived;

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            return string.Empty;
        }

        private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data) && Regex.IsMatch(e.Data, "Apache ActiveMQ .* started"))
            {
                _semaphore.Release();
            }
        }
    }
}