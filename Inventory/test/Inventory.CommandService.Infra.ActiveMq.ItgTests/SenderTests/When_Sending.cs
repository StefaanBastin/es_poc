﻿using System;
using Amqp;
using Autofac;
using Autofac.Builder;
using FluentAssertions;
using Inventory.CommandService.Host.WebApiApp;
using Inventory.CommandService.Host.WebApiApp.Container;
using Inventory.CommandService.TestInfra;
using Newtonsoft.Json;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Infra.ActiveMq.ItgTests.SenderTests
{
    [Collection("ActiveMqDocker collection")]
    public class When_Sending : Given_When_Then_Test
    {
        private ActiveMqSender _sut;
        private Type _source;
        private object _message;
        private string _uri;

        protected override void Given()
        {
            _source = typeof(When_Sending);
            _message = new TestMessage
            {
                Foo = "bar"
            };

            var container = new ContainerBuilderFactory()
                .Create()
                .Build(ContainerBuildOptions.IgnoreStartableComponents);

            container.StartStartable<IActiveMqConfiguration>();
            container.StartStartable<IActiveMqConnection>();

            _uri = container.Resolve<IActiveMqConfiguration>().ActiveMqUri;
            var activeMqConnectionCreator = container.Resolve<IActiveMqConnection>();

            _sut = new ActiveMqSender(activeMqConnectionCreator);
        }

        protected override void When()
        {
            _sut.Send(_source, _message).Wait();
        }

        [Fact]
        public void Then_Should_Send_To_ActiveMq()
        {
            var address = new Address(_uri);
            var connection = new Connection(address);

            try
            {
                var session = new Session(connection);
                var receiverLink = new ReceiverLink(session, "receiverLink", _source.Name);

                var amqpMessage = receiverLink.Receive(TimeSpan.FromSeconds(1));
                var json = amqpMessage.Body.ToString();
                var message = JsonConvert.DeserializeObject<TestMessage>(json);
                receiverLink.Accept(amqpMessage);

                message.Should().BeEquivalentTo(_message);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}