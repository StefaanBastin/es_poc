﻿using System;
using System.Collections.Generic;

namespace Inventory.CommandService.Infra.EventStore.ItgTests
{
    public class TestRunSpecificStreamNameGenerator : IStreamNameGenerator
    {
        private readonly IStreamNameGenerator _streamNameGenerator;
        private readonly IDictionary<Type, string> _generatedNames = new Dictionary<Type, string>();

        public TestRunSpecificStreamNameGenerator(IStreamNameGenerator streamNameGenerator)
        {
            _streamNameGenerator = streamNameGenerator;
        }

        public string Generate(Guid aggregateId, Type source)
        {
            return _streamNameGenerator.Generate(aggregateId, source);
        }

        public string Generate(Type source)
        {
            if (_generatedNames.TryGetValue(source, out var streamName))
            {
                return streamName;
            }

            streamName = $"{source}{Guid.NewGuid()}";
            _generatedNames.Add(source, streamName);

            return streamName;
        }
    }
}