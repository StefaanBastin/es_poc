﻿using System;
using System.Collections.Generic;
using Autofac;
using EventStore.ClientAPI;
using Inventory.CommandService.Domain;
using Inventory.CommandService.Host.WebApiApp.Container;

namespace Inventory.CommandService.Infra.EventStore.ItgTests
{
    public class EventStoreFixture : IDisposable
    {
        public IEventStoreConnection EventStoreConnection { get; }
        public EventDispatcher EventDispatcher { get; }
        public EventMessenger EventMessenger { get; }

        private readonly bool _isRunningBeforeStart;
        private readonly List<string> _streamNames = new List<string>();

        public EventStoreFixture()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new ConfigurationModule());
            containerBuilder.RegisterModule(new EventStoreModule());
            containerBuilder.RegisterModule(new DomainModule());

            var container = containerBuilder.Build();

            _isRunningBeforeStart = DockerRun("inspect -f {{.State.Running}} eventstore-node") == "true\n";

            if (!_isRunningBeforeStart)
            {
                DockerRun("run --name eventstore-node -p 2113:2113 -p 1113:1113 eventstore/eventstore", false);
            }

            EventStoreConnection = container.Resolve<IEventStoreConnection>();
            EventDispatcher = container.Resolve<EventDispatcher>();
            EventMessenger = container.Resolve<EventMessenger>();
        }

        public void Dispose()
        {
            if (!_isRunningBeforeStart)
            {
                DockerRun("rm eventstore-node -f");
            }
            else
            {
                _streamNames.ForEach(DeleteStream);
            }

            EventStoreConnection.Dispose();
        }

        private void DeleteStream(string streamName)
            => EventStoreConnection.
                DeleteStreamAsync(streamName, ExpectedVersion.Any, hardDelete: true).Wait();

        private static string DockerRun(string command, bool waitForResult = true)
        {
            var startInfo =
                new System.Diagnostics.ProcessStartInfo
                {
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                    FileName = "docker",
                    Arguments = command,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                };
            var process = new System.Diagnostics.Process
            {
                StartInfo = startInfo
            };
            process.Start();
            return waitForResult ? process.StandardOutput.ReadToEnd() : string.Empty;
        }

        public void AddStream(string streamName)
            => _streamNames.Add(streamName);
    }
}