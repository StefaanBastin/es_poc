﻿using System;
using Inventory.CommandService.TestInfra;

namespace Inventory.CommandService.Infra.EventStore.ItgTests.EventRepositoryTests
{
    public class TestAmountEvent : TestEvent
    {
        public int TestAmount { get; }

        public TestAmountEvent(Guid aggregateId, int testAmount)
            : base(aggregateId)
        {
            TestAmount = testAmount;
        }
    }
}