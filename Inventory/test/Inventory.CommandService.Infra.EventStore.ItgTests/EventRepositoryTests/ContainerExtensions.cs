﻿using System.Collections.Generic;
using Autofac;

namespace Inventory.CommandService.Infra.EventStore.ItgTests.EventRepositoryTests
{
    public static class ContainerExtensions
    {
        /// <summary>
        /// Starts the <see cref="IStartable"/> of type T.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IStartable"/> to start.</typeparam>
        /// <param name="container">The Autofac container containing the <see cref="IStartable"/>.</param>
        public static void StartStartable<T>(this IComponentContext container)
        {
            var startables = container.Resolve<IEnumerable<IStartable>>();

            foreach (var startable in startables)
            {
                if (startable is T)
                {
                    startable.Start();
                }
            }
        }
    }
}