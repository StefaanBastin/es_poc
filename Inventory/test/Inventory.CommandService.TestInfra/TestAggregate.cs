﻿using System;
using Inventory.CommandService.Domain;

namespace Inventory.CommandService.TestInfra
{
    public class TestAggregate : AggregateRoot
    {
        public void AddTestEvent()
        {
            Handle(new TestEvent(Guid.NewGuid()));
        }

        protected override void RegisterEventHandlers()
        {
            Handles<TestEvent>(Apply);
        }

        private void Apply(TestEvent testEvent)
        {
        }
    }
}