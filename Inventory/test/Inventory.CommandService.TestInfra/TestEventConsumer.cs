﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inventory.CommandService.Domain;

namespace Inventory.CommandService.TestInfra
{
    public class TestEventConsumer : IEventConsumer
    {
        public IList<Event> Events { get; }

        public TestEventConsumer()
        {
            Events = new List<Event>();
        }

        public Task ConsumeAsync(Event evt, Type source)
        {
            Events.Add(evt);
            return Task.FromResult(0);
        }
    }
}