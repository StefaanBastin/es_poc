﻿using System;
using Inventory.CommandService.Domain;

namespace Inventory.CommandService.TestInfra
{
    public class TestEvent : Event
    {
        public TestEvent(Guid aggregateId)
            : base(aggregateId)
        {
        }
    }
}