﻿using System.Collections.Generic;
using Inventory.CommandService.Domain;

namespace Inventory.CommandService.TestInfra
{
    public class TestEventDispatcher : EventDispatcher
    {
        private readonly TestEventConsumer _testEventConsumer;

        public IEnumerable<Event> Events => _testEventConsumer.Events;

        public TestEventDispatcher()
            : this(new TestEventConsumer())
        {
        }

        private TestEventDispatcher(TestEventConsumer testEventConsumer)
            : base(new List<IEventConsumer> { testEventConsumer })
        {
            _testEventConsumer = testEventConsumer;
        }
    }
}