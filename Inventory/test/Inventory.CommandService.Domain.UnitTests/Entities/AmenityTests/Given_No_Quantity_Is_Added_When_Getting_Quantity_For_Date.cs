﻿using System;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class Given_No_Quantity_Is_Added_When_Getting_Quantity_For_Date : Given_When_Then_Test
    {
        private Amenity _sut;
        private int _result;

        protected override void Given()
        {
            _sut = new Amenity();
        }

        protected override void When()
        {
            _result = _sut.GetQuantity(new DateTime(2018, 1, 12));
        }

        [Fact]
        public void Then_Quantity_Should_Be_Zero()
        {
            _result.Should().Be(0);
        }
    }
}