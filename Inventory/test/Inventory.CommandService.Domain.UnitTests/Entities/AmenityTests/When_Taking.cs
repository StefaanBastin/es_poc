﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Inventory.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class When_Taking : Given_When_Then_Test
    {
        private readonly Guid _reservationId = Guid.NewGuid();
        private Amenity _sut;
        private DateTime _date;
        private int _quantity;

        protected override void Given()
        {
            _date = new DateTime(2018, 1, 2);
            _quantity = 3;
            var amenityId = Guid.NewGuid();
            var startQuantity = 19;
            _sut = new Amenity(new List<Event>
            {
                new AmenityCreated(amenityId),
                new QuantityAdded(amenityId, _date, startQuantity)
            });
        }

        protected override void When()
        {
            _sut.Take(_date, _quantity, _reservationId);
        }

        [Fact]
        public void Then_The_Amenity_Should_Contain_The_Amenity_Booked_Event()
        {
            var testEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(testEventDispatcher)
                .Wait();

            testEventDispatcher.Events.Last().Should().BeEquivalentTo(new AmenityTaken(_sut.Id, _date, _quantity, _reservationId));
        }

        [Fact]
        public void Then_The_Quantity_Should_Be_Reduced()
        {
            _sut.GetQuantity(_date).Should().Be(16);
        }
    }
}