﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Inventory.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class Given_Date_Has_Quantity_When_Adding_Quantity : Given_When_Then_Test
    {
        private Amenity _sut;
        private DateTime _date;

        protected override void Given()
        {
            _date = new DateTime(2018, 4, 26);
            var amenityId = Guid.NewGuid();
            _sut = new Amenity(new List<Event>
            {
                new AmenityCreated(amenityId),
                new QuantityAdded(amenityId, _date, 10)
            });
        }

        protected override void When()
        {
            _sut.AddQuantity(_date, 11);
        }

        [Fact]
        public void Then_Quantity_Should_Be_Increased()
        {
            _sut.GetQuantity(_date).Should().Be(21);
        }

        [Fact]
        public void Then_The_Amenity_Should_Contain_The_Quantity_Added_Event()
        {
            var testEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(testEventDispatcher)
                .Wait();

            testEventDispatcher
                .Events
                .Last()
                .Should()
                .BeEquivalentTo(new QuantityAdded(_sut.Id, _date, 11));
        }
    }
}