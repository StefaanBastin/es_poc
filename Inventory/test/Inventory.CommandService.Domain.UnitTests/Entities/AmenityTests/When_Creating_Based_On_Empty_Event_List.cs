﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class When_Creating_Based_On_Empty_Event_List : Given_When_Then_Test
    {
        private Action _act;

        protected override void Given()
        {
        }

        protected override void When()
        {
            _act = () =>
            {
                var unused = new Amenity(new List<Event>());
            };
        }

        [Fact]
        public void Then_Should_Throw()
        {
            _act.Should().Throw<ArgumentException>();
        }
    }
}