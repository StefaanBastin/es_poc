﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class When_Creating_Based_On_Events_Without_Amenity_Created_First : Given_When_Then_Test
    {
        private Action _act;

        protected override void Given()
        {
        }

        protected override void When()
        {
            _act = () =>
            {
                var unused = new Amenity(new List<Event>
                {
                    new QuantityAdded(Guid.NewGuid(), default(DateTime), default(int))
                });
            };
        }

        [Fact]
        public void Then_Should_Throw()
        {
            _act.Should().Throw<ArgumentException>();
        }
    }
}