﻿using System;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Exceptions;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class Given_No_Quantity_When_Taking : Given_When_Then_Test
    {
        private Amenity _sut;
        private DateTime _dateTime;
        private Guid _reservationId;
        private Exception _exception;

        protected override void Given()
        {
            _dateTime = new DateTime(2018, 1, 12);
            _reservationId = Guid.NewGuid();

            _sut = new Amenity();

            _sut.GetQuantity(_dateTime).Should().Be(0);
        }

        protected override void When()
        {
            try
            {
                _sut.Take(_dateTime, 6, _reservationId);
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }

        [Fact]
        public void Then_Should_Throw_An_Exception()
        {
            _exception.Should().BeOfType<NoStockAvailableException>();
        }
    }
}