﻿using System;
using System.Linq;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Inventory.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class When_Creating : Given_When_Then_Test
    {
        private Amenity _sut;

        protected override void Given()
        {
        }

        protected override void When()
        {
            _sut = new Amenity();
        }

        [Fact]
        public void Then_Amenity_Should_Contain_Amenity_Created_Event()
        {
            var testEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(testEventDispatcher)
                .Wait();
            var createdEvent = testEventDispatcher.Events.OfType<AmenityCreated>().Single();

            createdEvent.AggregateId.Should().NotBe(Guid.Empty);

            createdEvent
                .Should()
                .BeEquivalentTo(new AmenityCreated(createdEvent.AggregateId));
        }
    }
}