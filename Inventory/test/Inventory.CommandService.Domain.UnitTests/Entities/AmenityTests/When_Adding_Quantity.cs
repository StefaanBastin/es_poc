﻿using System;
using System.Linq;
using FluentAssertions;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Inventory.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class When_Adding_Quantity : Given_When_Then_Test
    {
        private Amenity _sut;
        private DateTime _date;

        protected override void Given()
        {
            _sut = new Amenity();
            _date = new DateTime(2018, 4, 26);
            _sut.GetQuantity(_date).Should().Be(0);
        }

        protected override void When()
        {
            _sut.AddQuantity(_date, 28);
        }

        [Fact]
        public void Then_Quantity_Should_Be_Increased()
        {
            _sut.GetQuantity(_date).Should().Be(28);
        }

        [Fact]
        public void Then_The_Amenity_Should_Contain_The_Quantity_Added_Event()
        {
            var testEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(testEventDispatcher)
                .Wait();

            testEventDispatcher
                .Events
                .OfType<QuantityAdded>()
                .Single()
                .Should()
                .BeEquivalentTo(new QuantityAdded(_sut.Id, _date, 28));
        }
    }
}