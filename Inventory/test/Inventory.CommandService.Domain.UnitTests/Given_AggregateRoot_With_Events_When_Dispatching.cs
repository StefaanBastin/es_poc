﻿using System;
using System.Collections.Generic;
using Inventory.CommandService.TestInfra;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests
{
    public class Given_AggregateRoot_With_Events_When_Dispatching : Given_When_Then_Test
    {
        private TestAggregate _sut;

        protected override void Given()
        {
            _sut = new TestAggregate();
            _sut.AddTestEvent();
        }

        protected override void When()
        {
            _sut.DispatchAsync(new EventDispatcher(new List<IEventConsumer>()))
                .Wait();
        }

        [Fact]
        public void Then_Should_Not_Allow_Consumed_Events_To_Be_Consumed_Again()
        {
            var failingEventConsumerMock = new Mock<IEventConsumer>();
            failingEventConsumerMock
                .Setup(x => x.ConsumeAsync(It.IsAny<Event>(), It.IsAny<Type>()))
                .Callback((Event e, Type t) => throw new InvalidOperationException($"Tried to consume {e.GetType().Name} of {t.Name} a second time. " +
                                                                                   "This should not occur because the events should be cleared after dispatching."));

            _sut.DispatchAsync(new EventDispatcher(new List<IEventConsumer> { failingEventConsumerMock.Object }))
                .Wait();
        }
    }
}