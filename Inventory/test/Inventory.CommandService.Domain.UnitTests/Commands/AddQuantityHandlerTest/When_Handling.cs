﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Commands.AddQuantityHandlerTest
{
    public class When_Handling : Given_When_Then_Test
    {
        private readonly List<Event> _events = new List<Event>();
        private AddQuantityHandler _sut;
        private Amenity _amenity;

        protected override void Given()
        {
            _amenity = new Amenity();
            var amenityRepositoryMock = new Mock<IRepository<Amenity>>();
            amenityRepositoryMock
                .Setup(repo => repo.GetAsync(_amenity.Id))
                .Returns(Task.FromResult(_amenity));

            var eventConsumerMock = new Mock<IEventConsumer>();
            eventConsumerMock
                .Setup(consumer => consumer.ConsumeAsync(It.IsAny<QuantityAdded>(), typeof(Amenity)))
                .Returns(Task.CompletedTask)
                .Callback<Event, Type>((evt, source) => _events.Add((QuantityAdded)evt));

            var eventDispatcher = new EventDispatcher(new[] { eventConsumerMock.Object });

            _sut = new AddQuantityHandler(amenityRepositoryMock.Object, eventDispatcher);
        }

        protected override void When()
        {
            _sut.HandleAsync(new AddQuantity(_amenity.Id, new DateTime(2066, 3, 30), 623)).Wait();
        }

        [Fact]
        public void Then_Quantity_Should_Be_Increased()
        {
            _amenity.GetQuantity(new DateTime(2066, 3, 30)).Should().Be(623);
        }

        [Fact]
        public void Then_Dispatch_AmenityBooked_To_Event_Consumers()
        {
            _events.Should().BeEquivalentTo(new QuantityAdded(_amenity.Id, new DateTime(2066, 3, 30), 623));
        }
    }
}