﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Inventory.CommandService.TestInfra;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Commands.ReturnAmenityHandlerTests
{
    public class When_Handling : Given_When_Then_Test
    {
        private Guid _amenityId;
        private DateTime _date;
        private int _quantity;

        private ReturnAmenityHandler _sut;
        private Amenity _amenity;
        private TestEventDispatcher _testEventDispatcher;

        protected override void Given()
        {
            _date = new DateTime(2018, 5, 7);

            _amenityId = Guid.NewGuid();
            _amenity = new Amenity(new List<Event>
            {
                new AmenityCreated(_amenityId),
                new QuantityAdded(_amenityId, _date, 5)
            });

            _quantity = 156;

            var amenityRepositoryMock = new Mock<IRepository<Amenity>>();
            amenityRepositoryMock
                .Setup(repo => repo.GetAsync(_amenity.Id))
                .Returns(Task.FromResult(_amenity));

            _testEventDispatcher = new TestEventDispatcher();

            _sut = new ReturnAmenityHandler(amenityRepositoryMock.Object, _testEventDispatcher);
        }

        protected override void When()
        {
            _sut.HandleAsync(new ReturnAmenity(_amenityId, _date, _quantity)).Wait();
        }

        [Fact]
        public void Then_The_Amenity_Quantity_Should_Be_Increased()
        {
            _amenity.GetQuantity(_date).Should().Be(161);
        }

        [Fact]
        public void Then_Dispatch_AmenityReturned_To_Event_Consumers()
        {
            _testEventDispatcher.Events.Last().Should()
                .BeEquivalentTo(new AmenityReturned(_amenityId, _date, _quantity));
        }
    }
}