﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Commands.TakeAmenityHandlerTests
{
    public class When_Handling : Given_When_Then_Test
    {
        private readonly IList<AmenityTaken> _events = new List<AmenityTaken>();
        private readonly Guid _reservationId = Guid.NewGuid();
        private TakeAmenityHandler _sut;
        private Amenity _amenity;

        protected override void Given()
        {
            var amenityId = Guid.NewGuid();
            _amenity = new Amenity(
                new List<Event>
                {
                    new AmenityCreated(amenityId),
                    new QuantityAdded(amenityId, new DateTime(2018, 4, 27), 10),
                });

            var amenityRepositoryMock = new Mock<IRepository<Amenity>>();
            amenityRepositoryMock
                .Setup(repo => repo.GetAsync(_amenity.Id))
                .Returns(Task.FromResult(_amenity));

            var eventConsumerMock = new Mock<IEventConsumer>();
            eventConsumerMock
                .Setup(consumer => consumer.ConsumeAsync(It.IsAny<AmenityTaken>(), typeof(Amenity)))
                .Returns(Task.CompletedTask)
                .Callback<Event, Type>((evt, source) => _events.Add((AmenityTaken)evt));
            var eventDispatcher = new EventDispatcher(new[] { eventConsumerMock.Object });

            _sut = new TakeAmenityHandler(amenityRepositoryMock.Object, eventDispatcher, default(IRejectedCommandHandler<TakeAmenity>));
        }

        protected override void When()
        {
            _sut.HandleAsync(new TakeAmenity(_amenity.Id, new DateTime(2018, 4, 27), 3, _reservationId)).Wait();
        }

        [Fact]
        public void Then_The_Amenity_Quantity_Should_Be_Reduced()
        {
            _amenity.GetQuantity(new DateTime(2018, 4, 27)).Should().Be(7);
        }

        [Fact]
        public void Then_Dispatch_AmenityBooked_To_Event_Consumers()
        {
            _events.Should().BeEquivalentTo(new List<AmenityTaken>
            {
                new AmenityTaken(_amenity.Id, new DateTime(2018, 4, 27), 3, _reservationId),
            });
        }
    }
}