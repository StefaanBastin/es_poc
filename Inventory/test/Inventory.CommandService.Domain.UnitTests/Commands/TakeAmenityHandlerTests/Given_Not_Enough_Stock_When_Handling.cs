﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Entities;
using Inventory.CommandService.Domain.Events;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Commands.TakeAmenityHandlerTests
{
    public class Given_Not_Enough_Stock_When_Handling : Given_When_Then_Test
    {
        private readonly IList<AmenityTaken> _events = new List<AmenityTaken>();
        private readonly Guid _reservationId = Guid.NewGuid();
        private TakeAmenityHandler _sut;
        private Amenity _amenity;
        private TakeAmenity _failedCommand;
        private TakeAmenity _command;
        private DateTime _date;

        protected override void Given()
        {
            var amenityId = Guid.NewGuid();
            _amenity = new Amenity(
                new List<Event>
                {
                    new AmenityCreated(amenityId),
                    new QuantityAdded(amenityId, new DateTime(2018, 4, 26), 3),
                });

            var amenityRepositoryMock = new Mock<IRepository<Amenity>>();
            amenityRepositoryMock
                .Setup(repo => repo.GetAsync(_amenity.Id))
                .Returns(Task.FromResult(_amenity));

            var eventConsumerMock = new Mock<IEventConsumer>();
            eventConsumerMock
                .Setup(consumer => consumer.ConsumeAsync(It.IsAny<AmenityTaken>(), typeof(Amenity)))
                .Returns(Task.CompletedTask)
                .Callback<Event, Type>((evt, source) => _events.Add((AmenityTaken)evt));
            var eventDispatcher = new EventDispatcher(new[] { eventConsumerMock.Object });

            var externalMessengerMock = new Mock<IRejectedCommandHandler<TakeAmenity>>();
            externalMessengerMock
                .Setup(x => x.Reject(It.IsAny<TakeAmenity>()))
                .Returns(Task.CompletedTask)
                .Callback((TakeAmenity cmd) => { _failedCommand = cmd; });

            _date = new DateTime(2018, 4, 26);
            _command = new TakeAmenity(_amenity.Id, _date, 6, _reservationId);

            _sut = new TakeAmenityHandler(amenityRepositoryMock.Object, eventDispatcher, externalMessengerMock.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(new TakeAmenity(_amenity.Id, _date, 6, _reservationId)).Wait();
        }

        [Fact]
        public void Then_The_Amenity_Quantity_Should_Be_Unchanged_If_Not_Enough_Stock()
        {
            _amenity.GetQuantity(new DateTime(2018, 4, 26)).Should().Be(3);
        }

        [Fact]
        public void Then_Should_Send_FailAmenityBookingCommand_To_External_Systems()
        {
            _failedCommand.Should().BeEquivalentTo(_command);
        }
    }
}