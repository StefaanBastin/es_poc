﻿using System;
using FluentAssertions;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Policies;
using Inventory.CommandService.Domain.ReservationEvents;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Policies.ReturnAmenityPolicyTests
{
    public class When_Handling_AmenityBookingReleased : Given_When_Then_Test
    {
        private Guid _amenityId;
        private Guid _reservationId;
        private DateTime _date;
        private int _quantity;

        private Mock<ICommandHandler<ReturnAmenity>> _commandHandlerMock;
        private ReturnAmenityPolicy _sut;
        private ReturnAmenity _returnAmenity;

        protected override void Given()
        {
            _amenityId = Guid.NewGuid();
            _reservationId = Guid.NewGuid();
            _date = new DateTime(2018, 5, 7);
            _quantity = 156;

            _commandHandlerMock = new Mock<ICommandHandler<ReturnAmenity>>();
            _commandHandlerMock
                .Setup(x => x.HandleAsync(It.IsAny<ReturnAmenity>()))
                .Callback((ReturnAmenity cmd) => { _returnAmenity = cmd; });

            _sut = new ReturnAmenityPolicy(_commandHandlerMock.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(new AmenityBookingReleased(_amenityId, _reservationId, _date, _quantity));
        }

        [Fact]
        public void Then_Should_Return_Amenity()
        {
            _returnAmenity.Should().BeEquivalentTo(new ReturnAmenity(_amenityId, _date, _quantity));
        }
    }
}