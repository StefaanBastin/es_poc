﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Inventory.CommandService.Domain.Commands;
using Inventory.CommandService.Domain.Dtos;
using Inventory.CommandService.Domain.Policies;
using Inventory.CommandService.Domain.ReservationEvents;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Domain.UnitTests.Policies.TakeAmenityPolicyTests
{
    public class When_Handling_ReservationCreated : Given_When_Then_Test
    {
        private TakeAmenityPolicy _sut;
        private Mock<ICommandHandler<TakeAmenity>> _commandHandlerMock;
        private TakeAmenity _takeAmenity;
        private Guid _amenityId;
        private Guid _reservationId;
        private List<AmenityDto> _amenities;
        private DateTime _date;

        protected override void Given()
        {
            _commandHandlerMock = new Mock<ICommandHandler<TakeAmenity>>();
            _commandHandlerMock
                .Setup(x => x.HandleAsync(It.IsAny<TakeAmenity>()))
                .Returns(Task.CompletedTask)
                .Callback((TakeAmenity cmd) => { _takeAmenity = cmd; });

            _date = new DateTime(2018, 5, 1);

            _reservationId = Guid.NewGuid();

            _amenityId = Guid.NewGuid();
           _amenities = new List<AmenityDto>
            {
                new AmenityDto(_amenityId, _date, 2)
            };

            _sut = new TakeAmenityPolicy(_commandHandlerMock.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(new ReservationCreated(_reservationId, Guid.NewGuid(), _amenities)).Wait();
        }

        [Fact]
        public void Then_Should_Book_Amenity()
        {
            _takeAmenity.Should().BeEquivalentTo(new TakeAmenity(_amenityId, _date, 2, _reservationId));
        }
    }
}