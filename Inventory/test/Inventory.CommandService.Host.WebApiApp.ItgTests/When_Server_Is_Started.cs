﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Inventory.CommandService.Host.WebApi.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using ToolBelt.TestSupport;
using Xunit;

namespace Inventory.CommandService.Host.WebApiApp.ItgTests
{
    public class When_Server_Is_Started : Given_When_Then_Test
    {
        private IEnumerable<Type> _controllerTypes;
        private TestServer _server;

        protected override void Given()
        {
            var controllerAssembly = Assembly.GetAssembly(typeof(ValuesController));
            _controllerTypes = GetControllerTypes(controllerAssembly);
        }

        protected override void When()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
        }

        [Fact]
        public void Then_Should_Be_Able_To_Resolve_All_Controllers()
        {
            _controllerTypes
                .ToList()
                .ForEach(controller => _server.Host.Services.GetRequiredService(controller));
        }

        private IEnumerable<Type> GetControllerTypes(Assembly assembly)
        {
            return assembly.GetTypes().Where(type => type.IsDefined(typeof(RouteAttribute)) ||
                                                     type.IsDefined(typeof(ControllerAttribute)) ||
                                                     type.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase));
        }
    }
}