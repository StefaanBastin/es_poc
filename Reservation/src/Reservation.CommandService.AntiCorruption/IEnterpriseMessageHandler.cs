﻿using System;

namespace Reservation.CommandService.AntiCorruption
{
    public interface IEnterpriseMessageHandler<in TMessage> : IObserver<TMessage>
    {
    }
}