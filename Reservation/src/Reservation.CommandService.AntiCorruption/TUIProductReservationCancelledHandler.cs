﻿using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using TO.CommercialYield.CanonicalModels;

namespace Reservation.CommandService.AntiCorruption
{
    public class TUIProductReservationCancelledHandler : IEnterpriseMessageSubscriber, IEnterpriseMessageHandler<TUIProductReservationEvent>
    {
        private readonly ICommandHandler<CancelReservation> _cancelReservationHandler;
        private readonly IEnterpriseMessageReceiver<TUIProductReservationEvent> _enterpriseReceiver;
        private readonly ILogger<TUIProductReservationCancelledHandler> _logger;
        private IDisposable _subscription;

        public TUIProductReservationCancelledHandler(
            ICommandHandler<CancelReservation> cancelReservationHandler,
            IEnterpriseMessageReceiver<TUIProductReservationEvent> enterpriseReceiver,
            ILogger<TUIProductReservationCancelledHandler> logger)
        {
            _cancelReservationHandler = cancelReservationHandler;
            _enterpriseReceiver = enterpriseReceiver;
            _logger = logger;
        }

        public void Start()
        {
            _subscription = _enterpriseReceiver.Subscribe(this);
        }

        public void OnCompleted()
        {
            _subscription.Dispose();
        }

        public void OnError(Exception error)
        {
            _logger.LogError(error, error.Message);
        }

        public void OnNext(TUIProductReservationEvent reservationEvent)
        {
            if (reservationEvent.eventType != EventType.TUIProductReservationCancelled ||
                reservationEvent.payload.TUIProductReservation.reservationSegments.Any(segment => segment.state != SegmentStatus.Cancelled))
            {
                return;
            }

            var reservationId = new Guid(reservationEvent.payload.TUIProductReservation.reservationGUID);
            _cancelReservationHandler.HandleAsync(new CancelReservation(reservationId)).Wait();
        }
    }
}