﻿using System;
using System.Threading.Tasks;

namespace Reservation.CommandService.AntiCorruption
{
    public interface IEnterpriseSender
    {
        Task Send(Type source, object message);
    }
}