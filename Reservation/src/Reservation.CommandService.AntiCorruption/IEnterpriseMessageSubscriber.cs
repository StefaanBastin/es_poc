﻿namespace Reservation.CommandService.AntiCorruption
{
    public interface IEnterpriseMessageSubscriber
    {
        void Start();
    }
}