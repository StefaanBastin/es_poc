﻿using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using TO.CommercialYield.CanonicalModels;

namespace Reservation.CommandService.AntiCorruption
{
    public class TUIProductReservationCreatedHandler : IEnterpriseMessageSubscriber, IEnterpriseMessageHandler<TUIProductReservationEvent>
    {
        private readonly ICommandHandler<CreateReservation> _createReservationHandler;
        private readonly IEnterpriseMessageReceiver<TUIProductReservationEvent> _enterpriseReceiver;
        private readonly ILogger<TUIProductReservationCreatedHandler> _logger;
        private IDisposable _subscription;

        public TUIProductReservationCreatedHandler(
            ICommandHandler<CreateReservation> createReservationHandler,
            IEnterpriseMessageReceiver<TUIProductReservationEvent> enterpriseReceiver,
            ILogger<TUIProductReservationCreatedHandler> logger)
        {
            _createReservationHandler = createReservationHandler;
            _enterpriseReceiver = enterpriseReceiver;
            _logger = logger;
        }

        public void Start()
        {
            _subscription = _enterpriseReceiver.Subscribe(this);
        }

        public void OnCompleted()
        {
            _subscription.Dispose();
        }

        public void OnError(Exception error)
        {
            _logger.LogError(error, error.Message);
        }

        public void OnNext(TUIProductReservationEvent reservationEvent)
        {
            if (reservationEvent.eventType != EventType.TUIProductReservationCreated ||
                reservationEvent.payload.TUIProductReservation.reservationSegments.Any(segment => segment.state != SegmentStatus.Created))
            {
                return;
            }

            var productId = new Guid(reservationEvent.payload.TUIProductReservation.productGUID);

            var amenities = reservationEvent.payload.TUIProductReservation.reservationSegments
                .Select(segment => new
                {
                    Period = new Period(segment.scheduledStartDate, segment.scheduledEndDate),
                    Quantity = segment.assignments.Length,
                    AmenityDetails = segment.amenityDetails.amenityDetail
                })
                .SelectMany(
                    s => s.Period.GetDates(),
                    (s, d) => new
                    {
                        Date = d,
                        s.Quantity,
                        s.AmenityDetails
                    })
                .SelectMany(
                    s => s.AmenityDetails,
                    (s, ad) => new AmenityDto(new Guid(ad.amenityGUID), s.Date, s.Quantity));

            var createReservation = new CreateReservation(productId, amenities);

            _createReservationHandler.HandleAsync(createReservation).Wait();
        }
    }
}