﻿using System;

namespace Reservation.CommandService.AntiCorruption
{
    public interface IEnterpriseMessageReceiver<out TMessage> : IObservable<TMessage>
    {
    }
}