﻿using System;

namespace Reservation.CommandService.Application
{
    public interface IBusinessContextMessageReceiver<out TMessage> : IObservable<TMessage>
    {
    }
}