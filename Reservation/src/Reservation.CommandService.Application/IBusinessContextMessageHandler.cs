﻿using System;

namespace Reservation.CommandService.Application
{
    public interface IBusinessContextMessageHandler<in TMessage> : IObserver<TMessage>
    {
    }
}