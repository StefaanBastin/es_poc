﻿using System;
using System.Threading.Tasks;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.Application
{
    public interface IInternalEventMessenger
    {
        Task Send(Type source, Event evt);
    }
}