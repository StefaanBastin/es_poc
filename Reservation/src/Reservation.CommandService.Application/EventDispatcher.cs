﻿using System;
using System.Threading.Tasks;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.Application
{
    public class EventDispatcher : IEventDispatcher
    {
        private readonly IEventRepository _eventRepository;
        private readonly IInternalEventMessenger _internalEventMessenger;

        public EventDispatcher(IEventRepository eventRepository, IInternalEventMessenger internalEventMessenger)
        {
            _eventRepository = eventRepository;

            _internalEventMessenger = internalEventMessenger;
        }

        public async Task DispatchAsync(Event evt, Type source)
        {
            await _eventRepository.SaveAsync(evt, source);
            await _internalEventMessenger.Send(source, evt);
        }
    }
}