﻿using System;
using Reservation.CommandService.Messages;

namespace Reservation.CommandService.Application
{
    public class FailAmenityBookingMessageHandler : IBusinessContextMessageHandler<FailAmenityBooking>
    {
        public void OnCompleted()
        {
        }

        public void OnError(Exception error)
        {
        }

        public void OnNext(FailAmenityBooking value)
        {
        }
    }
}