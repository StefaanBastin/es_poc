﻿using System.Threading.Tasks;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Messages;

namespace Reservation.CommandService.Application
{
    public class AmenityBookingReleasedEventMessageSender : IEventHandler<AmenityBookingReleased>
    {
        private readonly IBusinessContextSender _businessContextSender;

        public AmenityBookingReleasedEventMessageSender(IBusinessContextSender businessContextSender)
        {
            _businessContextSender = businessContextSender;
        }

        public Task HandleAsync(AmenityBookingReleased evt)
        {
            var message = new AmenityBookingReleasedEventMessage
            {
                AmenityId = evt.AmenityId,
                Date = evt.Date,
                Quantity = evt.Quantity
            };

            return _businessContextSender.Send(message.GetType(), message);
        }
    }
}