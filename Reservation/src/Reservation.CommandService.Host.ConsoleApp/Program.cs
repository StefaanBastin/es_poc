﻿using Autofac;
using Autofac.Builder;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using Reservation.CommandService.Host.ConsoleApp.Container;

namespace Reservation.CommandService.Host.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LogManager.LoadConfiguration("nlog.config");

            var container = GetContainer();

            var logger = container.Resolve<ILogger<Program>>();
            logger.LogInformation("Starting application");

            LogManager.Shutdown();
        }

        private static IContainer GetContainer()
        {
            var services = new ServiceCollection();

            services.AddLogging(builder => builder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace));

            var containerBuilder = new ContainerBuilderFactory().Create();
            containerBuilder.Populate(services);
            var container = containerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            var loggerFactory = container.Resolve<ILoggerFactory>();

            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            LogManager.LoadConfiguration("nlog.config");

            return container;
        }
    }
}
