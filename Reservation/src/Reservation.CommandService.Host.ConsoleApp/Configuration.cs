﻿using System.IO;
using System.Net;
using Autofac;
using Microsoft.Extensions.Configuration;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;
using Reservation.CommandService.Infra.ActiveMq.Receiver;

namespace Reservation.CommandService.Host.ConsoleApp
{
    public class Configuration : IStartable, IEventStoreConfiguration, IActiveMqConfiguration, IActiveMqReceiverConfiguration
    {
        private IConfigurationRoot _configuration;

        public void Start()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            _configuration = builder.Build();

            EventStoreEndPoint = new IPEndPoint(IPAddress.Parse(_configuration["EventStore:Address"]), int.Parse(_configuration["EventStore:Port"]));
        }

        public int ListenTimeoutInMilliseconds => int.Parse(_configuration["ActiveMq:ListenTimeoutInMilliseconds"]);

        public IPEndPoint EventStoreEndPoint { get; private set; }

        public string BusinessContextActiveMqUri => _configuration["ActiveMq:BusinessContextUri"];
        public string EnterpriseActiveMqUri => _configuration["ActiveMq:EnterpriseUri"];
    }
}