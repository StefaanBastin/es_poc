﻿using System.Collections.Generic;
using Autofac;
using Reservation.CommandService.AntiCorruption;

namespace Reservation.CommandService.Host.ConsoleApp
{
    public class AntiCorruptionStartable : IStartable
    {
        private readonly IEnumerable<IEnterpriseMessageSubscriber> _enterpriseMessageHandlers;

        public AntiCorruptionStartable(IEnumerable<IEnterpriseMessageSubscriber> enterpriseMessageHandlers)
        {
            _enterpriseMessageHandlers = enterpriseMessageHandlers;
        }

        public void Start()
        {
            foreach (var enterpriseMessageHandler in _enterpriseMessageHandlers)
            {
                enterpriseMessageHandler.Start();
            }
        }
    }
}