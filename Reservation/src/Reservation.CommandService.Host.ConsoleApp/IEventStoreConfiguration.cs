﻿using System.Net;

namespace Reservation.CommandService.Host.ConsoleApp
{
    public interface IEventStoreConfiguration
    {
        IPEndPoint EventStoreEndPoint { get; }
    }
}