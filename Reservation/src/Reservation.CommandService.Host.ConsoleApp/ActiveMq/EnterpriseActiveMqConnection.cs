﻿using System;
using Amqp;
using Reservation.CommandService.Infra.ActiveMq;

namespace Reservation.CommandService.Host.ConsoleApp.ActiveMq
{
    public class EnterpriseActiveMqConnection : Autofac.IStartable, IDisposable
    {
        private readonly IActiveMqConfiguration _configuration;
        private readonly ActiveMqConnectionPool _activeMqConnectionPool;
        private Connection _connection;

        public EnterpriseActiveMqConnection(IActiveMqConfiguration configuration, ActiveMqConnectionPool activeMqConnectionPool)
        {
            _configuration = configuration;
            _activeMqConnectionPool = activeMqConnectionPool;
        }

        public void Start()
        {
            var address = new Address(_configuration.EnterpriseActiveMqUri);
            _connection = new Connection(address);
            _activeMqConnectionPool.AddSession(ActiveMqInstances.Enterprise, new Session(_connection));
        }

        public void Dispose()
        {
            _connection.Close();
        }
    }
}