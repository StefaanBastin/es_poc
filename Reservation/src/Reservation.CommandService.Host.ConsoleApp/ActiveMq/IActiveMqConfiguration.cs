﻿namespace Reservation.CommandService.Host.ConsoleApp.ActiveMq
{
    public interface IActiveMqConfiguration
    {
        string BusinessContextActiveMqUri { get; }
        string EnterpriseActiveMqUri { get; }
    }
}