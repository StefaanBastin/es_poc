﻿using System;
using Amqp;
using Reservation.CommandService.Infra.ActiveMq;

namespace Reservation.CommandService.Host.ConsoleApp.ActiveMq
{
    public class BusinessContextActiveMqConnection : Autofac.IStartable, IDisposable
    {
        private readonly IActiveMqConfiguration _configuration;
        private readonly ActiveMqConnectionPool _activeMqConnectionPool;
        private Connection _connection;

        public BusinessContextActiveMqConnection(IActiveMqConfiguration configuration, ActiveMqConnectionPool activeMqConnectionPool)
        {
            _configuration = configuration;
            _activeMqConnectionPool = activeMqConnectionPool;
        }

        public void Start()
        {
            var address = new Address(_configuration.BusinessContextActiveMqUri);
            _connection = new Connection(address);
            _activeMqConnectionPool.AddSession(ActiveMqInstances.BusinessContext, new Session(_connection));
        }

        public void Dispose()
        {
            _connection.Close();
        }
    }
}