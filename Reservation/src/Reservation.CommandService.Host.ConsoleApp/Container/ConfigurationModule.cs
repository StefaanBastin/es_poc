﻿using Autofac;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<Configuration>()
                .As<IEventStoreConfiguration>()
                .As<IActiveMqConfiguration>()
                .As<IStartable>()
                .SingleInstance();
        }
    }
}