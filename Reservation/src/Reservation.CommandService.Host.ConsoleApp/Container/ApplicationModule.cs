﻿using Autofac;
using Reservation.CommandService.Application;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EventDispatcher>().As<IEventDispatcher>();

            builder.RegisterType<AmenityBookingReleasedEventMessageSender>().As<IEventHandler>();
        }
    }
}