﻿using Amqp;
using Autofac;
using Reservation.CommandService.AntiCorruption;
using Reservation.CommandService.Application;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;
using Reservation.CommandService.Infra.ActiveMq;
using Reservation.CommandService.Infra.ActiveMq.Receiver;
using Reservation.CommandService.Infra.ActiveMq.Sender;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class ActiveMqModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<BusinessContextActiveMqConnection>()
                .As<IStartable>()
                .SingleInstance();

            builder
                .RegisterType<BusinessContextActiveMqSender>()
                .As<IBusinessContextSender>()
                .SingleInstance();

            builder
                .RegisterType<EnterpriseActiveMqConnection>()
                .As<IStartable>()
                .SingleInstance();

            builder
                .RegisterType<EnterpriseActiveMqSender>()
                .As<IEnterpriseSender>()
                .SingleInstance();

            builder
                .RegisterType<ActiveMqSender>()
                .SingleInstance();

            builder
                .RegisterType<ActiveMqConnectionPool>()
                .SingleInstance();

            builder
                .RegisterGeneric(typeof(ActiveMqReceiver<>))
                .WithParameter((pi, cc) => pi.ParameterType == typeof(Session), (pi, cc) => cc.Resolve<ActiveMqConnectionPool>().GetSession(ActiveMqInstances.Enterprise))
                .As(typeof(IEnterpriseMessageReceiver<>));

            builder
                .RegisterGeneric(typeof(ActiveMqReceiver<>))
                .WithParameter((pi, cc) => pi.ParameterType == typeof(Session), (pi, cc) => cc.Resolve<ActiveMqConnectionPool>().GetSession(ActiveMqInstances.BusinessContext))
                .As(typeof(IBusinessContextMessageReceiver<>));
        }
    }
}