﻿using Autofac;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class ContainerBuilderFactory
    {
        public ContainerBuilder Create()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new ConfigurationModule());
            containerBuilder.RegisterModule(new EventStoreModule());
            containerBuilder.RegisterModule(new ActiveMqModule());
            containerBuilder.RegisterModule(new DomainModule());
            containerBuilder.RegisterModule(new InMemoryBusModule());
            containerBuilder.RegisterModule(new ApplicationModule());
            containerBuilder.RegisterModule(new LoggingModule());

            return containerBuilder;
        }
    }
}