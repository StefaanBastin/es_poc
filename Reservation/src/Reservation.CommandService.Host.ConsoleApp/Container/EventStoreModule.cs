﻿using Autofac;
using EventStore.ClientAPI;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Infra.EventStore;
using Reservation.CommandService.Infra.EventStore.Converters;
using Module = Autofac.Module;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class EventStoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<EventStoreConnectionCreator>()
                .AsSelf()
                .As<IStartable>()
                .SingleInstance();

            builder
                .RegisterType<AggregateEventVersionCache>()
                .As<IAggregateEventVersionCache>()
                .InstancePerLifetimeScope();

            builder
                .Register(context => context.Resolve<EventStoreConnectionCreator>().Connection)
                .As<IEventStoreConnection>()
                .SingleInstance();

            builder
                .RegisterType<StreamNameGenerator>()
                .As<IStreamNameGenerator>();

            builder
                .RegisterType<UTF8ByteArrayConverter>()
                .As<IByteArrayConverter>();

            builder
                .RegisterType<EventRepository>()
                .As<IEventRepository>();
        }
    }
}