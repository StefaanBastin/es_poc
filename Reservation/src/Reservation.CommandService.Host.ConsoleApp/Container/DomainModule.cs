﻿using Autofac;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Entities;
using Module = Autofac.Module;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<BookedReservationRepository>()
                .As<IRepository<BookedReservation>>();

            builder
                .RegisterType<ReservationRepository>()
                .As<IRepository<Domain.Entities.Reservation>>();
        }
    }
}