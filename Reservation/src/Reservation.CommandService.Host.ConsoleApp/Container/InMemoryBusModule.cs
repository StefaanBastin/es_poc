﻿using Autofac;
using Reservation.CommandService.Application;
using Reservation.CommandService.Infra.InMemoryBus;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class InMemoryBusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<InMemoryBus>().As<IInternalEventMessenger>().SingleInstance();
        }
    }
}