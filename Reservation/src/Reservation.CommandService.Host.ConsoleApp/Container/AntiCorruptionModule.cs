﻿using Autofac;
using Reservation.CommandService.AntiCorruption;
using TO.CommercialYield.CanonicalModels;

namespace Reservation.CommandService.Host.ConsoleApp.Container
{
    public class AntiCorruptionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<AntiCorruptionStartable>()
                .As<IStartable>()
                .SingleInstance();

            builder
                .RegisterType<TUIProductReservationCreatedHandler>()
                .As<IEnterpriseMessageSubscriber>()
                .As<IEnterpriseMessageHandler<TUIProductReservationEvent>>()
                .SingleInstance();

            builder
                .RegisterType<TUIProductReservationCancelledHandler>()
                .As<IEnterpriseMessageSubscriber>()
                .As<IEnterpriseMessageHandler<TUIProductReservationEvent>>()
                .SingleInstance();
        }
    }
}