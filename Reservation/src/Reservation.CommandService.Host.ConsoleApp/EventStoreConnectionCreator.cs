﻿using System;
using Autofac;
using EventStore.ClientAPI;

namespace Reservation.CommandService.Host.ConsoleApp
{
    public class EventStoreConnectionCreator : IStartable, IDisposable
    {
        public IEventStoreConnection Connection { get; private set; }

        private readonly IEventStoreConfiguration _eventStoreConfiguration;

        public EventStoreConnectionCreator(IEventStoreConfiguration eventStoreConfiguration)
        {
            _eventStoreConfiguration = eventStoreConfiguration;
        }

        public void Start()
        {
            Connection = EventStoreConnection.Create(_eventStoreConfiguration.EventStoreEndPoint);
            Connection.ConnectAsync().Wait();
        }

        public void Dispose()
        {
            Connection?.Dispose();
        }
    }
}