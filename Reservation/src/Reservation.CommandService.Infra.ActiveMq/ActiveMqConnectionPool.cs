﻿using System.Collections.Generic;
using Amqp;

namespace Reservation.CommandService.Infra.ActiveMq
{
    public class ActiveMqConnectionPool
    {
        private readonly IDictionary<string, Session> _pool = new Dictionary<string, Session>();

        public Session GetSession(string sessionKey)
        {
            return _pool[sessionKey];
        }

        public void AddSession(string sessionKey, Session session)
        {
            _pool[sessionKey] = session;
        }
    }
}