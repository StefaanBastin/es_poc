﻿using System;

namespace Reservation.CommandService.Infra.ActiveMq
{
    public interface IMessageTypeToAddressMapper
    {
        string Map(Type messageType);
    }
}