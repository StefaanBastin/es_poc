﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Reservation.CommandService.Infra.ActiveMq
{
    public class MessageTypeToAddressMapper : IMessageTypeToAddressMapper
    {
        private static readonly IReadOnlyDictionary<Type, string> Lookup = new ReadOnlyDictionary<Type, string>(
            new Dictionary<Type, string>());

        public string Map(Type messageType)
        {
            return Lookup[messageType];
        }
    }
}