﻿using System;
using System.Collections.Generic;
using Amqp;
using Newtonsoft.Json;
using Reservation.CommandService.AntiCorruption;
using Reservation.CommandService.Application;

namespace Reservation.CommandService.Infra.ActiveMq.Receiver
{
    public class ActiveMqReceiver<TMessage> : IBusinessContextMessageReceiver<TMessage>, IEnterpriseMessageReceiver<TMessage>
    {
        private readonly int _listenTimeoutInMilliseconds;
        private readonly Session _session;
        private readonly IMessageTypeToAddressMapper _messageTypeToAddressMapper;
        private readonly List<IObserver<TMessage>> _observers = new List<IObserver<TMessage>>();
        private bool _stopped;

        public ActiveMqReceiver(
            Session session,
            IActiveMqReceiverConfiguration activeMqReceiverConfiguration,
            IMessageTypeToAddressMapper messageTypeToAddressMapper)
        {
            _session = session;
            _messageTypeToAddressMapper = messageTypeToAddressMapper;
            _listenTimeoutInMilliseconds = activeMqReceiverConfiguration.ListenTimeoutInMilliseconds;
        }

        private async void Start()
        {
            var receiverLink = new ReceiverLink(_session, "receiverLink", _messageTypeToAddressMapper.Map(typeof(TMessage)));

            while (!_stopped)
            {
                var message = await receiverLink.ReceiveAsync(new TimeSpan(0, 0, 0, 0, _listenTimeoutInMilliseconds));

                if (_stopped)
                {
                    return;
                }

                if (message != null)
                {
                    HandleMessage(message);
                    receiverLink.Accept(message);
                }
            }
        }

        private void HandleMessage(Message message)
        {
            var m = JsonConvert.DeserializeObject<TMessage>(message.Body as string);
            foreach (var observer in _observers)
            {
                observer.OnNext(m);
            }
        }

        private void Stop()
        {
            _stopped = true;
        }

        public IDisposable Subscribe(IObserver<TMessage> observer)
        {
            if (!_observers.Contains(observer))
            {
                _observers.Add(observer);
            }

            if (_observers.Count == 1)
            {
                Start();
            }

            var subscription = new Subscription(observer);
            subscription.Disposed += Unsubscribe;
            return subscription;
        }

        private void Unsubscribe(IObserver<TMessage> observer, Subscription subscription)
        {
            if (_observers.Contains(observer))
            {
                _observers.Remove(observer);
            }

            if (_observers.Count == 0)
            {
                Stop();
            }

            subscription.Disposed -= Unsubscribe;
        }

        private class Subscription : IDisposable
        {
            private readonly IObserver<TMessage> _observer;
            public event Action<IObserver<TMessage>, Subscription> Disposed = (message, subscription) => { };

            public Subscription(IObserver<TMessage> observer)
            {
                _observer = observer;
            }

            public void Dispose()
            {
                Disposed(_observer, this);
            }
        }
    }
}