﻿namespace Reservation.CommandService.Infra.ActiveMq.Receiver
{
    public interface IActiveMqReceiverConfiguration
    {
        int ListenTimeoutInMilliseconds { get; }
    }
}