﻿using System;
using System.Threading.Tasks;
using Reservation.CommandService.AntiCorruption;

namespace Reservation.CommandService.Infra.ActiveMq.Sender
{
    public class EnterpriseActiveMqSender : IEnterpriseSender
    {
        private readonly ActiveMqSender _activeMqSender;

        public EnterpriseActiveMqSender(ActiveMqSender activeMqSender)
        {
            _activeMqSender = activeMqSender;
        }

        public Task Send(Type source, object message)
        {
            return _activeMqSender.Send(source, message, ActiveMqInstances.Enterprise);
        }
    }
}