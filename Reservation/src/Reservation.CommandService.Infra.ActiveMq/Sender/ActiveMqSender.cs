﻿using System;
using System.Threading.Tasks;
using Amqp;
using Newtonsoft.Json;

namespace Reservation.CommandService.Infra.ActiveMq.Sender
{
    public class ActiveMqSender
    {
        private readonly ActiveMqConnectionPool _activeMqConnectionPool;

        public ActiveMqSender(ActiveMqConnectionPool activeMqConnectionPool)
        {
            _activeMqConnectionPool = activeMqConnectionPool;
        }

        public Task Send(Type source, object message, string sessionKey)
        {
            var senderLink = new SenderLink(_activeMqConnectionPool.GetSession(sessionKey), "senderLink", source.Name);
            var json = JsonConvert.SerializeObject(message);
            var amqpMessage = new Message(json);

            return senderLink.SendAsync(amqpMessage);
        }
    }
}