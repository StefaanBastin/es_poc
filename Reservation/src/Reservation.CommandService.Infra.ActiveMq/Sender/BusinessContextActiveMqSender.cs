﻿using System;
using System.Threading.Tasks;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.Infra.ActiveMq.Sender
{
    public class BusinessContextActiveMqSender : IBusinessContextSender
    {
        private readonly ActiveMqSender _activeMqSender;

        public BusinessContextActiveMqSender(ActiveMqSender activeMqSender)
        {
            _activeMqSender = activeMqSender;
        }

        public Task Send(Type source, object message)
        {
            return _activeMqSender.Send(source, message, ActiveMqInstances.BusinessContext);
        }
    }
}