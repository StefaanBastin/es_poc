﻿namespace Reservation.CommandService.Infra.ActiveMq
{
    public static class ActiveMqInstances
    {
        public static string Enterprise => "Enterprise";

        public static string BusinessContext => "BusinessContext";
    }
}