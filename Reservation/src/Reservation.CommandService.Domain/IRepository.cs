﻿using System;
using System.Threading.Tasks;

namespace Reservation.CommandService.Domain
{
    public interface IRepository<TAggregate>
    {
        Task<TAggregate> GetAsync(Guid aggregateId);
    }
}