﻿using System;
using System.Collections.Generic;

namespace Reservation.CommandService.Domain
{
    public class Period
    {
        public DateTime Start { get; }
        public DateTime End { get; }

        public Period(DateTime start, DateTime end)
        {
            if (start > end)
            {
                throw new ArgumentException(nameof(start));
            }

            Start = start;
            End = end;
        }

        public IEnumerable<DateTime> GetDates()
        {
            var date = Start.Date;
            while (date <= End.Date)
            {
                yield return date;
                date = date.AddDays(1);
            }
        }
    }
}