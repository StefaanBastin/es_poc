﻿using System;
using System.Collections.Generic;
using System.Linq;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Domain.Exceptions;
using Reservation.CommandService.Domain.ValueObjects;

namespace Reservation.CommandService.Domain.Entities
{
    public class Reservation : AggregateRoot
    {
        private IEnumerable<Amenity> _amenities;
        private bool _isFailed;

        public Reservation(IEnumerable<AmenityDto> amenities)
        {
            Handle(new ReservationCreated(Guid.NewGuid(), amenities));
        }

        public Reservation(IList<Event> events)
            : base(events)
        {
            if (!(events.FirstOrDefault() is ReservationCreated))
            {
                throw new ArgumentException(
                    $"{nameof(Reservation)} should be created with a {nameof(ReservationCreated)}");
            }

            if (events.LastOrDefault() is ReservationBooked)
            {
                throw new AggregateNotFoundException(Id);
            }
        }

        protected override void RegisterEventHandlers()
        {
            Handles<ReservationCreated>(Apply);
            Handles<AmenityBooked>(Apply);
            HandlesWithoutApplying<ReservationBooked>();
            Handles<AmenityBookingFailed>(Apply);
            Handles<AmenityBookingReleased>(Apply);
            Handles<ReservationFailed>(Apply);
        }

        private void Apply(AmenityBookingReleased amenity)
        {
            _amenities
                .Single(new Amenity(amenity.AmenityId, amenity.Date).Equals)
                .Release();
        }

        private void Apply(AmenityBookingFailed evt)
        {
            _amenities
                .Single(new Amenity(evt.AmenityId, evt.Date).Equals)
                .FailBooking();
        }

        private void Apply(ReservationFailed reservationFailed)
        {
            MarkAsFailed();
        }

        private void MarkAsFailed()
        {
            _isFailed = true;
        }

        public void BookAmenity(Guid amenityId, DateTime date)
        {
            ThrowIfAmenityNotExists(amenityId, date);
            ThrowIfAmenityAlreadyBooked(amenityId, date);
            if (_isFailed)
            {
                var amenity = _amenities.Single(new Amenity(amenityId, date).Equals);
                Handle(new AmenityBookingReleased(amenity.Id, Id, amenity.Date, amenity.Quantity));
            }
            else
            {
                Handle(new AmenityBooked(amenityId, Id, date));
            }
        }

        private void ThrowIfAmenityNotExists(Guid amenityId, DateTime date)
        {
            if (!_amenities.Any(new Amenity(amenityId, date).Equals))
            {
                throw new ArgumentException(
                    $"Amenity not found for id {amenityId} and date {date} on reservation with id {Id}");
            }
        }

        private void ThrowIfAmenityAlreadyBooked(Guid amenityId, DateTime date)
        {
            if (_amenities.Single(new Amenity(amenityId, date).Equals).IsBooked)
            {
                throw new InvalidOperationException(
                    $"Amenity with id {amenityId} and date {date} on reservation with id {Id} is already booked");
            }
        }

        private void ThrowIfAmenityAlreadyFailedBooking(Guid amenityId, DateTime date)
        {
            if (_amenities.Single(new Amenity(amenityId, date).Equals).BookingFailed)
            {
                throw new InvalidOperationException(
                    $"Amenity with id {amenityId} and date {date} on reservation with id {Id} has already failed booking");
            }
        }

        private void Apply(ReservationCreated reservationCreated)
        {
            Id = reservationCreated.AggregateId;

            var amenities = new List<Amenity>();
            foreach (var amenity in reservationCreated.Amenities)
            {
                amenities.Add(new Amenity(amenity.Id, amenity.Date, amenity.Quantity));
            }

            _amenities = amenities;
        }

        private void Apply(AmenityBooked amenityBooked)
        {
            var amenity = _amenities.Single(new Amenity(amenityBooked.AmenityId, amenityBooked.Date).Equals);
            amenity.Book();

            if (_amenities.All(x => x.IsBooked))
            {
                var amenities = new List<AmenityDto>();
                foreach (var a in _amenities)
                {
                    amenities.Add(new AmenityDto(a.Id, a.Date, a.Quantity));
                }

                Handle(new ReservationBooked(Id, amenities));
            }
        }

        public void FailAmenityBooking(Guid amenityId, DateTime date)
        {
            ThrowIfAmenityNotExists(amenityId, date);
            ThrowIfAmenityAlreadyFailedBooking(amenityId, date);

            Handle(new AmenityBookingFailed(amenityId, Id, date));
            if (!_isFailed)
            {
                Handle(new ReservationFailed(Id));
            }

            _amenities
                .Where(amenity => amenity.IsBooked)
                .Select(amenity => new AmenityBookingReleased(amenity.Id, Id, amenity.Date, amenity.Quantity))
                .ToList()
                .ForEach(Handle);
        }
    }
}