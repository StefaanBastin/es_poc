﻿using System;
using System.Collections.Generic;
using System.Linq;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Domain.Exceptions;
using Reservation.CommandService.Domain.ValueObjects;

namespace Reservation.CommandService.Domain.Entities
{
    public class BookedReservation : AggregateRoot
    {
        private IEnumerable<Amenity> _amenities;

        public BookedReservation(IEnumerable<AmenityDto> amenities)
        {
            Handle(new BookedReservationCreated(Guid.NewGuid(), amenities));
        }

        public BookedReservation(IList<Event> events)
            : base(events)
        {
            if (!(events.FirstOrDefault() is BookedReservationCreated))
            {
                throw new ArgumentException($"{nameof(BookedReservation)} should be created with a {nameof(BookedReservationCreated)}");
            }

            if (events.LastOrDefault() is ReservationCancelled)
            {
                throw new AggregateNotFoundException(Id);
            }
        }

        public void Cancel()
        {
            Handle(new ReservationCancelled(Id));
            foreach (var amenity in _amenities)
            {
                ReleaseAmenityBooking(amenity);
            }
        }

        protected override void RegisterEventHandlers()
        {
            Handles<BookedReservationCreated>(Apply);
            HandlesWithoutApplying<ReservationCancelled>();
            Handles<AmenityBookingReleased>(Apply);
        }

        private void ReleaseAmenityBooking(Amenity amenity)
        {
            ThrowIfAmenityNotExists(amenity.Id, amenity.Date);
            Handle(new AmenityBookingReleased(amenity.Id, Id, amenity.Date, amenity.Quantity));
        }

        private void ThrowIfAmenityNotExists(Guid amenityId, DateTime date)
        {
            if (!_amenities.Any(new Amenity(amenityId, date).Equals))
            {
                throw new ArgumentException(
                    $"Amenity not found for id {amenityId} and date {date} on reservation with id {Id}");
            }
        }

        private void Apply(BookedReservationCreated bookedReservationCreated)
        {
            Id = bookedReservationCreated.AggregateId;

            var amenities = new List<Amenity>();
            foreach (var amenity in bookedReservationCreated.Amenities)
            {
                amenities.Add(new Amenity(amenity.Id, amenity.Date, amenity.Quantity));
            }

            _amenities = amenities;
        }

        private void Apply(AmenityBookingReleased amenity)
        {
            _amenities
                .Single(new Amenity(amenity.AmenityId, amenity.Date).Equals)
                .Release();
        }
    }
}