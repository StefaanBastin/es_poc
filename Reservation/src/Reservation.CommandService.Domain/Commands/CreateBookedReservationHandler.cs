﻿using System.Threading.Tasks;
using Reservation.CommandService.Domain.Entities;

namespace Reservation.CommandService.Domain.Commands
{
    public class CreateBookedReservationHandler : ICommandHandler<CreateBookedReservation>
    {
        private readonly IEventDispatcher _eventDispatcher;

        public CreateBookedReservationHandler(IEventDispatcher eventDispatcher)
        {
            _eventDispatcher = eventDispatcher;
        }

        public Task HandleAsync(CreateBookedReservation createBookedReservation)
        {
            return new BookedReservation(createBookedReservation.Amenities).DispatchAsync(_eventDispatcher);
        }
    }
}