﻿using System.Threading.Tasks;
using Reservation.CommandService.Domain.Entities;

namespace Reservation.CommandService.Domain.Commands
{
    public class CancelReservationHandler : ICommandHandler<CancelReservation>
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IRepository<BookedReservation> _reservationRepository;

        public CancelReservationHandler(IEventDispatcher eventDispatcher, IRepository<BookedReservation> reservationRepository)
        {
            _eventDispatcher = eventDispatcher;
            _reservationRepository = reservationRepository;
        }

        public async Task HandleAsync(CancelReservation command)
        {
            var bookedReservation = await _reservationRepository.GetAsync(command.Id);
            bookedReservation.Cancel();
            await bookedReservation.DispatchAsync(_eventDispatcher);
        }
    }
}