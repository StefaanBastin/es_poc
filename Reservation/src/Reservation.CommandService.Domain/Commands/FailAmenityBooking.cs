﻿using System;

namespace Reservation.CommandService.Domain.Commands
{
    public class FailAmenityBooking
    {
        public Guid AmenityId { get; }
        public DateTime AmenityDate { get; }
        public Guid ReservationId { get; }

        public FailAmenityBooking(Guid amenityId, DateTime amenityDate, Guid reservationId)
        {
            AmenityId = amenityId;
            AmenityDate = amenityDate;
            ReservationId = reservationId;
        }
    }
}