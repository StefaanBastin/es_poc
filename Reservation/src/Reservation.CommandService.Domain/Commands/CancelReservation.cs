﻿using System;

namespace Reservation.CommandService.Domain.Commands
{
    public class CancelReservation
    {
        public Guid Id { get; }

        public CancelReservation(Guid id)
        {
            Id = id;
        }
    }
}