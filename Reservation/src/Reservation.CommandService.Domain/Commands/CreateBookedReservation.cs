﻿using System.Collections.Generic;
using Reservation.CommandService.Domain.Dtos;

namespace Reservation.CommandService.Domain.Commands
{
    public class CreateBookedReservation
    {
        public IEnumerable<AmenityDto> Amenities { get; }

        public CreateBookedReservation(IEnumerable<AmenityDto> amenities)
        {
            Amenities = amenities;
        }
    }
}