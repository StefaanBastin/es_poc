﻿using System;
using System.Collections.Generic;
using Reservation.CommandService.Domain.Dtos;

namespace Reservation.CommandService.Domain.Commands
{
    public class CreateReservation
    {
        public Guid ProductId { get; }
        public IEnumerable<AmenityDto> Amenities { get; }

        public CreateReservation(Guid productId, IEnumerable<AmenityDto> amenities)
        {
            ProductId = productId;
            Amenities = amenities;
        }
    }
}