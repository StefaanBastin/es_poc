﻿using System.Threading.Tasks;

namespace Reservation.CommandService.Domain.Commands
{
    public class CreateReservationHandler : ICommandHandler<CreateReservation>
    {
        private readonly IEventDispatcher _eventDispatcher;

        public CreateReservationHandler(IEventDispatcher eventDispatcher)
        {
            _eventDispatcher = eventDispatcher;
        }

        public Task HandleAsync(CreateReservation createReservation)
        {
            var reservation = new Entities.Reservation(createReservation.Amenities);
            return reservation.DispatchAsync(_eventDispatcher);
        }
    }
}