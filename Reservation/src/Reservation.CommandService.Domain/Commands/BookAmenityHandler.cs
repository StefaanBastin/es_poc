﻿using System.Threading.Tasks;

namespace Reservation.CommandService.Domain.Commands
{
    public class BookAmenityHandler : ICommandHandler<BookAmenity>
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IRepository<Entities.Reservation> _reservationRepository;

        public BookAmenityHandler(IEventDispatcher eventDispatcher, IRepository<Entities.Reservation> reservationRepository)
        {
            _eventDispatcher = eventDispatcher;
            _reservationRepository = reservationRepository;
        }

        public async Task HandleAsync(BookAmenity command)
        {
            var reservation = await _reservationRepository.GetAsync(command.ReservationId);
            reservation.BookAmenity(command.Id, command.Date);
            await reservation.DispatchAsync(_eventDispatcher);
        }
    }
}