﻿using System.Threading.Tasks;

namespace Reservation.CommandService.Domain.Commands
{
    public class FailAmenityBookingHandler : ICommandHandler<FailAmenityBooking>
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IRepository<Entities.Reservation> _reservationRepository;

        public FailAmenityBookingHandler(IEventDispatcher eventDispatcher, IRepository<Entities.Reservation> reservationRepository)
        {
            _eventDispatcher = eventDispatcher;
            _reservationRepository = reservationRepository;
        }

        public async Task HandleAsync(FailAmenityBooking command)
        {
            var reservation = await _reservationRepository.GetAsync(command.ReservationId);
            reservation.FailAmenityBooking(command.AmenityId, command.AmenityDate);
            await reservation.DispatchAsync(_eventDispatcher);
        }
    }
}