﻿using System;

namespace Reservation.CommandService.Domain.Commands
{
    public class BookAmenity
    {
        public Guid Id { get; }
        public DateTime Date { get; }
        public Guid ReservationId { get; }

        public BookAmenity(Guid id, DateTime date, Guid reservationId)
        {
            Id = id;
            Date = date;
            ReservationId = reservationId;
        }
    }
}