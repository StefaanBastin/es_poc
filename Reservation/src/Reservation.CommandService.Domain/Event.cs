﻿using System;

namespace Reservation.CommandService.Domain
{
    public abstract class Event
    {
        public Guid AggregateId { get; }

        protected Event(Guid aggregateId)
        {
            AggregateId = aggregateId;
        }
    }
}