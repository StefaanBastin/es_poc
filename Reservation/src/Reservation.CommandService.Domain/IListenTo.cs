﻿using System.Threading.Tasks;

namespace Reservation.CommandService.Domain
{
    /// <summary>
    /// An interface for classes that listen to external events, i.e. events from other systems.
    /// For events internal to the current system, use <see cref="IEventHandler{TEvent}"/>.
    /// </summary>
    /// <typeparam name="TEvent">The event type to listen to.</typeparam>
    public interface IListenTo<in TEvent>
    {
        Task HandleAsync(TEvent evt);
    }
}