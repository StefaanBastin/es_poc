﻿using System;
using System.Threading.Tasks;

namespace Reservation.CommandService.Domain
{
    public class ReservationRepository : IRepository<Entities.Reservation>
    {
        private readonly IEventRepository _eventRepository;

        public ReservationRepository(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<Entities.Reservation> GetAsync(Guid aggregateId)
        {
            var events = await _eventRepository.GetAsync(aggregateId, typeof(Entities.Reservation));

            return new Entities.Reservation(events);
        }
    }
}