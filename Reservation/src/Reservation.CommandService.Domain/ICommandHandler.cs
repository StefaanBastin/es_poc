﻿using System.Threading.Tasks;

namespace Reservation.CommandService.Domain
{
    public interface ICommandHandler<in TCommand>
    {
        Task HandleAsync(TCommand command);
    }
}