﻿using System;

namespace Reservation.CommandService.Domain.Exceptions
{
    public class AggregateNotFoundException : Exception
    {
        public Guid AggregateId { get; }

        public AggregateNotFoundException(Guid aggregateId)
        {
            AggregateId = aggregateId;
        }
    }
}