﻿using System;
using System.Threading.Tasks;

namespace Reservation.CommandService.Domain
{
    public interface IBusinessContextSender
    {
        Task Send(Type source, object message);
    }
}