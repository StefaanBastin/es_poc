﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reservation.CommandService.Domain.Exceptions;

namespace Reservation.CommandService.Domain
{
    public abstract class AggregateRoot
    {
        private readonly List<Event> _events = new List<Event>();

        private readonly Dictionary<Type, Action<Event>> _handlers = new Dictionary<Type, Action<Event>>();

        public Guid Id { get; protected set; }

        protected abstract void RegisterEventHandlers();

        public AggregateRoot()
        {
            // It's the most pragmatic way to make sure that the event handlers are registered.
            // ReSharper disable once VirtualMemberCallInConstructor
            RegisterEventHandlers();
        }

        public AggregateRoot(IList<Event> events)
            : this()
        {
            foreach (var evt in events)
            {
                InvokeEventHandlers(evt);
            }
        }

        public async Task DispatchAsync(IEventDispatcher eventDispatcher)
        {
            await Task.WhenAll(_events.Select(evt => eventDispatcher.DispatchAsync(evt, GetType())));
            _events.Clear();
        }

        protected void Handles<TEvent>(Action<TEvent> handler)
            where TEvent : Event
        {
            _handlers.Add(typeof(TEvent), evt => handler((TEvent)evt));
        }

        protected void HandlesWithoutApplying<TEvent>()
            where TEvent : Event
        {
            _handlers.Add(typeof(TEvent), evt => { });
        }

        protected void Handle(Event evt)
        {
            _events.Add(evt);
            InvokeEventHandlers(evt);
        }

        private void InvokeEventHandlers(Event evt)
        {
            if (!_handlers.ContainsKey(evt.GetType()))
            {
                throw new UnhandledEventException(evt.GetType());
            }

            _handlers[evt.GetType()].Invoke(evt);
        }
    }
}