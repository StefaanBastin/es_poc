﻿using System;
using System.Threading.Tasks;

namespace Reservation.CommandService.Domain
{
    public interface IEventDispatcher
    {
        Task DispatchAsync(Event evt, Type source);
    }
}