﻿using System;

namespace Reservation.CommandService.Domain.ValueObjects
{
    public class Amenity
    {
        public Guid Id { get; }
        public DateTime Date { get; }
        public bool IsBooked { get; private set; }
        public bool BookingFailed { get; private set; }
        public int Quantity { get; }

        public Amenity(Guid id, DateTime date)
        {
            Id = id;
            Date = date;
        }

        public Amenity(Guid id, DateTime date, int quantity)
            : this(id, date)
        {
            Quantity = quantity;
        }

        public void Book()
        {
            IsBooked = true;
        }

        public void FailBooking()
        {
            BookingFailed = true;
        }

        public void Release()
        {
            IsBooked = false;
        }

        public override bool Equals(object obj)
            => obj is Amenity amenity && amenity.Id == Id && amenity.Date == Date;

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ Date.GetHashCode();
            }
        }
    }
}
