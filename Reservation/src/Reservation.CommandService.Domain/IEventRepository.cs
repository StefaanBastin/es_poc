﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reservation.CommandService.Domain
{
    public interface IEventRepository
    {
        Task<IList<Event>> GetAsync(Guid aggregateId, Type aggregateType);
        Task SaveAsync(Event evt, Type source);
    }
}