﻿using System;

namespace Reservation.CommandService.Domain.InventoryEvents
{
    public class AmenityTaken
    {
        public Guid AmenityId { get; }
        public DateTime Date { get; }
        public int Quantity { get; }
        public Guid ReservationId { get; }

        public AmenityTaken(Guid amenityId, DateTime date, int quantity, Guid reservationId)
        {
            AmenityId = amenityId;
            Date = date;
            Quantity = quantity;
            ReservationId = reservationId;
        }
    }
}