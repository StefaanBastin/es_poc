﻿using System;
using System.Threading.Tasks;
using Reservation.CommandService.Domain.Entities;

namespace Reservation.CommandService.Domain
{
    public class BookedReservationRepository : IRepository<BookedReservation>
    {
        private readonly IEventRepository _eventRepository;

        public BookedReservationRepository(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<BookedReservation> GetAsync(Guid aggregateId)
        {
            var events = await _eventRepository.GetAsync(aggregateId, typeof(BookedReservation));

            return new BookedReservation(events);
        }
    }
}