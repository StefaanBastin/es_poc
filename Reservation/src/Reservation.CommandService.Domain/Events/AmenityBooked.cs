﻿using System;

namespace Reservation.CommandService.Domain.Events
{
    public class AmenityBooked : Event
    {
        public Guid AmenityId { get; }
        public DateTime Date { get; }

        public AmenityBooked(Guid amenityId, Guid reservationId, DateTime date)
            : base(reservationId)
        {
            AmenityId = amenityId;
            Date = date;
        }
    }
}