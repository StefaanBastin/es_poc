﻿using System;

namespace Reservation.CommandService.Domain.Events
{
    public class ReservationFailed : Event
    {
        public ReservationFailed(Guid reservationId)
            : base(reservationId)
        {
        }
    }
}