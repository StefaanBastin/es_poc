﻿using System;

namespace Reservation.CommandService.Domain.Events
{
    public class AmenityBookingReleased : Event
    {
        public DateTime Date { get; }
        public int Quantity { get; }
        public Guid AmenityId { get; }

        public AmenityBookingReleased(Guid amenityId, Guid reservationId, DateTime date, int quantity)
            : base(reservationId)
        {
            AmenityId = amenityId;
            Date = date;
            Quantity = quantity;
        }
    }
}