﻿using System;

namespace Reservation.CommandService.Domain.Events
{
    public class ReservationCancelled : Event
    {
        public ReservationCancelled(Guid aggregateId)
            : base(aggregateId)
        {
        }
    }
}