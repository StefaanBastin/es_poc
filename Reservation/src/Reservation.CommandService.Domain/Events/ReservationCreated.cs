﻿using System;
using System.Collections.Generic;
using Reservation.CommandService.Domain.Dtos;

namespace Reservation.CommandService.Domain.Events
{
    public class ReservationCreated : Event
    {
        public IEnumerable<AmenityDto> Amenities { get; }

        public ReservationCreated(Guid id, IEnumerable<AmenityDto> amenities)
            : base(id)
        {
            Amenities = amenities;
        }
    }
}