﻿using System;

namespace Reservation.CommandService.Domain.Events
{
    public class AmenityBookingFailed : Event
    {
        public Guid AmenityId { get; }
        public DateTime Date { get; }

        public AmenityBookingFailed(Guid amenityId, Guid reservationId, DateTime date)
            : base(reservationId)
        {
            AmenityId = amenityId;
            Date = date;
        }
    }
}