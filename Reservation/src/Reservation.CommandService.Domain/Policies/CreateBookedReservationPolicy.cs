﻿using System.Threading.Tasks;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Events;

namespace Reservation.CommandService.Domain.Policies
{
    public class CreateBookedReservationPolicy : IEventHandler<ReservationBooked>
    {
        private readonly ICommandHandler<CreateBookedReservation> _commandHandler;

        public CreateBookedReservationPolicy(ICommandHandler<CreateBookedReservation> commandHandler)
        {
            _commandHandler = commandHandler;
        }

        public Task HandleAsync(ReservationBooked reservationBooked)
        {
            return _commandHandler.HandleAsync(new CreateBookedReservation(reservationBooked.Amenities));
        }
    }
}