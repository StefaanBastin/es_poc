﻿using System.Threading.Tasks;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.InventoryEvents;

namespace Reservation.CommandService.Domain.Policies
{
    public class BookAmenityPolicy : IListenTo<AmenityTaken>
    {
        private readonly ICommandHandler<BookAmenity> _commandHandler;

        public BookAmenityPolicy(ICommandHandler<BookAmenity> commandHandler)
        {
            _commandHandler = commandHandler;
        }

        public Task HandleAsync(AmenityTaken evt)
        {
            var command = new BookAmenity(evt.AmenityId, evt.Date, evt.ReservationId);
            return _commandHandler.HandleAsync(command);
        }
    }
}