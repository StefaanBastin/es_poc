﻿using System;

namespace Reservation.CommandService.Domain.Dtos
{
    public class AmenityDto
    {
        public Guid Id { get; }
        public DateTime Date { get; }
        public int Quantity { get; }

        public AmenityDto(Guid id, DateTime date, int quantity)
        {
            Id = id;
            Date = date;
            Quantity = quantity;
        }
    }
}