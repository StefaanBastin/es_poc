﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Reservation.CommandService.Application;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.Infra.InMemoryBus
{
    public class InMemoryBus : IInternalEventMessenger
    {
        private readonly IEnumerable<IEventHandler> _eventHandler;
        private readonly ILogger<InMemoryBus> _logger;

        public InMemoryBus(IEnumerable<IEventHandler> eventHandlers, ILogger<InMemoryBus> logger)
        {
            _eventHandler = eventHandlers;
            _logger = logger;
        }

        public Task Send(Type source, Event evt)
        {
            foreach (var eventHandler in _eventHandler)
            {
                var eventHandlerType = eventHandler.GetType();
                if (typeof(IEventHandler<>).IsAssignableFrom(eventHandlerType))
                {
                    return Task.FromResult(0);
                }

                var handleMethods = eventHandlerType
                    .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => m.Name == nameof(IEventHandler<Event>.HandleAsync) && m.GetParameters().Length == 1);

                foreach (var handleMethod in handleMethods)
                {
                    var eventType = handleMethod.GetParameters().Single().ParameterType;
                    if (eventType == evt.GetType())
                    {
                        try
                        {
                            handleMethod.Invoke(eventHandler, new object[] { evt });
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(e, $"An exception occurred when invoking the {nameof(IEventHandler<Event>.HandleAsync)} method of {eventHandlerType.Name}.");
                        }
                    }
                }
            }

            return Task.FromResult(0);
        }
    }
}