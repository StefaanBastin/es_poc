﻿using System;

namespace Reservation.CommandService.Messages
{
    public class AmenityBookingReleasedEventMessage
    {
        public Guid AmenityId { get; set; }
        public DateTime Date { get; set; }
        public int Quantity { get; set; }
    }
}
