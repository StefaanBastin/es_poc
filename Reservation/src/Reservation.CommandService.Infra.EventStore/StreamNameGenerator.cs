﻿using System;

namespace Reservation.CommandService.Infra.EventStore
{
    public class StreamNameGenerator : IStreamNameGenerator
    {
        public string Generate(Guid aggregateId, Type source)
            => source.Name + "-" + aggregateId.ToString().Replace("-", string.Empty);
    }
}