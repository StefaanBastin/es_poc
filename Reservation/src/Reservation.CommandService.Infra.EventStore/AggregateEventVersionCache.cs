﻿using System;
using System.Collections.Generic;

namespace Reservation.CommandService.Infra.EventStore
{
    public class AggregateEventVersionCache : IAggregateEventVersionCache
    {
        private readonly Dictionary<Guid, long?> _eventVersionCache;

        public AggregateEventVersionCache()
        {
            _eventVersionCache = new Dictionary<Guid, long?>();
        }

        public long? GetEventVersion(Guid aggregateId)
        {
            _eventVersionCache.TryGetValue(aggregateId, out var version);
            return version;
        }

        public void SetEventVersion(Guid aggregateId, long eventVersion)
        {
            _eventVersionCache[aggregateId] = eventVersion;
        }
    }
}