﻿using System;

namespace Reservation.CommandService.Infra.EventStore
{
    public class EventMetaData
    {
        public Type EventType { get; set; }
    }
}