﻿using System;
using System.Text;
using Newtonsoft.Json;

namespace Reservation.CommandService.Infra.EventStore.Converters
{
    public class UTF8ByteArrayConverter : IByteArrayConverter
    {
        public byte[] ToByteArray(object value)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(value));
        }

        public object FromByteArray(byte[] bytes, Type type)
        {
            return JsonConvert.DeserializeObject(Encoding.UTF8.GetString(bytes), type);
        }
    }
}