﻿using System;

namespace Reservation.CommandService.Infra.EventStore
{
    public interface IStreamNameGenerator
    {
        string Generate(Guid aggregateId, Type source);
    }
}