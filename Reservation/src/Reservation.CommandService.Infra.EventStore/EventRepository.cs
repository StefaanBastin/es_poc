﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Infra.EventStore.Converters;

namespace Reservation.CommandService.Infra.EventStore
{
    public class EventRepository
        : IEventRepository
    {
        private readonly IStreamNameGenerator _nameGenerator;
        private readonly IEventStoreConnection _connection;
        private readonly IAggregateEventVersionCache _aggregateEventVersionCache;
        private readonly IByteArrayConverter _converter;

        public EventRepository(
            IStreamNameGenerator nameGenerator,
            IEventStoreConnection connection,
            IAggregateEventVersionCache aggregateEventVersionCache,
            IByteArrayConverter converter)
        {
            _nameGenerator = nameGenerator;
            _connection = connection;
            _aggregateEventVersionCache = aggregateEventVersionCache;
            _converter = converter;
        }

        public Task SaveAsync(Event evt, Type source)
        {
            var version = ExpectedVersion.NoStream;
            var expectedEventVersion = _aggregateEventVersionCache.GetEventVersion(evt.AggregateId);
            if (expectedEventVersion.HasValue)
            {
                version = expectedEventVersion.Value;
            }

            var eventData = _converter.ToByteArray(evt);

            var metadata = _converter.ToByteArray(new EventMetaData { EventType = evt.GetType() });

            var storeEvent = new EventData(Guid.NewGuid(), evt.GetType().Name, true, eventData, metadata);
            var streamName = _nameGenerator.Generate(evt.AggregateId, source);

            _aggregateEventVersionCache.SetEventVersion(evt.AggregateId, version + 1);

            return _connection.AppendToStreamAsync(streamName, version, storeEvent);
        }

        public async Task<IList<Event>> GetAsync(Guid aggregateId, Type source)
        {
            if (_aggregateEventVersionCache.GetEventVersion(aggregateId).HasValue)
            {
                throw new ArgumentException("Aggregate already exists in current context");
            }

            var streamName = _nameGenerator.Generate(aggregateId, source);

            var result = await _connection.ReadStreamEventsForwardAsync(streamName, 0, 4096, false);
            var eventList = new List<Event>();
            foreach (var resolvedEvent in result.Events)
            {
                var metadata = (EventMetaData)_converter.FromByteArray(resolvedEvent.Event.Metadata, typeof(EventMetaData));
                eventList.Add((Event)_converter.FromByteArray(resolvedEvent.Event.Data, metadata.EventType));
            }

            return eventList;
        }
    }
}