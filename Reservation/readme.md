# Reservation

## Getting Started

### Docker images

```
docker pull eventstore/eventstore
docker pull rmohr/activemq

docker run --name eventstore -it -p 2113:2113 -p 1113:1113 eventstore/eventstore
docker run --name activemq-node -p 5672:5672 -p 8161:8161 rmohr/activemq
```

### Weburl

1. eventstore : http://192.168.99.100:1113/
1. active mq : http://192.168.99.100:8161/