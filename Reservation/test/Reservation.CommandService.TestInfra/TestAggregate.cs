﻿using System;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.TestInfra
{
    public class TestAggregate : AggregateRoot
    {
        public void AddTestEvent()
        {
            Handle(new TestEvent(Guid.NewGuid()));
        }

        protected override void RegisterEventHandlers()
        {
            Handles<TestEvent>(Apply);
        }

        private void Apply(TestEvent testEvent)
        {
        }
    }
}