﻿using System;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.TestInfra
{
    public class TestEvent : Event
    {
        public TestEvent(Guid aggregateId)
            : base(aggregateId)
        {
        }
    }
}