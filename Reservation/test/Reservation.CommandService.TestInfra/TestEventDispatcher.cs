﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.TestInfra
{
    public class TestEventDispatcher : IEventDispatcher
    {
        public IList<Event> Events { get; }

        public TestEventDispatcher()
        {
            Events = new List<Event>();
        }

        public Task DispatchAsync(Event evt, Type source)
        {
            Events.Add(evt);
            return Task.FromResult(0);
        }
    }
}