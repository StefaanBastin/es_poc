﻿using System;
using System.Collections.Generic;
using System.Threading;
using Amqp;
using Autofac;
using Autofac.Builder;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using Reservation.CommandService.Application;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.ActiveMq.Receiver;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.ActiveMq.ItgTests.Receiver.ActiveMqReceiverTests
{
    [Collection("ActiveMqDocker collection")]
    public class Given_Started_When_Sending_Messages : Given_When_Then_Test
    {
        private readonly IList<TestMessage> _receivedMessages = new List<TestMessage>();
        private readonly AutoResetEvent _messageReceived = new AutoResetEvent(false);
        private Message _amqpMessage1;
        private Message _amqpMessage2;
        private SenderLink _senderLink;
        private TestMessage _message1;
        private TestMessage _message2;
        private IDisposable _subscription;

        protected override void Given()
        {
            _message1 = new TestMessage
            {
                Foo = "bar"
            };

            _message2 = new TestMessage
            {
                Foo = "bar2"
            };

            var container = new ContainerBuilderFactory()
                .Create()
                .Build(ContainerBuildOptions.IgnoreStartableComponents);

            container.StartStartable<IActiveMqConfiguration>();
            container.StartStartable<BusinessContextActiveMqConnection>();

            var activeMqConnectionPool = container.Resolve<ActiveMqConnectionPool>();

            var nodeAddress = Guid.NewGuid().ToString();
            _senderLink = new SenderLink(activeMqConnectionPool.GetSession(ActiveMqInstances.BusinessContext), "senderLink", nodeAddress);
            var json1 = JsonConvert.SerializeObject(_message1);
            _amqpMessage1 = new Message(json1);

            var json2 = JsonConvert.SerializeObject(_message2);
            _amqpMessage2 = new Message(json2);

            var activeMqListenerConfigurationMock = new Mock<IActiveMqReceiverConfiguration>();
            activeMqListenerConfigurationMock
                .Setup(configuration => configuration.ListenTimeoutInMilliseconds)
                .Returns(1000);

            var messageTypeToAddressMapperMock = new Mock<IMessageTypeToAddressMapper>();
            messageTypeToAddressMapperMock
                .Setup(mapper => mapper.Map(typeof(TestMessage)))
                .Returns(nodeAddress);

            var session = activeMqConnectionPool.GetSession(ActiveMqInstances.BusinessContext);
            var sut = new ActiveMqReceiver<TestMessage>(session, activeMqListenerConfigurationMock.Object, messageTypeToAddressMapperMock.Object);

            var businessContextHandlerMock = new Mock<IBusinessContextMessageHandler<TestMessage>>();
            businessContextHandlerMock
                .Setup(handler => handler.OnNext(It.IsAny<TestMessage>()))
                .Callback<TestMessage>(message =>
                {
                    _receivedMessages.Add(message);
                    _messageReceived.Set();
                });

            _subscription = sut.Subscribe(businessContextHandlerMock.Object);
        }

        protected override void When()
        {
            _senderLink.SendAsync(_amqpMessage1).Wait();
            _senderLink.SendAsync(_amqpMessage2).Wait();
        }

        [Fact]
        public void Then_Should_Receive_All_Messages()
        {
            _messageReceived.WaitOne(1000);
            _messageReceived.WaitOne(1000);
            _receivedMessages.Should().BeEquivalentTo(new List<TestMessage>
            {
                _message1,
                _message2
            });
        }

        protected override void Cleanup()
        {
            _subscription.Dispose();
        }
    }
}