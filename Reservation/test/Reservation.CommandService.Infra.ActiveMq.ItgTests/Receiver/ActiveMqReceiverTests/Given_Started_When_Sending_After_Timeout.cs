﻿using System;
using System.Collections.Generic;
using System.Threading;
using Amqp;
using Autofac;
using Autofac.Builder;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using Reservation.CommandService.Application;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.ActiveMq.Receiver;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.ActiveMq.ItgTests.Receiver.ActiveMqReceiverTests
{
    [Collection("ActiveMqDocker collection")]
    public class Given_Started_When_Sending_After_Timeout : Given_When_Then_Test
    {
        private readonly IList<TestMessage> _receivedMessages = new List<TestMessage>();
        private readonly AutoResetEvent _messageReceived = new AutoResetEvent(false);
        private TestMessage _message;
        private SenderLink _senderLink;
        private Message _amqpMessage;
        private IDisposable _subscription;

        protected override void Given()
        {
            _message = new TestMessage
            {
                Foo = "bar"
            };

            var container = new ContainerBuilderFactory()
                .Create()
                .Build(ContainerBuildOptions.IgnoreStartableComponents);

            container.StartStartable<IActiveMqConfiguration>();
            container.StartStartable<BusinessContextActiveMqConnection>();

            var activeMqConnectionPool = container.Resolve<ActiveMqConnectionPool>();

            var json1 = JsonConvert.SerializeObject(_message);
            _amqpMessage = new Message(json1);

            var activeMqListenerConfigurationMock = new Mock<IActiveMqReceiverConfiguration>();
            activeMqListenerConfigurationMock
                .Setup(configuration => configuration.ListenTimeoutInMilliseconds)
                .Returns(1);

            var messageTypeToAddressMapperMock = new Mock<IMessageTypeToAddressMapper>();

            var nodeAddress = Guid.NewGuid().ToString();

            messageTypeToAddressMapperMock
                .Setup(mapper => mapper.Map(typeof(TestMessage)))
                .Returns(nodeAddress);

            var session = activeMqConnectionPool.GetSession(ActiveMqInstances.BusinessContext);
            var sut = new ActiveMqReceiver<TestMessage>(session, activeMqListenerConfigurationMock.Object, messageTypeToAddressMapperMock.Object);

            var businessContextHandlerMock = new Mock<IBusinessContextMessageHandler<TestMessage>>();
            businessContextHandlerMock
                .Setup(handler => handler.OnNext(It.IsAny<TestMessage>()))
                .Callback<TestMessage>(message =>
                {
                    _receivedMessages.Add(message);
                    _messageReceived.Set();
                });

            _subscription = sut.Subscribe(businessContextHandlerMock.Object);

            _senderLink = new SenderLink(activeMqConnectionPool.GetSession(ActiveMqInstances.BusinessContext), "senderLink", nodeAddress);
        }

        protected override void When()
        {
            _senderLink.SendAsync(_amqpMessage).Wait();
        }

        [Fact]
        public void Then_Receive_Message()
        {
            _messageReceived.WaitOne(100);
            _receivedMessages.Should().BeEquivalentTo(new List<TestMessage>
            {
                _message
            });
        }

        protected override void Cleanup()
        {
            _subscription.Dispose();
        }
    }
}