﻿using System;
using System.Collections.Generic;
using System.Threading;
using Amqp;
using Autofac;
using Autofac.Builder;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using Reservation.CommandService.Application;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.ActiveMq.Receiver;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.ActiveMq.ItgTests.Receiver.ActiveMqReceiverTests
{
    [Collection("ActiveMqDocker collection")]
    public class Given_Unsubscribed_When_Sending_Messages : Given_When_Then_Test
    {
        private readonly IList<TestMessage> _receivedMessages = new List<TestMessage>();
        private readonly AutoResetEvent _messageReceived = new AutoResetEvent(false);
        private Message _amqpMessage;
        private SenderLink _senderLink;
        private TestMessage _message;
        private IDisposable _subscription;

        protected override void Given()
        {
            _message = new TestMessage
            {
                Foo = "bar"
            };

            var container = new ContainerBuilderFactory()
                .Create()
                .Build(ContainerBuildOptions.IgnoreStartableComponents);

            container.StartStartable<IActiveMqConfiguration>();
            container.StartStartable<BusinessContextActiveMqConnection>();

            var activeMqConnectionPool = container.Resolve<ActiveMqConnectionPool>();

            var nodeAddress = Guid.NewGuid().ToString();

            _senderLink = new SenderLink(activeMqConnectionPool.GetSession(ActiveMqInstances.BusinessContext), "senderLink", nodeAddress);
            var json1 = JsonConvert.SerializeObject(_message);
            _amqpMessage = new Message(json1);

            var activeMqListenerConfiguraitonMock = new Mock<IActiveMqReceiverConfiguration>();
            activeMqListenerConfiguraitonMock
                .Setup(configuration => configuration.ListenTimeoutInMilliseconds)
                .Returns(1000);

            var messageTypeToAddressMapperMock = new Mock<IMessageTypeToAddressMapper>();
            messageTypeToAddressMapperMock
                .Setup(mapper => mapper.Map(typeof(TestMessage)))
                .Returns(nodeAddress);

            var session = activeMqConnectionPool.GetSession(ActiveMqInstances.BusinessContext);
            var sut = new ActiveMqReceiver<TestMessage>(session, activeMqListenerConfiguraitonMock.Object, messageTypeToAddressMapperMock.Object);

            var businessContextHandlerMock = new Mock<IBusinessContextMessageHandler<TestMessage>>();
            businessContextHandlerMock
                .Setup(handler => handler.OnNext(It.IsAny<TestMessage>()))
                .Callback<TestMessage>(message =>
                {
                    _receivedMessages.Add(message);
                    _messageReceived.Set();
                });

            _subscription = sut.Subscribe(businessContextHandlerMock.Object);
            _subscription.Dispose();
        }

        protected override void When()
        {
            _senderLink.SendAsync(_amqpMessage).Wait();
        }

        [Fact]
        public void Then_Should_Not_Receive_Any_Messages()
        {
            _messageReceived.WaitOne(100);
            _receivedMessages.Should().BeEmpty();
        }
    }
}