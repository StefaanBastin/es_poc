﻿using System;
using Amqp;
using Autofac;
using Autofac.Builder;
using FluentAssertions;
using Newtonsoft.Json;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.ActiveMq.Sender;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.ActiveMq.ItgTests.Sender.EnterpriseActiveMqSenderTests
{
    [Collection("ActiveMqDocker collection")]
    public class When_Sending : Given_When_Then_Test
    {
        private EnterpriseActiveMqSender _sut;
        private Type _source;
        private object _message;
        private IContainer _container;

        protected override void Given()
        {
            _source = typeof(TestAggregate);
            _message = new TestMessage
            {
                Foo = "bar"
            };

            _container = new ContainerBuilderFactory()
                .Create()
                .Build(ContainerBuildOptions.IgnoreStartableComponents);

            _container.StartStartable<IActiveMqConfiguration>();
            _container.StartStartable<EnterpriseActiveMqConnection>();

            _sut = new EnterpriseActiveMqSender(_container.Resolve<ActiveMqSender>());
        }

        protected override void When()
        {
            _sut.Send(_source, _message);
        }

        [Fact]
        public void Then_Should_Send_To_ActiveMq()
        {
            var activeMqConfiguration = _container.Resolve<IActiveMqConfiguration>();
            var address = new Address(activeMqConfiguration.EnterpriseActiveMqUri);
            var connection = new Connection(address);

            try
            {
                var session = new Session(connection);
                var receiverLink = new ReceiverLink(session, "receiverLink", _source.Name);

                var amqpMessage = receiverLink.Receive(TimeSpan.FromSeconds(10));
                var json = amqpMessage.Body.ToString();
                var message = JsonConvert.DeserializeObject<TestMessage>(json);
                receiverLink.Accept(amqpMessage);

                message.Should().BeEquivalentTo(_message);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}