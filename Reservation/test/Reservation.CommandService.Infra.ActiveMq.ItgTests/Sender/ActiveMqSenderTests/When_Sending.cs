﻿using System;
using Amqp;
using Autofac;
using Autofac.Builder;
using FluentAssertions;
using Newtonsoft.Json;
using Reservation.CommandService.Host.ConsoleApp.ActiveMq;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.ActiveMq.Sender;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.ActiveMq.ItgTests.Sender.ActiveMqSenderTests
{
    [Collection("ActiveMqDocker collection")]
    public class When_Sending : Given_When_Then_Test
    {
        private ActiveMqSender _sut;
        private Type _source;
        private object _message;
        private string _uri;

        protected override void Given()
        {
            _source = typeof(When_Sending);
            _message = new TestMessage
            {
                Foo = "bar"
            };

            var container = new ContainerBuilderFactory()
                .Create()
                .Build(ContainerBuildOptions.IgnoreStartableComponents);

            container.StartStartable<IActiveMqConfiguration>();
            container.StartStartable<BusinessContextActiveMqConnection>();

            _uri = container.Resolve<IActiveMqConfiguration>().BusinessContextActiveMqUri;
            var activeMqConnectionPool = container.Resolve<ActiveMqConnectionPool>();

            _sut = new ActiveMqSender(activeMqConnectionPool);
        }

        protected override void When()
        {
            _sut.Send(_source, _message, ActiveMqInstances.BusinessContext).Wait();
        }

        [Fact]
        public void Then_Should_Send_To_ActiveMq()
        {
            var address = new Address(_uri);
            var connection = new Connection(address);

            try
            {
                var session = new Session(connection);
                var receiverLink = new ReceiverLink(session, "receiverLink", _source.Name);

                var amqpMessage = receiverLink.Receive(TimeSpan.FromSeconds(10));
                var json = amqpMessage.Body.ToString();
                var message = JsonConvert.DeserializeObject<TestMessage>(json);
                receiverLink.Accept(amqpMessage);

                message.Should().BeEquivalentTo(_message);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}