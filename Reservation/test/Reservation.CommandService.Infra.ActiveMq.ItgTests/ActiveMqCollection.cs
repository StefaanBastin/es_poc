﻿using Reservation.CommandService.TestInfra;
using Xunit;

namespace Reservation.CommandService.Infra.ActiveMq.ItgTests
{
    /// <summary>
    /// This class has no code, and is never created. Its purpose is simply
    /// to be the place to apply [CollectionDefinition] and all the
    /// ICollectionFixture interfaces.
    /// </summary>
    [CollectionDefinition("ActiveMqDocker collection")]
    public class ActiveMqCollection : ICollectionFixture<ActiveMqDockerFixture>
    {
    }
}