﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Application.UnitTests.EventDispatcherTests
{
    public class Given_EventRepository_Fails_When_Dispatching : Given_When_Then_Test
    {
        private TestEvent _event;

        private bool _isEventSent;
        private EventDispatcher _sut;

        protected override void Given()
        {
            _event = new TestEvent(Guid.NewGuid());

            var eventRepositoryMock = new Mock<IEventRepository>();
            eventRepositoryMock
                .Setup(repository => repository.SaveAsync(It.IsAny<Event>(), It.IsAny<Type>()))
                .ThrowsAsync(new InvalidOperationException());

            _isEventSent = false;
            var eventMessenger = new Mock<IInternalEventMessenger>();
            eventMessenger
                .Setup(bus => bus.Send(_event.GetType(), _event))
                .Callback<Type, Event>((source, evt) => _isEventSent = true)
                .Returns(Task.FromResult(0));

            _sut = new EventDispatcher(eventRepositoryMock.Object, eventMessenger.Object);
        }

        protected override void When()
        {
            try
            {
                _sut.DispatchAsync(_event, typeof(TestEvent)).Wait();
            }
            catch (AggregateException e) when (e.InnerException is InvalidOperationException)
            {
            }
        }

        [Fact]
        public void Then_Event_Should_Not_Be_Sent()
        {
            _isEventSent.Should().BeFalse();
        }
    }
}