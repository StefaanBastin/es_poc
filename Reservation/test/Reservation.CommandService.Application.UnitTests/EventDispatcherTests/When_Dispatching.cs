﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Application.UnitTests.EventDispatcherTests
{
    public class When_Dispatching : Given_When_Then_Test
    {
        private EventDispatcher _sut;
        private Event _event;
        private bool _eventBusCalledAfterEventRepository;

        protected override void Given()
        {
            _event = new TestEvent(Guid.NewGuid());

            var eventRepositoryCalled = false;
            var eventRepositoryMock = new Mock<IEventRepository>();
            eventRepositoryMock
                .Setup(repository => repository.SaveAsync(_event, _event.GetType()))
                .Callback<Event, Type>((evt, source) => eventRepositoryCalled = true)
                .Returns(Task.FromResult(0));

            _eventBusCalledAfterEventRepository = false;
            var eventBusMock = new Mock<IInternalEventMessenger>();
            eventBusMock
                .Setup(bus => bus.Send(_event.GetType(), _event))
                .Callback<Type, Event>((source, evt) =>
                {
                    if (eventRepositoryCalled)
                    {
                        _eventBusCalledAfterEventRepository = true;
                    }
                })
                .Returns(Task.FromResult(0));

            _sut = new EventDispatcher(eventRepositoryMock.Object, eventBusMock.Object);
        }

        protected override void When()
        {
            _sut.DispatchAsync(_event, _event.GetType()).Wait();
        }

        [Fact]
        public void Then_Call_Event_Repository_Before_Putting_On_Bus()
        {
            _eventBusCalledAfterEventRepository.Should().BeTrue();
        }
    }
}