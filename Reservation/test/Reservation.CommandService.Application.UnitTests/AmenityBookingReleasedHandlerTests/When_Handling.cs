﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Messages;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Application.UnitTests.AmenityBookingReleasedHandlerTests
{
    public class When_Handling : Given_When_Then_Test
    {
        private AmenityBookingReleasedEventMessageSender _sut;
        private AmenityBookingReleased _amenityBookingReleased;
        private object _event;
        private Type _eventSource;
        private Guid _amenityId;
        private DateTime _amenityDate;
        private int _quantity;

        protected override void Given()
        {
            _amenityId = Guid.NewGuid();
            _amenityDate = new DateTime(2018, 5, 31);
            _quantity = 4;
            _amenityBookingReleased = new AmenityBookingReleased(_amenityId, default(Guid), _amenityDate, _quantity);

            var eventMessengerMock = new Mock<IBusinessContextSender>();
            eventMessengerMock
                .Setup(messenger => messenger.Send(It.IsAny<Type>(), It.IsAny<object>()))
                .Callback<Type, object>((source, evt) =>
                {
                    _event = evt;
                    _eventSource = source;
                })
                .Returns(Task.FromResult(0));

            _sut = new AmenityBookingReleasedEventMessageSender(eventMessengerMock.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(_amenityBookingReleased).Wait();
        }

        [Fact]
        public void Then_Send_Message()
        {
            _event.Should().BeEquivalentTo(new AmenityBookingReleasedEventMessage
            {
                AmenityId = _amenityId,
                Date = _amenityDate,
                Quantity = 4
            });
            _eventSource.Should().Be(typeof(AmenityBookingReleasedEventMessage));
        }
    }
}