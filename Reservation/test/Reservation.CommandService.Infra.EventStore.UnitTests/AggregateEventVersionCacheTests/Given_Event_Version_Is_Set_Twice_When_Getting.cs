﻿using System;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.EventStore.UnitTests.AggregateEventVersionCacheTests
{
    public class Given_Event_Version_Is_Set_Twice_When_Getting : Given_When_Then_Test
    {
        private AggregateEventVersionCache _sut;
        private Guid _aggregateId;
        private long? _result;

        protected override void Given()
        {
            _aggregateId = Guid.NewGuid();

            _sut = new AggregateEventVersionCache();
            _sut.SetEventVersion(_aggregateId, 669);
            _sut.SetEventVersion(_aggregateId, 700);
        }

        protected override void When()
        {
            _result = _sut.GetEventVersion(_aggregateId);
        }

        [Fact]
        public void Then_Should_Return_Latest_Version()
        {
            _result.Should().Be(700);
        }
    }
}