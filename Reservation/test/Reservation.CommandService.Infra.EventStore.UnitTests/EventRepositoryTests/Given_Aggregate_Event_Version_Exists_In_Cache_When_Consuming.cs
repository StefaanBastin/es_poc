﻿using System;
using EventStore.ClientAPI;
using Moq;
using Reservation.CommandService.Infra.EventStore.Converters;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.EventStore.UnitTests.EventRepositoryTests
{
    public class Given_Aggregate_Event_Version_Exists_In_Cache_When_Consuming : Given_When_Then_Test
    {
        private EventRepository _sut;
        private Guid _aggregateId;
        private Mock<IEventStoreConnection> _eventStoreConnectionMock;
        private Mock<IAggregateEventVersionCache> _aggregateVersionCacheMock;

        protected override void Given()
        {
            _aggregateId = Guid.NewGuid();

            _aggregateVersionCacheMock = new Mock<IAggregateEventVersionCache>();
            _aggregateVersionCacheMock
                .Setup(cache => cache.GetEventVersion(_aggregateId))
                .Returns(554);
            _eventStoreConnectionMock = new Mock<IEventStoreConnection>();
            _sut = new EventRepository(new Mock<IStreamNameGenerator>().Object, _eventStoreConnectionMock.Object, _aggregateVersionCacheMock.Object, new UTF8ByteArrayConverter());
        }

        protected override void When()
        {
            _sut.SaveAsync(new TestEvent(_aggregateId), GetType());
        }

        [Fact]
        public void Then_Append_With_Cached_Version()
        {
            _eventStoreConnectionMock
                .Verify(connection => connection.AppendToStreamAsync(It.IsAny<string>(), 554, It.IsAny<EventData>()), Times.Once);
        }

        [Fact]
        public void Then_Increase_Cached_Version()
        {
            _aggregateVersionCacheMock
                .Verify(cache => cache.SetEventVersion(_aggregateId, 555), Times.Once);
        }
    }
}