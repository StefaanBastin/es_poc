﻿using System;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Infra.EventStore.Converters;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.EventStore.UnitTests.EventRepositoryTests
{
    public class Given_Aggregate_Event_Version_Exists_In_Cache_When_Getting : Given_When_Then_Test
    {
        private EventRepository _sut;
        private Guid _aggregateId;
        private Mock<IEventStoreConnection> _eventStoreConnectionMock;
        private Mock<IAggregateEventVersionCache> _aggregateVersionCacheMock;
        private Func<Task> _act;

        protected override void Given()
        {
            _aggregateId = Guid.NewGuid();

            _aggregateVersionCacheMock = new Mock<IAggregateEventVersionCache>();
            _aggregateVersionCacheMock
                .Setup(cache => cache.GetEventVersion(_aggregateId))
                .Returns(122);

            _eventStoreConnectionMock = new Mock<IEventStoreConnection>();
            _sut = new EventRepository(new Mock<IStreamNameGenerator>().Object, _eventStoreConnectionMock.Object, _aggregateVersionCacheMock.Object, new UTF8ByteArrayConverter());
        }

        protected override void When()
        {
            _act = () => _sut.GetAsync(_aggregateId, default(Type));
        }

        [Fact]
        public void Then_Throw()
        {
            _act.Should().Throw<ArgumentException>();
        }
    }
}