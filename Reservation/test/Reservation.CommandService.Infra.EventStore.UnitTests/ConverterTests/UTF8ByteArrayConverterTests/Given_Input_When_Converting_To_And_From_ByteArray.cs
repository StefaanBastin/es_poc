﻿using FluentAssertions;
using Reservation.CommandService.Infra.EventStore.Converters;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.EventStore.UnitTests.ConverterTests.UTF8ByteArrayConverterTests
{
    public class Given_Input_When_Converting_To_And_From_ByteArray : Given_When_Then_Test
    {
        private UTF8ByteArrayConverter _sut;
        private TestObject _input;
        private TestObject _output;

        protected override void Given()
        {
            _sut = new UTF8ByteArrayConverter();

            _input = new TestObject
            {
                BooleanProperty = true,
                ByteProperty = 15,
                IntegerProperty = 2345,
                FloatProperty = 5379.3749f,
                DoubleProperty = 3803.7393d,
                DecimalProperty = 432543532645745M,
                StringProperty = "Just testing",
                ObjectProperty = new TestObject
                {
                    BooleanProperty = false,
                    ByteProperty = 99,
                    IntegerProperty = 9383,
                    FloatProperty = 2345.9887f,
                    DoubleProperty = 7832.1234d,
                    DecimalProperty = 24354325432678M,
                    StringProperty = "Just testing again",
                }
            };
        }

        protected override void When()
        {
            _output = (TestObject)_sut.FromByteArray(_sut.ToByteArray(_input), typeof(TestObject));
        }

        [Fact]
        public void Then_Output_Should_Equal_Input()
        {
            _input.Should().BeEquivalentTo(_output);
        }
    }
}
