﻿namespace Reservation.CommandService.Infra.EventStore.UnitTests.ConverterTests
{
    public class TestObject
    {
        public bool BooleanProperty { get; set; }

        public byte ByteProperty { get; set; }

        public int IntegerProperty { get; set; }

        public float FloatProperty { get; set; }

        public double DoubleProperty { get; set; }

        public decimal DecimalProperty { get; set; }

        public string StringProperty { get; set; }

        public TestObject ObjectProperty { get; set; }
    }
}
