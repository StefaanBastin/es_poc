﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using ToolBelt.TestSupport;
using TO.CommercialYield.CanonicalModels;
using Xunit;

namespace Reservation.CommandService.AntiCorruption.UnitTests.TUIProductReservationCancelledHandlerTests
{
    public class When_Receiving : Given_When_Then_Test
    {
        private TUIProductReservationCancelledHandler _sut;
        private TUIProductReservationEvent _reservationEvent;
        private Mock<ICommandHandler<CancelReservation>> _commandHandlerMock;
        private CancelReservation _command;
        private Guid _productId;
        private Guid _reservationId;
        private Guid _amenity1Id;
        private Guid _amenity2Id;

        protected override void Given()
        {
            _reservationId = Guid.NewGuid();
            _productId = Guid.NewGuid();
            _amenity1Id = Guid.NewGuid();
            _amenity2Id = Guid.NewGuid();

            _reservationEvent = new TUIProductReservationEvent
            {
                eventType = EventType.TUIProductReservationCancelled,
                payload = new TUIProductReservationPayload
                {
                    TUIProductReservation = new TUIProductReservation
                    {
                        reservationGUID = _reservationId.ToString(),
                        productGUID = _productId.ToString(),
                        reservationSegments = new[]
                        {
                            new ReservationSegment
                            {
                                state = SegmentStatus.Cancelled,
                                scheduledStartDate = new DateTime(2018, 6, 6),
                                scheduledEndDate = new DateTime(2018, 6, 6),
                                assignments = new[]
                                {
                                    new Assignment(),
                                    new Assignment(),
                                    new Assignment()
                                },
                                amenityDetails = new amenityDetails
                                {
                                    amenityDetail = new[]
                                    {
                                        new amenityDetailsAmenityDetail
                                        {
                                            amenityGUID = _amenity1Id.ToString()
                                        }
                                    }
                                }
                            },
                            new ReservationSegment
                            {
                                state = SegmentStatus.Cancelled,
                                scheduledStartDate = new DateTime(2018, 6, 7),
                                scheduledEndDate = new DateTime(2018, 6, 7),
                                assignments = new[]
                                {
                                    new Assignment(),
                                    new Assignment()
                                },
                                amenityDetails = new amenityDetails
                                {
                                    amenityDetail = new[]
                                    {
                                        new amenityDetailsAmenityDetail
                                        {
                                            amenityGUID = _amenity2Id.ToString()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            _commandHandlerMock = new Mock<ICommandHandler<CancelReservation>>();
            _commandHandlerMock
                .Setup(x => x.HandleAsync(It.IsAny<CancelReservation>()))
                .Returns(Task.CompletedTask)
                .Callback((CancelReservation cmd) => _command = cmd);

            _sut = new TUIProductReservationCancelledHandler(_commandHandlerMock.Object, default(IEnterpriseMessageReceiver<TUIProductReservationEvent>), default(ILogger<TUIProductReservationCancelledHandler>));
        }

        protected override void When()
        {
            _sut.OnNext(_reservationEvent);
        }

        [Fact]
        public void Then_Should_Call_CommandHandler()
        {
            var amenityDtos = new[]
            {
                new AmenityDto(_amenity1Id, new DateTime(2018, 6, 6), 3),
                new AmenityDto(_amenity2Id, new DateTime(2018, 6, 7), 2)
            };

            _command.Should().BeEquivalentTo(new CancelReservation(_reservationId));
        }
    }
}