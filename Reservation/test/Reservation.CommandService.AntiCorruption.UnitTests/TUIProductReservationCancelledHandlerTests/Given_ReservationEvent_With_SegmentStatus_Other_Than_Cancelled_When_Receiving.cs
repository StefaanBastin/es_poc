﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using ToolBelt.TestSupport;
using TO.CommercialYield.CanonicalModels;
using Xunit;

namespace Reservation.CommandService.AntiCorruption.UnitTests.TUIProductReservationCancelledHandlerTests
{
    public class Given_ReservationEvent_With_SegmentStatus_Other_Than_Cancelled_When_Receiving : Given_When_Then_Test
    {
        private TUIProductReservationCancelledHandler _sut;
        private TUIProductReservationEvent _reservationEvent;
        private Mock<ICommandHandler<CancelReservation>> _commandHandlerMock;

        protected override void Given()
        {
            _reservationEvent = new TUIProductReservationEvent
            {
                eventType = EventType.TUIProductReservationCancelled,
                payload = new TUIProductReservationPayload
                {
                    TUIProductReservation = new TUIProductReservation
                    {
                        reservationSegments = new[]
                        {
                            new ReservationSegment
                            {
                                state = SegmentStatus.Cancelled,
                            },
                            new ReservationSegment
                            {
                                state = SegmentStatus.OnRequest,
                            }
                        }
                    }
                }
            };

            _commandHandlerMock = new Mock<ICommandHandler<CancelReservation>>();
            _commandHandlerMock
                .Setup(x => x.HandleAsync(It.IsAny<CancelReservation>()))
                .Returns(Task.CompletedTask);

            _sut = new TUIProductReservationCancelledHandler(_commandHandlerMock.Object, default(IEnterpriseMessageReceiver<TUIProductReservationEvent>), default(ILogger<TUIProductReservationCancelledHandler>));
        }

        protected override void When()
        {
            _sut.OnNext(_reservationEvent);
        }

        [Fact]
        public void Then_Should_Not_Call_CommandHandler()
        {
            _commandHandlerMock.Verify(x => x.HandleAsync(It.IsAny<CancelReservation>()), Times.Never);
        }
    }
}