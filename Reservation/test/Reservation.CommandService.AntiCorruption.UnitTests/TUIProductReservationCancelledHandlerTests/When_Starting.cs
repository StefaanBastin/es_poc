﻿using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using TO.CommercialYield.CanonicalModels;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.AntiCorruption.UnitTests.TUIProductReservationCancelledHandlerTests
{
    public class When_Starting : Given_When_Then_Test
    {
        private TUIProductReservationCancelledHandler _sut;
        private Mock<IEnterpriseMessageReceiver<TUIProductReservationEvent>> _enterpriseListenerMock;

        protected override void Given()
        {
            _enterpriseListenerMock = new Mock<IEnterpriseMessageReceiver<TUIProductReservationEvent>>();

            _sut = new TUIProductReservationCancelledHandler(default(ICommandHandler<CancelReservation>), _enterpriseListenerMock.Object, default(ILogger<TUIProductReservationCancelledHandler>));
        }

        protected override void When()
        {
            _sut.Start();
        }

        [Fact]
        public void Then_Should_Subscribe_To_EnterpriseListener()
        {
            _enterpriseListenerMock.Verify(x => x.Subscribe(_sut));
        }
    }
}