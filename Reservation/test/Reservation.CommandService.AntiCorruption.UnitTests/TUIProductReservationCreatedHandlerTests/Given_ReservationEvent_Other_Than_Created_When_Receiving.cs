﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using ToolBelt.TestSupport;
using TO.CommercialYield.CanonicalModels;
using Xunit;

namespace Reservation.CommandService.AntiCorruption.UnitTests.TUIProductReservationCreatedHandlerTests
{
    public class Given_ReservationEvent_Other_Than_Created_When_Receiving : Given_When_Then_Test
    {
        private TUIProductReservationCreatedHandler _sut;
        private TUIProductReservationEvent _reservationEvent;
        private Mock<ICommandHandler<CreateReservation>> _commandHandlerMock;

        protected override void Given()
        {
            _reservationEvent = new TUIProductReservationEvent
            {
                eventType = EventType.TUIProductReservationChanged,
            };

            _commandHandlerMock = new Mock<ICommandHandler<CreateReservation>>();
            _commandHandlerMock
                .Setup(x => x.HandleAsync(It.IsAny<CreateReservation>()))
                .Returns(Task.CompletedTask);

            _sut = new TUIProductReservationCreatedHandler(_commandHandlerMock.Object, default(IEnterpriseMessageReceiver<TUIProductReservationEvent>), default(ILogger<TUIProductReservationCreatedHandler>));
        }

        protected override void When()
        {
            _sut.OnNext(_reservationEvent);
        }

        [Fact]
        public void Then_Should_Not_Call_CommandHandler()
        {
            _commandHandlerMock.Verify(x => x.HandleAsync(It.IsAny<CreateReservation>()), Times.Never);
        }
    }
}