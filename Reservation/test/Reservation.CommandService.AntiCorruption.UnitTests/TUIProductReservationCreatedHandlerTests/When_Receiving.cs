﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using ToolBelt.TestSupport;
using TO.CommercialYield.CanonicalModels;
using Xunit;

namespace Reservation.CommandService.AntiCorruption.UnitTests.TUIProductReservationCreatedHandlerTests
{
    public class When_Receiving : Given_When_Then_Test
    {
        private TUIProductReservationCreatedHandler _sut;
        private TUIProductReservationEvent _reservationEvent;
        private Mock<ICommandHandler<CreateReservation>> _commandHandlerMock;
        private CreateReservation _command;
        private Guid _productId;
        private Guid _amenity1Id;
        private Guid _amenity2Id;

        protected override void Given()
        {
            _productId = Guid.NewGuid();
            _amenity1Id = Guid.NewGuid();
            _amenity2Id = Guid.NewGuid();

            _reservationEvent = new TUIProductReservationEvent
            {
                eventType = EventType.TUIProductReservationCreated,
                payload = new TUIProductReservationPayload
                {
                    TUIProductReservation = new TUIProductReservation
                    {
                        productGUID = _productId.ToString(),
                        reservationSegments = new[]
                        {
                            new ReservationSegment
                            {
                                state = SegmentStatus.Created,
                                scheduledStartDate = new DateTime(2018, 6, 6),
                                scheduledEndDate = new DateTime(2018, 6, 6),
                                assignments = new[]
                                {
                                    new Assignment(),
                                    new Assignment(),
                                    new Assignment()
                                },
                                amenityDetails = new amenityDetails
                                {
                                    amenityDetail = new[]
                                    {
                                        new amenityDetailsAmenityDetail
                                        {
                                            amenityGUID = _amenity1Id.ToString()
                                        }
                                    }
                                }
                            },
                            new ReservationSegment
                            {
                                state = SegmentStatus.Created,
                                scheduledStartDate = new DateTime(2018, 6, 7),
                                scheduledEndDate = new DateTime(2018, 6, 7),
                                assignments = new[]
                                {
                                    new Assignment(),
                                    new Assignment()
                                },
                                amenityDetails = new amenityDetails
                                {
                                    amenityDetail = new[]
                                    {
                                        new amenityDetailsAmenityDetail
                                        {
                                            amenityGUID = _amenity2Id.ToString()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            _commandHandlerMock = new Mock<ICommandHandler<CreateReservation>>();
            _commandHandlerMock
                .Setup(x => x.HandleAsync(It.IsAny<CreateReservation>()))
                .Returns(Task.CompletedTask)
                .Callback((CreateReservation cmd) => _command = cmd);

            _sut = new TUIProductReservationCreatedHandler(_commandHandlerMock.Object, default(IEnterpriseMessageReceiver<TUIProductReservationEvent>), default(ILogger<TUIProductReservationCreatedHandler>));
        }

        protected override void When()
        {
            _sut.OnNext(_reservationEvent);
        }

        [Fact]
        public void Then_Should_Call_CommandHandler()
        {
            var amenityDtos = new[]
            {
                new AmenityDto(_amenity1Id, new DateTime(2018, 6, 6), 3),
                new AmenityDto(_amenity2Id, new DateTime(2018, 6, 7), 2)
            };

            _command.Should().BeEquivalentTo(new CreateReservation(_productId, amenityDtos));
        }
    }
}