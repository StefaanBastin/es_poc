﻿using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using ToolBelt.TestSupport;
using TO.CommercialYield.CanonicalModels;
using Xunit;

namespace Reservation.CommandService.AntiCorruption.UnitTests.TUIProductReservationCreatedHandlerTests
{
    public class When_Starting : Given_When_Then_Test
    {
        private TUIProductReservationCreatedHandler _sut;
        private Mock<IEnterpriseMessageReceiver<TUIProductReservationEvent>> _enterpriseListenerMock;

        protected override void Given()
        {
            _enterpriseListenerMock = new Mock<IEnterpriseMessageReceiver<TUIProductReservationEvent>>();

            _sut = new TUIProductReservationCreatedHandler(default(ICommandHandler<CreateReservation>), _enterpriseListenerMock.Object, default(ILogger<TUIProductReservationCreatedHandler>));
        }

        protected override void When()
        {
            _sut.Start();
        }

        [Fact]
        public void Then_Should_Subscribe_To_EnterpriseListener()
        {
            _enterpriseListenerMock.Verify(x => x.Subscribe(_sut));
        }
    }
}