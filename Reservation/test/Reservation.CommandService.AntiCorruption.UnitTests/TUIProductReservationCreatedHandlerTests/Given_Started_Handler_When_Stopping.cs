﻿using System;
using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using ToolBelt.TestSupport;
using TO.CommercialYield.CanonicalModels;
using Xunit;

namespace Reservation.CommandService.AntiCorruption.UnitTests.TUIProductReservationCreatedHandlerTests
{
    public class Given_Started_Handler_When_Stopping : Given_When_Then_Test
    {
        private TUIProductReservationCreatedHandler _sut;
        private Mock<IEnterpriseMessageReceiver<TUIProductReservationEvent>> _enterpriseListenerMock;
        private Mock<IDisposable> _disposableMock;

        protected override void Given()
        {
            _disposableMock = new Mock<IDisposable>();

            _enterpriseListenerMock = new Mock<IEnterpriseMessageReceiver<TUIProductReservationEvent>>();
            _enterpriseListenerMock
                .Setup(x => x.Subscribe(It.IsAny<IObserver<TUIProductReservationEvent>>()))
                .Returns(_disposableMock.Object);

            _sut = new TUIProductReservationCreatedHandler(default(ICommandHandler<CreateReservation>), _enterpriseListenerMock.Object, default(ILogger<TUIProductReservationCreatedHandler>));

            _sut.Start();
        }

        protected override void When()
        {
            _sut.OnCompleted();
        }

        [Fact]
        public void Then_Should_Unsubscribe()
        {
            _disposableMock.Verify(x => x.Dispose());
        }
    }
}