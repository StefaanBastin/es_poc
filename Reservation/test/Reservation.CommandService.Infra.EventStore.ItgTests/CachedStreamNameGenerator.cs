﻿using System;
using System.Collections.Generic;

namespace Reservation.CommandService.Infra.EventStore.ItgTests
{
    public class CachedStreamNameGenerator : IStreamNameGenerator
    {
        private readonly IStreamNameGenerator _streamNameGenerator;
        public IList<string> GeneratedStreamNames { get; }

        public CachedStreamNameGenerator(IStreamNameGenerator streamNameGenerator)
        {
            _streamNameGenerator = streamNameGenerator;

            GeneratedStreamNames = new List<string>();
        }

        public string Generate(Guid aggregateId, Type source)
        {
            var generatedName = _streamNameGenerator.Generate(aggregateId, source);

            if (!GeneratedStreamNames.Contains(generatedName))
            {
                GeneratedStreamNames.Add(generatedName);
            }

            return generatedName;
        }
    }
}