﻿using System;
using System.Linq;
using Autofac;
using Autofac.Builder;
using EventStore.ClientAPI;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Host.ConsoleApp;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.EventStore.Converters;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.EventStore.ItgTests.EventRepositoryTests
{
    [Collection("EventStoreDocker collection")]
    public class When_Consuming_Events : Given_When_Then_Test
    {
        private EventRepository _sut;
        private TestAmountEvent _evt;
        private Guid _aggregateId;
        private IEventStoreConnection _eventStoreConnection;
        private CachedStreamNameGenerator _cachedStreamNameGenerator;

      protected override void Given()
        {
            _aggregateId = Guid.NewGuid();
            _evt = new TestAmountEvent(_aggregateId, 7);

            var container = new ContainerBuilderFactory().Create().Build(ContainerBuildOptions.IgnoreStartableComponents);

            container.StartStartable<IEventStoreConfiguration>();
            container.StartStartable<EventStoreConnectionCreator>();

            _eventStoreConnection = container.Resolve<IEventStoreConnection>();

            _cachedStreamNameGenerator = new CachedStreamNameGenerator(container.Resolve<IStreamNameGenerator>());

            _sut = new EventRepository(
                _cachedStreamNameGenerator,
                _eventStoreConnection,
                new Mock<IAggregateEventVersionCache>().Object,
                container.Resolve<IByteArrayConverter>());
        }

        protected override void When()
        {
            _sut.SaveAsync(_evt, typeof(TestAggregate)).Wait();
        }

        [Fact]
        public void Then_The_Event_Should_Be_Persisted_Correctly()
        {
            var evt = _sut.GetAsync(_aggregateId, typeof(TestAggregate))
                .Result
                .Single();
            evt.Should().BeEquivalentTo(new TestAmountEvent(_aggregateId, 7));
        }

        protected override void Cleanup()
        {
            foreach (var generatedStreamName in _cachedStreamNameGenerator.GeneratedStreamNames)
            {
                _eventStoreConnection.DeleteStreamAsync(generatedStreamName, ExpectedVersion.Any, hardDelete: true).Wait();
            }
        }
    }
}