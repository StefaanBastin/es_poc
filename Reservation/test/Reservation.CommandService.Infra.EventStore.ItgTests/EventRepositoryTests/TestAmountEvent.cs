﻿using System;
using Reservation.CommandService.TestInfra;

namespace Reservation.CommandService.Infra.EventStore.ItgTests.EventRepositoryTests
{
    public class TestAmountEvent : TestEvent
    {
        public int TestAmount { get; }

        public TestAmountEvent(Guid aggregateId, int testAmount)
            : base(aggregateId)
        {
            TestAmount = testAmount;
        }
    }
}