﻿using System;

namespace Reservation.CommandService.Infra.EventStore.ItgTests
{
    public class EventStoreDockerFixture : IDisposable
    {
        private readonly bool _isRunningBeforeStart;

        public EventStoreDockerFixture()
        {
            _isRunningBeforeStart = DockerRun("inspect -f {{.State.Running}} eventstore-node") == "true\n";

            if (!_isRunningBeforeStart)
            {
                DockerRun("run --name eventstore-node -p 2113:2113 -p 1113:1113 eventstore/eventstore", false);
            }
        }

        public void Dispose()
        {
            if (!_isRunningBeforeStart)
            {
                DockerRun("rm eventstore-node -f");
            }
        }

        private static string DockerRun(string command, bool waitForResult = true)
        {
            var startInfo =
                new System.Diagnostics.ProcessStartInfo
                {
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                    FileName = "docker",
                    Arguments = command,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                };
            var process = new System.Diagnostics.Process
            {
                StartInfo = startInfo
            };
            process.Start();
            return waitForResult ? process.StandardOutput.ReadToEnd() : string.Empty;
        }
    }
}