﻿using System;
using System.Collections.Generic;
using Autofac;
using Autofac.Builder;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.EventStore;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.ItgTests
{
    public class Given_Reservation_Is_Booked_When_Dispatching : Given_When_Then_Test
    {
        private Entities.Reservation _reservation;
        private IEventDispatcher _eventDispatcher;
        private ReservationBooked _reservationBooked;

        protected override void Given()
        {
            var container = new ContainerBuilderFactory().Create().Build(ContainerBuildOptions.IgnoreStartableComponents);

            container = container.Remove(new[]
            {
                typeof(EventRepository)
            });

            var reservationBookedHandlerMock = new Mock<IEventHandler<ReservationBooked>>();
            reservationBookedHandlerMock
                .Setup(handler => handler.HandleAsync(It.IsAny<ReservationBooked>()))
                .Callback<ReservationBooked>(reservationBooked => _reservationBooked = reservationBooked);

            container = container.Add(containerBuilder =>
            {
                containerBuilder.RegisterInstance(reservationBookedHandlerMock.Object).As<IEventHandler>();
                containerBuilder.RegisterType<FakeEventRepository>()
                    .As<IEventRepository>();
            });

            _eventDispatcher = container.Resolve<IEventDispatcher>();

            var reservationId = Guid.NewGuid();
            var amenityId = Guid.NewGuid();
            _reservation = new Entities.Reservation(new List<Event>
            {
                new ReservationCreated(reservationId, new List<AmenityDto> { new AmenityDto(amenityId, default(DateTime), default(int)) }),
                new AmenityBooked(amenityId, reservationId, default(DateTime))
            });
        }

        protected override void When()
        {
            _reservation.DispatchAsync(_eventDispatcher).Wait();
        }

        [Fact]
        public void Then_Reservation_Booked_Handler_Should_Be_Called()
        {
            _reservationBooked.Should().NotBeNull();
        }
    }
}