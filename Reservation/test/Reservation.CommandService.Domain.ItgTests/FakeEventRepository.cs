﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reservation.CommandService.Domain.ItgTests
{
    public class FakeEventRepository : IEventRepository
    {
        private IDictionary<Guid, IList<Event>> _events = new Dictionary<Guid, IList<Event>>();

        public Task SaveAsync(Event evt, Type source)
        {
            return Task.Factory.StartNew(() =>
            {
                if (!_events.TryGetValue(evt.AggregateId, out var events))
                {
                    _events.Add(evt.AggregateId, new List<Event> { evt });
                    return;
                }

                events.Add(evt);
            });
        }

        public Task<IList<Event>> GetAsync(Guid aggregateId, Type aggregateType)
        {
            return Task<IList<Event>>.Factory.StartNew(() => _events[aggregateId]);
        }
    }
}