﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Core.Lifetime;

namespace Reservation.CommandService.Domain.ItgTests
{
    public static class ContainerBuilderExtensions
    {
        public static IContainer Remove(this IContainer container, IEnumerable<Type> typesToRemove)
        {
            var components = FilterComponents(container, typesToRemove);

            var builder = CreateContainerBuilder(container.ComponentRegistry.Sources, components);

            var newContainer = builder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            return newContainer;
        }

        public static IContainer Add(this IContainer container, Action<ContainerBuilder> containerBuilder)
        {
            var builder = CreateContainerBuilder(container.ComponentRegistry.Sources, container.ComponentRegistry.Registrations);

            containerBuilder(builder);
            var newContainer = builder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            return newContainer;
        }

        private static IEnumerable<IComponentRegistration> FilterComponents(IComponentContext container, IEnumerable<Type> typesToRemove)
        {
            var components = container.ComponentRegistry.Registrations
                .Where(cr => cr.Activator.LimitType != typeof(LifetimeScope))
                .Where(cr => !typesToRemove.Contains(cr.Activator.LimitType));

            return components;
        }

        private static ContainerBuilder CreateContainerBuilder(IEnumerable<IRegistrationSource> registrationSources, IEnumerable<IComponentRegistration> componentRegistrations)
        {
            var builder = new ContainerBuilder();

            foreach (var c in componentRegistrations)
            {
                builder.RegisterComponent(c);
            }

            foreach (var source in registrationSources)
            {
                builder.RegisterSource(source);
            }

            return builder;
        }
    }
}