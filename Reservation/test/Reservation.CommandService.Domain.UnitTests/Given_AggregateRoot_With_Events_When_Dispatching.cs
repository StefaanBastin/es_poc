﻿using System;
using System.Threading.Tasks;
using Moq;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests
{
    public class Given_AggregateRoot_With_Events_When_Dispatching : Given_When_Then_Test
    {
        private TestAggregate _sut;

        protected override void Given()
        {
            _sut = new TestAggregate();
            _sut.AddTestEvent();
        }

        protected override void When()
        {
            _sut.DispatchAsync(new TestEventDispatcher()).Wait();
        }

        [Fact]
        public void Then_Should_Not_Allow_Consumed_Events_To_Be_Dispatched_Again()
        {
            var failingEventDispatcher = new Mock<IEventDispatcher>();
            failingEventDispatcher
                .Setup(x => x.DispatchAsync(It.IsAny<Event>(), It.IsAny<Type>()))
                .Returns(Task.CompletedTask)
                .Callback((Event e, Type t) => throw new InvalidOperationException($"Tried to consume {e.GetType().Name} of {t.Name} a second time. " +
                                                                                   "This should not occur because the events should be cleared after dispatching."));

            _sut.DispatchAsync(failingEventDispatcher.Object).Wait();
        }
    }
}