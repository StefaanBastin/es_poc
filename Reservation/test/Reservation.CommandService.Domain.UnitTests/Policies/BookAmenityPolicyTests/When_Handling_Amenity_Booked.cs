﻿using System;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.InventoryEvents;
using Reservation.CommandService.Domain.Policies;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Policies.BookAmenityPolicyTests
{
    public class When_Handling_Amenity_Booked : Given_When_Then_Test
    {
        private readonly Guid _amenityId = Guid.NewGuid();
        private BookAmenityPolicy _sut;
        private BookAmenity _bookAmenity;
        private Guid _reservationId;

        protected override void Given()
        {
            var commandHandlerMock = new Mock<ICommandHandler<BookAmenity>>();
            commandHandlerMock
                .Setup(handler => handler.HandleAsync(It.IsAny<BookAmenity>()))
                .Callback<BookAmenity>(checkAmenity => _bookAmenity = checkAmenity);

            _sut = new BookAmenityPolicy(commandHandlerMock.Object);
        }

        protected override void When()
        {
            _reservationId = Guid.NewGuid();
            _sut.HandleAsync(new AmenityTaken(_amenityId, new DateTime(2018, 4, 27), 666, _reservationId));
        }

        [Fact]
        public void Then_Should_Check_Amenity()
        {
            _bookAmenity.Should().BeEquivalentTo(
                new BookAmenity(_amenityId, new DateTime(2018, 4, 27), _reservationId));
        }
    }
}