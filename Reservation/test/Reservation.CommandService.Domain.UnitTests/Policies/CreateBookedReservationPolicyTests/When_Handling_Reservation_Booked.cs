﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Domain.Policies;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Policies.CreateBookedReservationPolicyTests
{
    public class When_Handling_Reservation_Booked : Given_When_Then_Test
    {
        private CreateBookedReservationPolicy _sut;
        private CreateBookedReservation _command;

        protected override void Given()
        {
            var commandHandlerMock = new Mock<ICommandHandler<CreateBookedReservation>>();

            commandHandlerMock
                .Setup(handler => handler.HandleAsync(It.IsAny<CreateBookedReservation>()))
                .Returns(Task.CompletedTask)
                .Callback<CreateBookedReservation>(command => _command = command);

            _sut = new CreateBookedReservationPolicy(commandHandlerMock.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(new ReservationBooked(Guid.NewGuid(), new List<AmenityDto>())).Wait();
        }

        [Fact]
        public void Then_Should_Handle_Command()
        {
            _command
                .Should()
                .NotBeNull();
        }
    }
}