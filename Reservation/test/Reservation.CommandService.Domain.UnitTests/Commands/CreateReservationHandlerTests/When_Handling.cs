﻿using System;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Commands.CreateReservationHandlerTests
{
    public class When_Handling : Given_When_Then_Test
    {
        private CreateReservationHandler _sut;
        private ReservationCreated _reservationCreated;
        private Guid _productId;
        private AmenityDto[] _amenities;
        private TestEventDispatcher _testEventDispatcher;

        protected override void Given()
        {
            _productId = Guid.NewGuid();

            var amenityId1 = Guid.NewGuid();
            var amenityId2 = Guid.NewGuid();
            _amenities = new[]
            {
                new AmenityDto(amenityId1, new DateTime(2018, 5, 2), 6),
                new AmenityDto(amenityId1, new DateTime(2018, 5, 3), 6),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 4), 7),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 5), 7),
            };

            _testEventDispatcher = new TestEventDispatcher();

            _sut = new CreateReservationHandler(_testEventDispatcher);
        }

        protected override void When()
        {
            _sut.HandleAsync(new CreateReservation(_productId, _amenities)).Wait();
        }

        [Fact]
        public void Then_It_Should_Handle_The_Command()
        {
            _reservationCreated = _testEventDispatcher.Events.OfType<ReservationCreated>().Single();

            _reservationCreated.AggregateId.Should().NotBe(Guid.Empty);

            _reservationCreated
                .Should()
                .BeEquivalentTo(
                    new ReservationCreated(Guid.Empty, _amenities),
                    options => options
                        .Excluding(reservationCreated => reservationCreated.AggregateId));
        }
    }
}