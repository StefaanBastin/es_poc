﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Commands.CreateBookedReservationHandlerTests
{
    public class When_Handling : Given_When_Then_Test
    {
        private CreateBookedReservationHandler _sut;
        private TestEventDispatcher _testEventDispatcher;
        private IEnumerable<AmenityDto> _amenities;

        protected override void Given()
        {
            _testEventDispatcher = new TestEventDispatcher();

            _amenities = new List<AmenityDto>();

            _sut = new CreateBookedReservationHandler(_testEventDispatcher);
        }

        protected override void When()
        {
            _sut.HandleAsync(new CreateBookedReservation(_amenities));
        }

        [Fact]
        public void Then_Should_Create_Booked_Reservation()
        {
            var bookedReservationCreated = _testEventDispatcher.Events.Single();

            bookedReservationCreated.AggregateId.Should().NotBe(Guid.Empty);

            bookedReservationCreated.Should().NotBeNull();
        }
    }
}