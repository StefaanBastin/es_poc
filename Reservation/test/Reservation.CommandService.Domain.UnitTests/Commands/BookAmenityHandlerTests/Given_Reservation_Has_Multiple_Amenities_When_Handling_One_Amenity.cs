﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Application;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Commands.BookAmenityHandlerTests
{
    public class Given_Reservation_Has_Multiple_Amenities_When_Handling_One_Amenity : Given_When_Then_Test
    {
        private BookAmenityHandler _sut;
        private AmenityBooked _amenityBookedEvent;
        private Guid _amenityId;
        private DateTime _amenityDate;
        private Domain.Entities.Reservation _reservation;
        private AmenityDto[] _amenities;

        protected override void Given()
        {
            _amenityDate = new DateTime(2018, 4, 27);
            _amenityId = Guid.NewGuid();

            var reservationRepositoryMock = new Mock<IRepository<Domain.Entities.Reservation>>();
            var amenityId2 = Guid.NewGuid();
            _amenities = new[]
            {
                new AmenityDto(_amenityId, new DateTime(2018, 4, 26), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 27), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 28), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 29), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 30), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 1), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 2), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 3), 6),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 4), 7),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 5), 7)
            };
            _reservation = new Domain.Entities.Reservation(_amenities);

            reservationRepositoryMock
                .Setup(repo => repo.GetAsync(_reservation.Id))
                .Returns(Task.FromResult(_reservation));

            var eventRepositoryMock = new Mock<IEventRepository>();
            eventRepositoryMock
                .Setup(consumer => consumer.SaveAsync(It.IsAny<AmenityBooked>(), typeof(Domain.Entities.Reservation)))
                .Returns(Task.CompletedTask)
                .Callback<Event, Type>((evt, source) => _amenityBookedEvent = (AmenityBooked)evt);

            _sut = new BookAmenityHandler(
                new EventDispatcher(eventRepositoryMock.Object, new Mock<IInternalEventMessenger>().Object),
                reservationRepositoryMock.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(new BookAmenity(_amenityId, _amenityDate, _reservation.Id)).Wait();
        }

        [Fact]
        public void Then_Amenity_Is_Marked_As_Booked()
        {
            _amenityBookedEvent
                .Should()
                .BeEquivalentTo(new AmenityBooked(_amenityId, _reservation.Id, _amenityDate));
        }
    }
}