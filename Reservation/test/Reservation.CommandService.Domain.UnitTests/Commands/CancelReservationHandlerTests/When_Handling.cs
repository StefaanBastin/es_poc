﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Entities;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Commands.CancelReservationHandlerTests
{
    public class When_Handling : Given_When_Then_Test
    {
        private CancelReservationHandler _sut;
        private CancelReservation _cancelReservation;
        private TestEventDispatcher _eventDispatcher;
        private Guid _reservationId;

        protected override void Given()
        {
            _reservationId = Guid.NewGuid();
            var bookedReservation = new BookedReservation(new List<Event>
            {
                new BookedReservationCreated(_reservationId, new List<AmenityDto>())
            });
            _cancelReservation = new CancelReservation(_reservationId);

            _eventDispatcher = new TestEventDispatcher();

            var reservationRepository = new Mock<IRepository<BookedReservation>>();
            reservationRepository
                .Setup(repository => repository.GetAsync(_reservationId))
                .Returns(Task.FromResult(bookedReservation));

            _sut = new CancelReservationHandler(_eventDispatcher, reservationRepository.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(_cancelReservation).Wait();
        }

        [Fact]
        public void Then_Assert()
        {
            _eventDispatcher.Events.Should().BeEquivalentTo(new List<Event>
            {
                new ReservationCancelled(_reservationId)
            });
        }
    }
}