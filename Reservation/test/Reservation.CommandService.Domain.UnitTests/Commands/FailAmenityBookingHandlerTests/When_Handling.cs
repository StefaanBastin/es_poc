﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Application;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Commands.FailAmenityBookingHandlerTests
{
    public class When_Handling : Given_When_Then_Test
    {
        private FailAmenityBookingHandler _sut;
        private DateTime _amenityDate;
        private Guid _amenityId;
        private AmenityBookingFailed _amenityBookingFailed;
        private ReservationFailed _reservationFailed;
        private AmenityDto[] _amenities;
        private Domain.Entities.Reservation _reservation;

        protected override void Given()
        {
            _amenityId = Guid.NewGuid();
            _amenityDate = new DateTime(2018, 4, 27);

            var eventRepositoryMock = new Mock<IEventRepository>();
            eventRepositoryMock
                .Setup(consumer => consumer.SaveAsync(It.IsAny<AmenityBookingFailed>(), typeof(Domain.Entities.Reservation)))
                .Returns(Task.CompletedTask)
                .Callback<Event, Type>((evt, source) => _amenityBookingFailed = (AmenityBookingFailed)evt);

            eventRepositoryMock
                .Setup(consumer => consumer.SaveAsync(It.IsAny<ReservationFailed>(), typeof(Domain.Entities.Reservation)))
                .Returns(Task.CompletedTask)
                .Callback<Event, Type>((evt, source) => _reservationFailed = (ReservationFailed)evt);

            var reservationRepositoryMock = new Mock<IRepository<Domain.Entities.Reservation>>();
            Guid.NewGuid();
            var amenityId2 = Guid.NewGuid();
            _amenities = new[]
            {
                new AmenityDto(_amenityId, new DateTime(2018, 4, 26), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 27), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 28), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 29), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 4, 30), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 1), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 2), 6),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 3), 6),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 4), 7),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 5), 7)
            };
            _reservation = new Domain.Entities.Reservation(_amenities);

            reservationRepositoryMock
                .Setup(repo => repo.GetAsync(_reservation.Id))
                .Returns(Task.FromResult(_reservation));

            _sut = new FailAmenityBookingHandler(
                new EventDispatcher(eventRepositoryMock.Object, new Mock<IInternalEventMessenger>().Object),
                reservationRepositoryMock.Object);
        }

        protected override void When()
        {
            _sut.HandleAsync(new FailAmenityBooking(_amenityId, _amenityDate, _reservation.Id)).Wait();
        }

        [Fact]
        public void Then_Amenity_Booked_Is_Failed()
        {
            _amenityBookingFailed
                .Should()
                .BeEquivalentTo(new AmenityBookingFailed(_amenityId, _reservation.Id, _amenityDate));
        }

        [Fact]
        public void Then_Reservation_Is_Failed()
        {
            _reservationFailed
                .Should()
                .BeEquivalentTo(new ReservationFailed(_reservation.Id));
        }
    }
}