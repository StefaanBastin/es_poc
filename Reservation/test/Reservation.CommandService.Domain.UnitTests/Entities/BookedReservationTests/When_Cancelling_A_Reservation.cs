﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Entities;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.BookedReservationTests
{
    public class When_Cancelling_A_Reservation
        : Given_When_Then_Test
    {
        private List<Event> _events;
        private BookedReservation _sut;
        private Guid _amenityId;
        private Guid _reservationId;

        protected override void Given()
        {
            _reservationId = Guid.NewGuid();
            _amenityId = Guid.NewGuid();
            var amenities = new List<AmenityDto>
            {
                new AmenityDto(_amenityId, new DateTime(2018, 5, 30), 2),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 31), 2),
                new AmenityDto(_amenityId, new DateTime(2018, 6, 1), 2)
            };
            _events = new List<Event>
            {
                new BookedReservationCreated(_reservationId, amenities)
            };

            _sut = new BookedReservation(_events);
        }

        protected override void When()
        {
            _sut.Cancel();
        }

        [Fact]
        public void Then_Should_Cancel_Reservation()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<ReservationCancelled>()
                .Should()
                .BeEquivalentTo(new ReservationCancelled(_reservationId));
        }

        [Fact]
        public void Then_Should_Release_All_Amenities()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<AmenityBookingReleased>()
                .Should()
                .BeEquivalentTo(
                    new AmenityBookingReleased(_amenityId, _sut.Id, new DateTime(2018, 5, 30), 2),
                    new AmenityBookingReleased(_amenityId, _sut.Id, new DateTime(2018, 5, 31), 2),
                    new AmenityBookingReleased(_amenityId, _sut.Id, new DateTime(2018, 6, 1), 2));
        }
    }
}