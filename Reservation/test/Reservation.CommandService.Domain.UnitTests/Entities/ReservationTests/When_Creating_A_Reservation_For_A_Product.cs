﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Creating_A_Reservation_For_A_Product : Given_When_Then_Test
    {
        private Domain.Entities.Reservation _sut;
        private List<AmenityDto> _amenities;

        protected override void Given()
        {
            var amenityId1 = Guid.NewGuid();
            var amenityId2 = Guid.NewGuid();
            _amenities = new List<AmenityDto>
            {
                new AmenityDto(amenityId1, new DateTime(2018, 5, 1), 5),
                new AmenityDto(amenityId1, new DateTime(2018, 5, 2), 5),
                new AmenityDto(amenityId1, new DateTime(2018, 5, 3), 5),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 4), 5),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 5), 5),
                new AmenityDto(amenityId2, new DateTime(2018, 5, 6), 5)
            };
        }

        protected override void When()
        {
            _sut = new Domain.Entities.Reservation(_amenities);
        }

        [Fact]
        public void Then_Reservation_Should_Create_Reservation_Created()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .Single()
                .Should()
                .BeEquivalentTo(
                    new ReservationCreated(Guid.Empty, _amenities),
                    options => options
                        .Excluding(reservationCreated => reservationCreated.AggregateId));
        }
    }
}
