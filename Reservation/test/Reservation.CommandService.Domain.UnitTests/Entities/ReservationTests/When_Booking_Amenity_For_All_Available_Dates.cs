﻿using System;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Booking_Amenity_For_All_Available_Dates : Given_When_Then_Test
    {
        private Domain.Entities.Reservation _sut;
        private Guid _amenityId;
        private DateTime _amenityDate1;
        private DateTime _amenityDate2;

        protected override void Given()
        {
            _amenityId = Guid.NewGuid();
            _amenityDate1 = new DateTime(2018, 5, 1);
            _amenityDate2 = new DateTime(2018, 5, 2);

            _sut = new Domain.Entities.Reservation(new[]
            {
                new AmenityDto(_amenityId, new DateTime(2018, 5, 1), 1000),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 2), 1000),
                new AmenityDto(Guid.NewGuid(), new DateTime(2018, 5, 1), 1000)
            });
        }

        protected override void When()
        {
            _sut.BookAmenity(_amenityId, _amenityDate1);
            _sut.BookAmenity(_amenityId, _amenityDate2);
        }

        [Fact]
        public void Then_Should_Create_Matching_Amenity_Marked_As_Booked_Events()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<AmenityBooked>()
                .Should()
                .BeEquivalentTo(
                    new AmenityBooked(_amenityId, _sut.Id, _amenityDate1),
                    new AmenityBooked(_amenityId, _sut.Id, _amenityDate2));
        }

        [Fact]
        public void Then_Should_Not_Create_Reservation_Booked_Event()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<ReservationBooked>()
                .Should()
                .BeEmpty();
        }
    }
}