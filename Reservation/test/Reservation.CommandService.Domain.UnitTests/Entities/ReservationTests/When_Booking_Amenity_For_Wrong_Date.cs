﻿using System;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Booking_Amenity_For_Wrong_Date : Given_When_Then_Test
    {
        private Domain.Entities.Reservation _sut;
        private Guid _amenityId;
        private DateTime _amenityDate;
        private ArgumentException _actualException;

        protected override void Given()
        {
            _amenityId = Guid.NewGuid();
            _amenityDate = new DateTime(2018, 4, 1);

            _sut = new Domain.Entities.Reservation(new[]
            {
                new AmenityDto(_amenityId, new DateTime(2018, 5, 1), 1000),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 2), 1000)
            });
        }

        protected override void When()
        {
            try
            {
                _sut.BookAmenity(_amenityId, _amenityDate);
            }
            catch (ArgumentException e)
            {
                _actualException = e;
            }
        }

        [Fact]
        public void Then_Should_Throw_Exception()
        {
            _actualException.Should().NotBeNull();
        }

        [Fact]
        public void Then_Should_Not_Create_Amenity_Marked_As_Booked_Events()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<AmenityBooked>()
                .Should()
                .BeEmpty();
        }
    }
}