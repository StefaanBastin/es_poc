﻿using System;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Booking_All_Amenities : Given_When_Then_Test
    {
        private Domain.Entities.Reservation _sut;
        private Guid _amenityId1;
        private Guid _amenityId2;
        private DateTime _amenityDate1;
        private DateTime _amenityDate2;
        private AmenityDto[] _amenities;

        protected override void Given()
        {
            _amenityId1 = Guid.NewGuid();
            _amenityId2 = Guid.NewGuid();
            _amenityDate1 = new DateTime(2018, 5, 1);
            _amenityDate2 = new DateTime(2018, 5, 2);

            _amenities = new[]
            {
                new AmenityDto(_amenityId1, new DateTime(2018, 5, 1), 1000),
                new AmenityDto(_amenityId1, new DateTime(2018, 5, 2), 1000),
                new AmenityDto(_amenityId2, new DateTime(2018, 5, 1), 1000)
            };
            _sut = new Domain.Entities.Reservation(_amenities);
        }

        protected override void When()
        {
            _sut.BookAmenity(_amenityId1, _amenityDate1);
            _sut.BookAmenity(_amenityId1, _amenityDate2);
            _sut.BookAmenity(_amenityId2, _amenityDate1);
        }

        [Fact]
        public void Then_Should_Create_All_Amenity_Marked_As_Booked_Events()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<AmenityBooked>()
                .Should()
                .BeEquivalentTo(
                    new AmenityBooked(_amenityId1, _sut.Id, _amenityDate1),
                    new AmenityBooked(_amenityId1, _sut.Id, _amenityDate2),
                    new AmenityBooked(_amenityId2, _sut.Id, _amenityDate1));
        }

        [Fact]
        public void Then_Should_Create_Reservation_Booked_Event()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<ReservationBooked>()
                .Single()
                .Should()
                .BeEquivalentTo(new ReservationBooked(_sut.Id, _amenities));
        }
    }
}