﻿using System;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Failing_Booking_All_Amenities : Given_When_Then_Test
    {
        private Domain.Entities.Reservation _sut;
        private Guid _amenityId1;
        private Guid _amenityId2;
        private DateTime _amenityDate1;
        private DateTime _amenityDate2;

        protected override void Given()
        {
            _amenityId1 = Guid.NewGuid();
            _amenityId2 = Guid.NewGuid();
            _amenityDate1 = new DateTime(2018, 5, 1);
            _amenityDate2 = new DateTime(2018, 5, 2);

            _sut = new Domain.Entities.Reservation(new[]
            {
                new AmenityDto(_amenityId1, new DateTime(2018, 5, 1), 1000),
                new AmenityDto(_amenityId1, new DateTime(2018, 5, 2), 1000),
                new AmenityDto(_amenityId2, new DateTime(2018, 5, 1), 1000)
            });
        }

        protected override void When()
        {
            _sut.FailAmenityBooking(_amenityId1, _amenityDate1);
            _sut.FailAmenityBooking(_amenityId1, _amenityDate2);
            _sut.FailAmenityBooking(_amenityId2, _amenityDate1);
        }

        [Fact]
        public void Then_Should_Create_All_Amenity_Marked_Failed_Events()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<AmenityBookingFailed>()
                .Should()
                .BeEquivalentTo(
                    new AmenityBookingFailed(_amenityId1, _sut.Id, _amenityDate1),
                    new AmenityBookingFailed(_amenityId1, _sut.Id, _amenityDate2),
                    new AmenityBookingFailed(_amenityId2, _sut.Id, _amenityDate1));
        }

        [Fact]
        public void Then_Should_Create_None_Amenity_Marked_As_Booked_Events()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<AmenityBooked>()
                .Should()
                .BeEmpty();
        }

        [Fact]
        public void Then_Should_Not_Create_Reservation_Booked_Event()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<ReservationBooked>()
                .Should()
                .BeEmpty();
        }

        [Fact]
        public void Then_Should_Create_Reservation_Faild_Event()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<ReservationFailed>()
                .Single()
                .Should()
                .BeEquivalentTo(new ReservationFailed(_sut.Id));
        }
    }
}