﻿using System;
using System.Linq;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class Given_Booked_Amenity_When_Booking : Given_When_Then_Test
    {
        private Domain.Entities.Reservation _sut;
        private Guid _amenityId;
        private InvalidOperationException _actualException;

        protected override void Given()
        {
            _amenityId = Guid.NewGuid();
            var amenities = new[]
            {
                new AmenityDto(_amenityId, new DateTime(2018, 5, 1), 2),
                new AmenityDto(_amenityId, new DateTime(2018, 5, 2), 2)
            };

            _sut = new Domain.Entities.Reservation(amenities);
            _sut.BookAmenity(_amenityId, new DateTime(2018, 5, 2));
        }

        protected override void When()
        {
            try
            {
                _sut.BookAmenity(_amenityId, new DateTime(2018, 5, 2));
            }
            catch (InvalidOperationException e)
            {
                _actualException = e;
            }
        }

        [Fact]
        public void Then_Reservation_Should_Not_Contain_Extra_Amenity_Booked_Event()
        {
            var fakeEventDispatcher = new TestEventDispatcher();
            _sut.DispatchAsync(fakeEventDispatcher).Wait();

            fakeEventDispatcher
                .Events
                .OfType<AmenityBooked>()
                .Count()
                .Should()
                .Be(1);

            fakeEventDispatcher
                .Events
                .OfType<AmenityBooked>()
                .Single()
                .Should()
                .BeEquivalentTo(new AmenityBooked(_amenityId, _sut.Id, new DateTime(2018, 5, 2)));
        }

        [Fact]
        public void Then_Should_Throw_Exception()
        {
            _actualException.Should().NotBeNull();
        }
    }
}