﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Domain.Exceptions;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Creating_With_Reservation_Booked_Event : Given_When_Then_Test
    {
        private IList<Event> _events;
        private Action _act;

        protected override void Given()
        {
            var id = Guid.NewGuid();
            IEnumerable<AmenityDto> amenities = new List<AmenityDto>();
            _events = new List<Event>
            {
                new ReservationCreated(id, amenities),
                new ReservationBooked(id, amenities)
            };
        }

        protected override void When()
        {
            _act = () =>
            {
                var unused = new Domain.Entities.Reservation(_events);
            };
        }

        [Fact]
        public void Then_Throw()
        {
            _act.Should().Throw<AggregateNotFoundException>();
        }
    }
}