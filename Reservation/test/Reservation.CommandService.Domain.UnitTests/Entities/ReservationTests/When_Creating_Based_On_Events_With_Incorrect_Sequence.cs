﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Events;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Creating_Based_On_Events_With_Incorrect_Sequence : Given_When_Then_Test
    {
        private Action _act;
        private IList<Event> _events;

        protected override void Given()
        {
            Guid id;
            _events = new List<Event>
            {
                new ReservationFailed(id),
                new ReservationCreated(id, new List<AmenityDto>())
            };
        }

        protected override void When()
        {
            _act = () =>
            {
                var unused = new Domain.Entities.Reservation(_events);
            };
        }

        [Fact]
        public void Then_Throw()
        {
            _act.Should().Throw<ArgumentException>();
        }
    }
}