﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.ReservationTests
{
    public class When_Creating_Based_On_Empty_Events_List : Given_When_Then_Test
    {
        private Action _act;
        private IList<Event> _events;

        protected override void Given()
        {
            _events = new List<Event>();
        }

        protected override void When()
        {
            _act = () =>
            {
                var unused = new Domain.Entities.Reservation(_events);
            };
        }

        [Fact]
        public void Then_Throw()
        {
            _act.Should().Throw<ArgumentException>();
        }
    }
}