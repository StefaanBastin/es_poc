﻿using System;
using FluentAssertions;
using Reservation.CommandService.Domain.ValueObjects;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class When_Comparing_With_An_Object : Given_When_Then_Test
    {
        private Amenity _sut;
        private bool _result;
        private object _obj;

        protected override void Given()
        {
            var id = Guid.NewGuid();
            _obj = new object();
            _sut = new Amenity(id, new DateTime(2018, 06, 04), 99);
        }

        protected override void When()
        {
            _result = _sut.Equals(_obj);
        }

        [Fact]
        public void Then_It_Should_Not_Be_Equal()
        {
            _result.Should().BeFalse();
        }
    }
}