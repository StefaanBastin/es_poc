﻿using System;
using FluentAssertions;
using Reservation.CommandService.Domain.ValueObjects;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Domain.UnitTests.Entities.AmenityTests
{
    public class When_Comparing_And_Id_Not_Equal : Given_When_Then_Test
    {
        private Amenity _sut;
        private Amenity _amenity;
        private bool _result;

        protected override void Given()
        {
            var date = new DateTime(2018, 05, 04);
            _amenity = new Amenity(Guid.NewGuid(), date);
            _sut = new Amenity(Guid.NewGuid(), date, 99);
        }

        protected override void When()
        {
            _result = _sut.Equals(_amenity);
        }

        [Fact]
        public void Then_It_Should_Not_Be_Equal()
        {
            _result.Should().BeFalse();
        }
    }
}