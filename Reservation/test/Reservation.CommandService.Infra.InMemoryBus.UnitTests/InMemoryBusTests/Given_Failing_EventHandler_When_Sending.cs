﻿using System;
using Microsoft.Extensions.Logging;
using Moq;
using Reservation.CommandService.Domain;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Infra.InMemoryBus.UnitTests.InMemoryBusTests
{
    public class Given_Failing_EventHandler_When_Sending : Given_When_Then_Test
    {
        private InMemoryBus _sut;
        private TestEvent _event;
        private Mock<IEventHandler<TestEvent>> _eventHandler1Mock;
        private Mock<IEventHandler<TestEvent>> _eventHandler2Mock;

        protected override void Given()
        {
            _eventHandler1Mock = new Mock<IEventHandler<TestEvent>>();
            _eventHandler2Mock = new Mock<IEventHandler<TestEvent>>();
            var failingEventHandler = new FailingEventHandler();

            var eventHandlers = new IEventHandler[]
            {
                _eventHandler1Mock.Object,
                failingEventHandler,
                _eventHandler2Mock.Object,
            };

            _event = new TestEvent(Guid.NewGuid());

            _sut = new InMemoryBus(eventHandlers, Mock.Of<ILogger<InMemoryBus>>());
        }

        protected override void When()
        {
            _sut.Send(typeof(TestAggregate), _event);
        }

        [Fact]
        public void Should_Send_To_Other_EventHandlers()
        {
            _eventHandler1Mock.Verify(x => x.HandleAsync(_event));
            _eventHandler2Mock.Verify(x => x.HandleAsync(_event));
        }
    }
}
