﻿using System.Threading.Tasks;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.Infra.InMemoryBus.UnitTests.InMemoryBusTests
{
    public class FailingEventHandler : IEventHandler<TestEvent>
    {
        public Task HandleAsync(TestEvent evt)
        {
            throw new System.NotImplementedException();
        }
    }
}