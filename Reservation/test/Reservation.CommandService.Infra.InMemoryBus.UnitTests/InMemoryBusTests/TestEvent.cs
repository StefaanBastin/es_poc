﻿using System;
using Reservation.CommandService.Domain;

namespace Reservation.CommandService.Infra.InMemoryBus.UnitTests.InMemoryBusTests
{
    public class TestEvent : Event
    {
        public TestEvent(Guid aggregateId)
            : base(aggregateId)
        {
        }
    }
}