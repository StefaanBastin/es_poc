﻿using System;
using System.Collections.Generic;
using Autofac;
using FluentAssertions;
using Moq;
using Reservation.CommandService.Domain;
using Reservation.CommandService.Domain.Commands;
using Reservation.CommandService.Domain.Dtos;
using Reservation.CommandService.Domain.Entities;
using Reservation.CommandService.Domain.Events;
using Reservation.CommandService.Host.ConsoleApp.Container;
using Reservation.CommandService.Infra.ActiveMq.Sender;
using Reservation.CommandService.Infra.EventStore;
using Reservation.CommandService.Messages;
using Reservation.CommandService.TestInfra;
using ToolBelt.TestSupport;
using Xunit;

namespace Reservation.CommandService.Application.ItgTests
{
    [Collection("ActiveMqDocker collection")]
    public class When_Cancelling_Reservation : Given_When_Then_Test
    {
        private readonly IList<Tuple<Type, object>> _messages = new List<Tuple<Type, object>>();
        private CancelReservationHandler _sut;
        private CancelReservation _cancelReservationCommand;
        private Guid _reservationId;
        private Guid _amenityId;

        protected override void Given()
        {
            _reservationId = Guid.NewGuid();
            _cancelReservationCommand = new CancelReservation(_reservationId);

            var container = new ContainerBuilderFactory().Create().Build();

            container = container.Remove(new List<Type>
            {
                typeof(EventRepository),
                typeof(ActiveMqSender)
            });

            _amenityId = Guid.NewGuid();
            var fakeEventRepository = new FakeEventRepository(new Dictionary<Guid, IList<Event>>
            {
                {
                    _reservationId,
                    new List<Event>
                    {
                        new BookedReservationCreated(_reservationId, new List<AmenityDto> { new AmenityDto(_amenityId, new DateTime(2018, 6, 1), 6) })
                    }
                }
            });
            var externalMessengerMock = new Mock<IBusinessContextSender>();
            externalMessengerMock
                .Setup(messenger => messenger.Send(It.IsAny<Type>(), It.IsAny<object>()))
                .Callback<Type, object>((source, message) =>
                {
                    _messages.Add(new Tuple<Type, object>(source, message));
                });
            container = container.Add(builder =>
            {
                builder.RegisterInstance(fakeEventRepository).As<IEventRepository>();
                builder.RegisterInstance(externalMessengerMock.Object).As<IBusinessContextSender>();
            });

            _sut = new CancelReservationHandler(container.Resolve<IEventDispatcher>(), container.Resolve<IRepository<BookedReservation>>());
        }

        protected override void When()
        {
            // TODO: once logic is available on a higher level in the application layer for calling the command handler this should be called here
            _sut.HandleAsync(_cancelReservationCommand).Wait();
        }

        [Fact]
        public void Then_Send_Booking_Amenity_Released_Messages()
        {
            var amenityBookingReleasedEventMessage = new AmenityBookingReleasedEventMessage
            {
                AmenityId = _amenityId,
                Date = new DateTime(2018, 6, 1),
                Quantity = 6
            };

            _messages.Should().BeEquivalentTo(
                new List<Tuple<Type, object>>
                {
                    new Tuple<Type, object>(typeof(AmenityBookingReleasedEventMessage), amenityBookingReleasedEventMessage)
                }, options => options.ComparingByMembers<Tuple<Type, object>>());
        }
    }
}